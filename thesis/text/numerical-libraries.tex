% This is a source file for the Numerical libraries for linear algebra chapter of Vít Břichňáč's bachelor thesis, Task-based Cholesky decomposition

\chapter{Numerical libraries for linear algebra}

In this chapter, we will present existing libraries for dense numerical linear algebra that are used in this thesis for performance comparison.

\section{LAPACK}\label{sec-lapack}

We will start with the~oldest library described in this chapter -- LAPACK.
The~content of this section closely follows \cite{LapackLawn01}.

In 1987, six authors, three of whom were co-authors of the~original articles that introduced the~Level 1, Level 2 and
Level 3 BLAS routine sets (articles \cite{AcmBlas1979}, \cite{AcmBlas1988} and \cite{AcmBlas1990}), pledged to design and develop a
new numerical library that would serve as an~alternative to the~LINPACK and EISPACK libraries \cite{LapackLawn01}. Consequently, the~scope of this new library
would mainly consist of the~following:

\begin{itemize}
	\item Routines for solving systems of linear equations
	\item Routines for eigenvalue problems
	\item Routines for linear least squares problems
	\item Routines for performing associated matrix factorizations
	\item Routines for estimating condition numbers for the~above problems
\end{itemize}

The~main reason for this proposal was the~insufficient performance of the~LINPACK and EISPACK libraries, which were constructed only using
Level 1 BLAS routines. Using optimized implementations of those routines, LINPACK and EISPACK could perform well on scalar processors,
but these libraries were not very well suited for vector processing machines. Thus, many of the~algorithms used in LINPACK and EISPACK were
restructured to employ the~Level 2 and Level 3 BLAS so that users could gain increased performance by linking against optimized
BLAS libraries.

Many new algorithms (especially the~ones computing matrix factorizations) took a~similar approach to utilize Level 3 BLAS routines --
partition the~matrices into blocks and then express the~computation using basic matrix operations on those blocks. Those algorithms were
called \textbf{blocked algorithms}; an~example is given in Section \ref{sec-cholesky-blocked}. The~basic block operations often involved
the~same computation being performed on one of the~blocks (such as the~\texttt{POTF2} routine used in Algorithm
\ref{alg-improved-blocked-cholesky}), which required the~\textbf{unblocked algorithm} to be implemented in the~library, too.
The~unblocked algorithms used Level 2 BLAS routines more frequently.

Like the~Reference BLAS libraries, LAPACK was implemented in Fortran 77. Many routines also followed the~naming convention showcased in Example
\ref{example-level2-blas-naming-conventions}, with the~exception that the~operation type could exceed the~two letter limit. The~routines were
divided into two groups -- top-level routines that were intended to solve complete problems and lower-level routines which represented the
individual steps of those solutions.

Apart from improving the~performance of LAPACK and EISPACK routines, the~library also intended to increase portability (mainly through making
calls to BLAS) and serve as a~benchmark to evaluate the~performance of various supercomputers.

\section{Intel OneApi MKL}

In an~effort to optimize performance with respect to the~given CPU architecture and its features, hardware vendors often provide their own
implementations of numerical routines. An~example of this phenomenon is the~Math Kernel Library (MKL),
which is freely provided by Intel as a~part of their OneApi interface. The~rest of this section follows \cite{MKLGetStartedGuide}.
The~functionalities of MKL include:

\begin{itemize}
	\item Dense linear algebra routines -- all BLAS and selected LAPACK routines
	\item Selected sparse BLAS routines
	\item Fast Fourier Transform routines
	\item Random number generators
	\item Selected Vector Math routines
\end{itemize}

MKL is parallelized using the~Intel Threading Building Blocks (TBB) runtime model but also supports OpenMP GPU offloading.
Programs that use the~MKL are suggested to be compiled using Intel's \texttt{icc} compiler (which is also part of the~OneApi interface),
though other compilers, such as GNU \texttt{gcc}, are supported as well.

\section{OpenBLAS}

OpenBLAS is an~open-source numerical library which supports many different CPU architectures, such as x86-64, AArch64 and RISC-V. As a~successor
to GotoBLAS, it was initially strictly a~BLAS library. Today, it implements many LAPACK routines as well (including the~\texttt{POTRF}
Cholesky decomposition routine), though the~optimization efforts
are mainly focused on the~BLAS routines. The~leading maintainer of the~project is Dr Zhang Xianyi \cite{OpenBLASGitHub}.

For many BLAS routines, the~authors of the~project use a~\textit{``template-based optimization framework''}  \cite{OpenBLASAUGEM} called 
AUGEM. The~framework allows them to select \textit{``the~best configurations based on performance feedback of the~optimized code''} \cite{OpenBLASAUGEM}.

\section{AMD Optimizing CPU Libraries}

This section follows \cite{AOCLUserGuide}.

As Intel MKL became more focused on Intel CPUs, AMD introduced their own set of numerical libraries named AMD Optimizing CPU Libraries (AOCL).
Unlike Intel MKL, AOCL is fully open-source. Most of the~libraries that the~AOCL set contains are forks of other open-source
numerical libraries, such as:

\begin{itemize}
	\item AOCL-BLIS -- an~optimized BLAS library (a~fork of BLIS)
	\item AOCL-libFLAME -- a~parallel numerical library providing the~functionality of LAPACK (a~fork of libFLAME)
	\item AOCL-ScaLAPACK -- a~numerical library for \textit{``parallel distributed memory machines''} \cite{AOCLUserGuide}
\end{itemize}

AOCL also implements routines for sparse linear algebra, Fast Fourier Transform, cryptographically secure random and pseudo-random number generation,
cryptography, data compression, optimized math, string and memory functions.
All of the~AOCL libraries are available on GitHub.

\section{Arm performance libraries}

Arm developed a~set of free-to-use (though not open-source) libraries named the~Arm Performance Libraries. It is optimized for Arm CPUs,
and it implements the~following functionalities \cite{ArmPLOverview}:

\begin{itemize}
	\item The~BLAS routines
	\item Higher level numerical linear algebra routines using the~LAPACK interface
	\item Routines for Fast Fourier Transform using the~FFTW\footnote{Fastest Fourier Transform in the~West, an~open-source library implementing Fast Fourier Transform routines.} interface
	\item Sparse numerical linear algebra functionalities
	\item Elementary math functions optimized for the~AArch64 architecture as defined in \texttt{math.h}
	\item String and memory functions optimized for the~AArch64 architecture as defined in \texttt{string.h}
\end{itemize}

\noindent Many of the~numerical routines are parallelized using OpenMP \cite{ArmPLOverview}.