\chapter*{Conclusions}\addcontentsline{toc}{chapter}{Conclusions}\markboth{Conclusions}{Conclusions}

The~goal of this thesis was to implement the~blocked Cholesky decomposition algorithm using a~task-based runtime system, test the
implementation and evaluate its performance compared to other well known numerical libraries for linear algebra. Along the~main algorithm,
the~implementation also consisted of three Level 3 BLAS routines (\texttt{DGEMM}, \texttt{DSYRK} and \texttt{DTRSM}) and the~low-level LAPACK
routine \texttt{POTF2}, which implements the~unblocked algorithm for Cholesky decomposition.

In the~implementation, users are able to select the utilized runtime systems (OpenMP and/or StarPU) via a~configuration
script. They can then also select a~numerical library that provides the~optimized implementations for the~four subroutines mentioned above.
In case that the~user does not have access to any of the~libraries or they choose not to use them, they can also opt for the~algorithm
to utilize the~builtin implementations of those subroutines. They are, however, not optimized, and their performance will
thus not compare well with their optimized counterparts.

All of the~variants for both runtime systems and all numerical libraries were tested using slightly modified LAPACK test routines.

The~performance of the~implementation was evaluated in comparison to the~Arm Performance Libraries, MKL, AOCL and OpenBLAS
libraries/library sets on three different compute nodes -- two of them with x86-64 processors (AMD EPYC 7H12 and Intel Cascade Lake 6240)
and the~third one with an~ARMv8 processor (Fujitsu A64FX). On both of the~x86-64 processors, the~StarPU implementation performs best with
MKL driver routines, where it reaches more than 95\% of MKL's performance for certain matrix sizes. On the~A64FX processor,
the~StarPU implementation using driver routines from Arm Performance Libraries performs well for large matrices,
even outperforming the~Arm Performance Libraries \texttt{DPOTRF} implementation for one specific matrix size.

In the~thesis, a~custom proof of correctness was provided for the~unblocked algorithm and a~custom proof inspired by \cite{TrefethenBau2022} was
formulated for the~correctness of the~blocked algorithm as well.

% Future

The~main area of possible improvement is the~implementation of the~four driver routines. The~implementation could be rewritten in order
to utilize features of modern processors (such as SSE/AVX vector register instructions) or some amount of parallelization. 
The~behavior of the~implementation could also be studied more thoroughly so that the~algorithm could select the~optimal block size
automatically depending on the~matrix size, the~CPU type/architecture and the~available number of cores.

The~main Cholesky decomposition routines could be extended to a~full parallelized alternative to the~LAPACK routine \texttt{DPOTRF}.
To accomplish that, the~routines would have to:
\begin{enumerate}
	\item Provide the~functionality to calculate the~upper triangular variant of the~Cholesky decomposition ($A=U^TU$)
	\item Support input matrices whose leading dimension is greater than their size
	\item Indicate invalid parameter values by returning non-zero integer values
	\item Indicate that the~input matrix is not symmetric positive definite, also by returning non-zero integer values
\end{enumerate}

The~performance of the~blocked Cholesky decomposition routines could also be improved by experimenting with different schedulers or
task priorities.

The~knowledge of the~concepts described in this thesis (block algorithm formulation, working with the~LAPACK library and the~BLAS interface,
fundamentals of task-based programming, testing and evaluation of numerical routines) could be utilized to provide implementations
of other blocked numerical algorithms, such as the~LU and QR factorizations or the~SVD.