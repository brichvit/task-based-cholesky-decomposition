% This is a source file for the Description of used BLAS routines chapter of Vít Břichňáč's bachelor thesis, Task-based Cholesky decomposition

\chapter{Description of used BLAS routines}
\label{chapter-blas-routines}

% Ref: Netlib - BLAS, section Presentation
\textbf{BLAS} (Basic Linear Algebra Subprograms) is a~set of routines that perform basic operations in linear algebra \cite{NetlibBlas}.
% Ref: Netlib - BLAS, section Optimized BLAS Library
A~BLAS library is a~library providing implementations for all BLAS routines.

A~so-called Reference BLAS library is available at \cite{NetlibBlas}.
This implementation can be used in various applications where basic linear algebra operations need to be performed,
though it is not meant to be used for production runs. The~main reason is its performance, which is suboptimal in comparison
to highly optimized (usually platform-specific) BLAS libraries.

The~more common way to utilize BLAS is to use machine-specific BLAS libraries, usually provided by the~computer vendor
(such as Intel OneApi MKL for Intel x86 processors) or independent software vendors (such as BLIS for AMD x86 processors).

The~Reference BLAS library has, for the~most part, two use cases:
\begin{itemize}
	\item A~last resort option for users 
	\item A~working example (and API reference) for testing other BLAS libraries
\end{itemize}

\section{BLAS routine subsets}

% Ref: Netlib - BLAS, section Presentation
BLAS routines are divided into three categories \cite{NetlibBlas}:
\begin{itemize}
	\item \textbf{Level 1 BLAS} -- routines performing scalar-vector or vector-vector operations of complexity $\mathcal{O}(n)$
	\item \textbf{Level 2 BLAS} -- routines performing matrix-vector operations of complexity $\mathcal{O}(n^2)$
	\item \textbf{Level 3 BLAS} -- routines performing matrix-matrix operations of complexity $\mathcal{O}(n^3)$
\end{itemize}

As we can see in the~following three subsections, this categorization also has a~historical context.
The~content of these subsections will closely follow \cite{AcmBlas1979}, \cite{AcmBlas1988} and \cite{AcmBlas1990}, respectively.

\subsection{Level 1 BLAS}

The~historically first BLAS package, published by C. L. Lawson, R. J. Hanson, D. R. Kincaid and F. T. Krogh in 1979, was
comprised of the~set of routines that we now call Level 1 BLAS \cite{AcmBlas1979}. It was written in Fortran, and it included
38 routines for working with single-precision, double-precision, complex or extended-precision vectors.
Examples of these routines are:

\begin{itemize}
	\item Dot products (\texttt{DDOT}, \texttt{SDSDOT}, \texttt{CDOTU})
	\item Givens rotation construction and application (\texttt{DROTG}, \texttt{DROT})
	\item Euclidean and Manhattan norm calculation (\texttt{DNRM2}, \texttt{DASUM})
\end{itemize}

\subsection{Level 2 BLAS}

In 1988, an~extended set of BLAS routines was published by J. J. Dongarra, J. Du Croz, S. Hammarling and R. J. Hanson \cite{AcmBlas1988}.
The~newly developed routines performed matrix-vector operations of three types:
\begin{itemize}
	\item Matrix-vector products (\texttt{GEMV}, \texttt{SYMV}, \texttt{TRMV})
	\item Solution of triangular linear systems of equations (\texttt{TRSV})
	\item Rank-1 or rank-2 updates (\texttt{GER}, \texttt{SYR}, \texttt{SYR2})
\end{itemize}

The~paper \cite{AcmBlas1988} also introduced the~Level 1 BLAS and Level 2 BLAS terminology, noting that the~newly-introduced
routines could be implemented by series of calls to the~original routine set from 1979, but it was inefficient
(and therefore not recommended) to use such an~implementation.

As the~new routine set included matrix operations, unlike its predecessor, it had to account for various types of matrices
(general, symmetric, hermitian, triangular, band matrices) and various forms of storage (packed matrices)
that were used in different applications. That is why it introduced a~naming convention with two prefixes -- one denoting the
scalar data type and the~other denoting the~matrix type. This naming convention would later be used in Level 3 BLAS \cite{AcmBlas1990} as well.

\begin{example}[Level 2 BLAS routine naming convention example]\label{example-level2-blas-naming-conventions}
\end{example}

\begin{center}
	\begin{tikzpicture}
		\node at (-4,0) {\Huge{\textbf{\color{DarkGreen}D}}};
		\node [rotate=90,scale=2] at (-4,-0.7) {\Huge$\lbrace$};
		\node at (-4,-1.5) {Scalar data type};
		\node at (-4,-2) {\textbf{\color{DarkGreen}D}ouble precision};

		\node at (0,0) {\Huge{\textbf{\color{OrangeRed}SB}}};
		\node [rotate=90,scale=2] at (0,-0.7) {\Huge$\lbrace$};
		\node at (0,-1.5) {Matrix type};
		\node at (0,-2) {\textbf{\color{OrangeRed}S}ymmetric \textbf{\color{OrangeRed}B}and matrix};

		\node at (4.5,0) {\Huge{\textbf{\color{NavyBlue}MV}}};
		\node [rotate=90,scale=2] at (4.5,-0.7) {\Huge$\lbrace$};
		\node at (4.5,-1.5) {Operation type};
		\node at (4.5,-2) {\textbf{\color{NavyBlue}M}atrix-\textbf{\color{NavyBlue}V}ector product};
	\end{tikzpicture}
\end{center}

Some newly introduced argument conventions (e.g., the~\texttt{TRANS} and \texttt{UPLO} arguments) would also later
make their way to Level 3 BLAS.

\subsection{Level 3 BLAS}\label{sec-level-3-blas}

Level 2 BLAS routines offered efficient matrix-vector computations suited mostly for vector processing machines
\cite{AcmBlas1990}. Many matrix-matrix operations could be performed by sequences of calls to Level 2 BLAS routines
(e.g., matrix-matrix multiplication could be carried out by calling the~matrix-vector multiplication routine \texttt{DGEMV}
on every column of the~right-hand side matrix). That approach, however, proved to be inefficient for computers with a~hierarchical
structure of memory (i.e., global memory, several levels of cache and vector registers), where better performance could be obtained
by splitting the~matrices into blocks and performing those operations on the~blocks.

For that reason, no more than 2 years after Level 2 BLAS was made public, a~new article describing Level 3 BLAS routines
was published along with a~reference implementation \cite{AcmBlas1990}. It followed naming conventions from Level 2 BLAS
(see Example \ref{example-level2-blas-naming-conventions}), and it introduced four new types of operations:

\begin{itemize}
	\item Matrix-matrix products (\texttt{GEMM}, \texttt{SYMM}, \texttt{HEMM})
	\item Rank-k or rank-2k updates of symmetric/hermitian matrices (\texttt{SYRK}, \texttt{HERK}, \texttt{SYR2K})
	\item Products of general matrices and triangular matrices (\texttt{TRMM})
	\item Solution of triangular linear systems of equations with a~right-hand side matrix (\texttt{TRSM})
\end{itemize}

The~scope of the~new set was intentionally limited -- for instance, no routines for matrix decomposition were proposed, as they
would later become part of a~library that was in development at the~time, LAPACK (see Section \ref{sec-lapack}).

Three Level 3 BLAS routines are used in the~blocked Cholesky decomposition algorithm, as proposed by \cite{AcmBlas1990}.
Implementation of these three routines was also a~part of this thesis. On that account, they will be described
in more detail in the~following sections of this chapter.

\section{General matrix-matrix product (\texttt{GEMM})}\label{sec-blas-gemm}

The~\texttt{GEMM} routine performs the~following computation:
\begin{equation}
	C \leftarrow \alpha \op_A (A) \op_B (B) + \beta C
\end{equation}

The~values of $\alpha$ and $\beta$ are passed to \texttt{GEMM} as parameters \texttt{ALPHA} and \texttt{BETA}.
Assuming that $A$, $B$ and $C$ are real matrices, the~meaning of $\op_A$ and $\op_B$ is determined by the~\texttt{TRANSA} and \texttt{TRANSB}
parameter values as follows:
\begin{itemize}
	\item If \texttt{TRANSA}$=$\texttt{N}, then $A$ is an~$M\times K$ matrix and $\op_A (A) = A$
	\item If \texttt{TRANSA}$=$\texttt{T} or \texttt{TRANSA}$=$\texttt{C}, then $A$ is a~$K\times M$ matrix and $\op_A (A) = A^T$
	\item If \texttt{TRANSB}$=$\texttt{N}, then $B$ is a~$K\times N$ matrix and $\op_B (B) = B$
	\item If \texttt{TRANSB}$=$\texttt{T} or \texttt{TRANSB}$=$\texttt{C}, then $B$ is an~$N\times K$ matrix and $\op_B (B) = B^T$
\end{itemize}

The~\texttt{C} value of these parameters marks a~Hermitian transpose, which, in the~case of real matrices,
is equivalent to a~transpose as specified in Definition \ref{definition-transpose} \cite{AcmBlas1990}.

% Note: integer-valued is the correct option (as opposed to integer valued)
The~other parameters of \texttt{GEMM} determine the~matrix sizes (integer-valued parameters \texttt{M}, \texttt{N} and \texttt{K}),
the pointers to the~matrices (parameters \texttt{A}, \texttt{B} and \texttt{C}) and the~leading dimensions
(parameters \texttt{LDA}, \texttt{LDB} and \texttt{LDC}).

A~naive (simple but unoptimized) implementation of the~\texttt{GEMM} routine could be described with the~following pseudocode
(we consider \texttt{TRANSA}$=$\texttt{N} and \texttt{TRANSB}$=$\texttt{N} and ignore leading dimensions for simplicity):

\begin{algorithm} [H]\label{alg-naive-gemm}
	\caption{Naive \texttt{GEMM} for \texttt{TRANSA}$=$\texttt{N} and \texttt{TRANSB}$=$\texttt{N} without leading dimensions}
	\KwData{Positive integers $M, N, K$, matrices $A\in\R^{M,K}, B\in\R^{K,N}, C\in\R^{M,N}$}
	\KwResult{The~updated matrix $C$}
	\For{$i\gets 1; i \le M; i\gets i+1$}{
		\For{$j\gets 1; j \le N; j\gets j+1$}{
			$C_{ij}\gets \beta C_{ij}$\label{naive-gemm-beta-step}\\
			\For{$k\gets 1; k \le K; k\gets k+1$}{
				$C_{ij}\gets C_{ij} + \alpha A_{ik} B_{kj}$\label{naive-gemm-alpha-step}\\
			}
		}
	}
	\Return{C}
\end{algorithm}

\newpage

The~number of floating point operations performed is:

\begin{itemize}
	\item $MN$ multiplications on line \ref{naive-gemm-beta-step}
	\item $2MNK$ multiplications on line \ref{naive-gemm-alpha-step}
	\item $MNK$ additions on line \ref{naive-gemm-alpha-step}
\end{itemize}

In total, we have $3MNK + MN$ floating point operations. If the~values of $\alpha$ and $\beta$ are both equal to one,
which is a~common case when benchmarking \texttt{GEMM} implementations, we can omit $MNK$ multiplications on line \ref{naive-gemm-alpha-step}
and remove line \ref{naive-gemm-beta-step} altogether. That leaves us with $2MNK$ floating point operations.

It is also worth noting that the~total number of floating point operations is the~same for all values of \texttt{TRANSA}, \texttt{TRANSB}
and leading dimensions \cite{VanDeGeijnQuintataOrti2008}.

\section{Symmetric matrix rank-k update (\texttt{SYRK})}\label{sec-blas-syrk}

The~\texttt{SYRK} routine calculates
\begin{equation}\label{eq-syrk-non-transposed}
	C \leftarrow \alpha AA^T + \beta C
\end{equation}

\noindent for an~$N\times K$ matrix $A$ if \texttt{TRANSA}$=$\texttt{N} and

\begin{equation}\label{eq-syrk-transposed}
	C \leftarrow \alpha A^TA + \beta C
\end{equation}

\noindent for a~$K\times N$ matrix $A$ if \texttt{TRANSA}$=$\texttt{T}. The~matrix $C$ is symmetric with the size $N\times N$ in both cases.

The~other parameters are:
\begin{itemize}
	\item \texttt{UPLO} -- determines whether the~upper (value \texttt{U}) or lower (value \texttt{L})
		part of $C$ is stored\\(see Subsection \ref{subsec-triangular-symmetric-matrix-representation})
	\item \texttt{N} and \texttt{K} -- the~matrix sizes
	\item \texttt{LDA} and \texttt{LDC} -- the~matrix leading dimensions
	\item \texttt{A} and \texttt{C} -- the~matrices
	\item \texttt{ALPHA} and \texttt{BETA} -- the~$\alpha$ and $\beta$ values used in equations
		\eqref{eq-syrk-non-transposed} and \eqref{eq-syrk-transposed}
\end{itemize}

For values \texttt{TRANSA}$=$\texttt{N} \texttt{UPLO}$=$\texttt{U}, \texttt{SYRK} could be naively implemented as follows
(again, ignoring leading dimensions):

\begin{algorithm} [H]\label{alg-naive-syrk}
	\caption{Naive \texttt{SYRK} for \texttt{TRANSA}$=$\texttt{N} and \texttt{UPLO}$=$\texttt{U} without leading dimensions}
	\KwData{Positive integers $N, K$, matrices $A\in\R^{N,K}$ and $C\in\R^{N,N}$}
	\KwResult{The~updated matrix $C$}
	\For{$i\gets 1; i \le N; i\gets i+1$}{
		\For{$j\gets i; j \le N; j\gets j+1$}{
			$C_{ij}\gets \beta C_{ij}$\label{naive-syrk-beta-step}\\
			\For{$k\gets 1; k \le K; k\gets k+1$}{
				$C_{ij}\gets C_{ij} + \alpha A_{ik} A_{jk}$\label{naive-syrk-alpha-step}\\
			}
		}
	}
	\Return{C}
\end{algorithm}

\newpage

The~number of times line \ref{naive-syrk-beta-step} is executed is
$$\Sum{i=1}{N}\Sum{j=i}{N}1 = \Sum{i=1}{N}(N-i+1) = N^2 - \Sum{i=1}{N}i + N = N^2 - \frac{N(N-1)}{2} + N = \frac{1}{2}N^2 + \frac{3}{2}N.$$

Line \ref{naive-syrk-alpha-step} is then executed $K\big(\frac{1}{2}N^2 + \frac{3}{2}N\big)$ times.

That leaves us with $K\big(\frac{1}{2}N^2 + \frac{3}{2}N\big)$ additive and 
$\frac{1}{2}N^2 + \frac{3}{2}N + 2K\big(\frac{1}{2}N^2 + \frac{3}{2}N\big) = (2K+1)\big(\frac{1}{2}N^2 + \frac{3}{2}N\big)$ multiplicative
floating point operations, or
$$K\Big(\frac{1}{2}N^2 + \frac{3}{2}N\Big) + (2K+1)\Big(\frac{1}{2}N^2 + \frac{3}{2}N\Big) = (3K+1)\Big(\frac{1}{2}N^2 + \frac{3}{2}N\Big)$$
floating point operations in total.

In case of both $\alpha$ and $\beta$ being equal to one, we end up with $K\big(\frac{1}{2}N^2 + \frac{3}{2}N\big)$ additive and
$K\big(\frac{1}{2}N^2 + \frac{3}{2}N\big)$ multiplicative floating point operations, totaling $K\big(N^2 + 3N\big)$ floating
point operations.

According to \cite{VanDeGeijnQuintataOrti2008}, the~analysis yields the~same result for other values of \texttt{TRANSA} and \texttt{UPLO}.
\section{Solution of triangular systems of equations with multiple right-hand sides (\texttt{TRSM})}\label{sec-blas-trsm}

The~\texttt{TRSM} routine receives a~nonsingular triangular matrix $A$ and an~$M\times N$ matrix $B$.

\noindent It calculates

\begin{equation}\label{eq-trsm-left}
	B \leftarrow \alpha \op(A)^{-1}B
\end{equation}

\noindent where $A\in\R^{M\times M}$ if \texttt{SIDE}~$=$~\texttt{L} and

\begin{equation}\label{eq-trsm-right}
	B \leftarrow \alpha B\op(A)^{-1}
\end{equation}

\noindent where $A\in\R^{N\times N}$ if \texttt{SIDE}~$=$~\texttt{R}.

If the~value of the~\texttt{DIAG} parameter is \texttt{U}, all of the~entries on the~main diagonal of
$A$ are assumed to equal one, allowing us to skip one floating point division for every entry of $B$
(see Algorithm \ref{alg-naive-trsm}). If the~entries on the~main diagonal are not known to be one, the~user
should use the~\texttt{N} value of \texttt{DIAG}.

The~value of $\alpha$ is determined by the~parameter \texttt{ALPHA} and $\op_A$ is determined by \texttt{TRANSA} in the same way
as described in Section \ref{sec-blas-gemm}. The~\texttt{UPLO} parameter defines the~stored part of $A$
(see Subsection \ref{subsec-triangular-symmetric-matrix-representation}). The~rest of the~parameters represent the~matrices
(\texttt{A} and \texttt{B}), their dimensions (\texttt{M} and \texttt{N}) and their
leading dimensions (\texttt{LDA} and \texttt{LDB}).

As stated above, $A$ is assumed to be nonsingular. The~\texttt{TRSM} routine, however, does not explicitly check
for its nonsingularity. As a~consequence, if a~singular
matrix (or a~``nearly singular'' matrix\footnote{By ``nearly singular'', we mean a~matrix whose condition number approaches positive infinity.})
matrix $A$ is used, the~resulting matrix may vastly differ from the~matrix specified in equations \eqref{eq-trsm-left} or \eqref{eq-trsm-right}
due to numerical stability issues.

For the~rest of this section, we name the~newly calculated matrix $X$.
When \texttt{SIDE}~$=$~\texttt{L} and \texttt{TRANSA}~$=$~\texttt{N}, we can rewrite the~calculation
\eqref{eq-trsm-right} as an~equation and then rearrange it in the~following way:

\begin{align*}
	X &= \alpha A^{-1}B\\
	AX &= A(\alpha A^{-1})B\\
	AX &= \alpha (AA^{-1})B\\
	AX &= \alpha IB\\
	AX &= \alpha B
\end{align*}

We further suppose that \texttt{UPLO}~$=$~\texttt{L}.
The~matrix $X$ can then be computed in an~entry-wise manner:
\begin{align*}
	(AX)_{ij} &= (\alpha B)_{ij}\\
	\sum_{k=1}^{N}A_{ik}X_{kj} &= \alpha B_{ij}\\
	\sum_{k=1}^{i}A_{ik}X_{kj} &= \alpha B_{ij}\\
	A_{ii}X_{ij} + \sum_{k=1}^{i-1}A_{ik}X_{kj} &= \alpha B_{ij}\\
	A_{ii}X_{ij} &= \alpha B_{ij} - \sum_{k=1}^{i-1}A_{ik}X_{kj}\\
	X_{ij} &= \frac{\alpha B_{ij} - \sum_{k=1}^{i-1}A_{ik}X_{kj}}{A_{ii}}
\end{align*}
When transforming the~sum in the~left-hand side of the~equation from line 2 to line 3,
we eliminated all summands containing the~term $A_{ik}$ where $k>i$.
Those terms are all equal to zero due to $A$ being lower triangular.

When calculating $X_{ij}$ using the~last equality, all entries in the~same column with a~smaller row index
must have already been calculated. Thus, if we choose any entry calculation order which satisfies those dependencies,
such as a~row major order where rows are calculated top to bottom, the~matrix $X$ can be calculated
in the~place of the~original matrix $B$.

Assuming that we also know that all of the~entries on the~main diagonal are equal to one (that is, \texttt{DIAG}~$=$~\texttt{U}),
we can skip the~division by $A_{ii}$. All of these findings lead us to the~following naive \texttt{TRSM} algorithm:

\begin{algorithm} [H]\label{alg-naive-trsm}
	\caption{Naive \texttt{TRSM} for \texttt{SIDE}~$=$~\texttt{L} and \texttt{TRANSA}~$=$~\texttt{N} without leading dimensions}
	\KwData{Positive integers $M, N$, matrices $A\in\R^{M,M}$ and $B\in\R^{M,N}$}
	\KwResult{The~updated matrix $B$}
	\For{$i\gets 1; i \le M; i\gets i+1$}{
		\For{$j\gets 1; j \le N; j\gets j+1$}{
			$B_{ij}\gets \alpha B_{ij}$\label{naive-trsm-alpha-step}\\
			\For{$k\gets 1; k < i; k\gets k+1$}{
				$B_{ij}\gets B_{ij} - A_{ik}B_{kj}$\label{naive-trsm-sub-step}\\
			}
			\If{\texttt{DIAG} $=$ \texttt{N}}{
				$B_{ij}\gets B_{ij} / A_{ii}$\label{naive-trsm-div-step}\\
			}
		}
	}
	\Return{B}
\end{algorithm}

In this case (\texttt{SIDE}~$=$~\texttt{L} and \texttt{TRANSA}~$=$~\texttt{N}), we can see that the~algorithm performs:
\begin{itemize}
	\item $MN$ floating point operations on line \ref{naive-trsm-alpha-step}, 
	\item $2 \cdot \big(\frac{1}{2}M(M-1)N\big) = M(M+1)N$ operations on line \ref{naive-trsm-sub-step},
	\item $MN$ operations on line \ref{naive-trsm-div-step} (only if \texttt{DIAG}~$=$~\texttt{N})
\end{itemize}

Summed up, that is $M\big(2N + (M-1)N\big)$ floating point operations. If $\alpha=1$,
all operations on line \ref{naive-trsm-alpha-step} are left out, leaving us with $MN + M(M-1)N = MN(1 + M - 1) = M^2N$ operations.

That number of operations stays the~same for both values of \texttt{TRANSA} and both values of \texttt{UPLO}.
If \texttt{DIAG}~$=$~\texttt{U}, it further reduces to $M^2N - MN$.
The~floating point operation count also changes for \texttt{SIDE}~$=$~\texttt{R} to $MN^2$ and $MN^2 - MN$ for values of \texttt{DIAG}
being equal to \texttt{N} and \texttt{U}, respectively.~\cite{VanDeGeijnQuintataOrti2008}

\section{The CBLAS interface}

The~content of this section closely follows \cite{NetlibCblas}.

% Note: "inconvenience" is countable in this context (https://www.oxfordlearnersdictionaries.com/definition/english/inconvenience_1)
The~original BLAS specifications were made primarily with Fortran in mind. As Fortran is a~compiled language, the~routines
can, by all means, be called from C/C++ as well.
Furthermore, there is no functionality of BLAS that C/C++ programmers may be unable to access via the~Fortran interface.
However, differences in the~design of the~C and Fortran languages lead to inconveniences for C/C++ programmers who
use the~Fortran BLAS interface, such as the~necessity to perform additional argument preprocessing and return value postprocessing
or the~increased difficulty of debugging errors due to fewer compile-time checks. Examples of those inconveniences are:

\begin{itemize}
	\item Arguments have to be passed by reference, not by value
	\item No compile-time checking of argument values is performed
	\item No C include files are available
	\item Only column major matrix storage is supported, as it is the~default storage scheme in Fortran
	\item Vector indices start with $1$ (in C/C++, arrays are indexed starting from $0$)
\end{itemize}

As a~result, a~new C interface for BLAS routines, named CBLAS, was proposed \cite{NetlibCblas}.
The~routines were newly named using non-capital letters and a~\texttt{cblas\_} prefix
(e.g., the~\texttt{DGEMM} routine became \texttt{cblas\_dgemm} in CBLAS). The~vector index numbering was shifted
from $1, 2, \dots, N$ to $0, 1, \dots, N-1$ and arguments were made to be passed by value.
An~include file named \texttt{cblas.h} was made newly available so that the~C compiler could check the~routine calls
against the~specified signatures during compile time, leading to fewer errors.

% Note: run-time error is the correct option (as opposed to runtime error)
The~CBLAS interface also started using enumerated types for arguments that were characters in the~original Fortran interface.
That allowed for tighter error checking -- for instance, passing \texttt{CblasNoTrans} as a~value of the~\texttt{CBLAS\_SIDE} argument
raises a~compile-time error, but in the~original BLAS interface, passing the~character \texttt{N} as a~value of the~\texttt{SIDE}
argument only produces a~run-time error, which is harder to debug for the~programmer.

Another feature introduced by the~CBLAS interface is row major storage support.
By convention, the~column major storage scheme is used for matrices in Fortran, so the~Fortran BLAS interface does not support
row major matrices at all. The~CBLAS interface adds a~new parameter \texttt{CBLAS\_ORDER} to all routines working with matrices.
Using this parameter, programmers may specify the~storage scheme (column major or row major) used for all matrices passed to the~routine.
Reference \cite{NetlibCblas} notes that for Level 1 and Level 3 BLAS routines, no extra storage is required to support row major BLAS operations.
However, some Level 2 BLAS routines may require up to $\mathcal{O}(n)$ extra storage and $\mathcal{O}(n)$ extra floating
point operations.

% Note: one-dimensional error is the correct option (as opposed to one dimensional)
The~paper also mentions discussions about support for two-dimensional arrays in C (i.e., arrays of pointers), which was
not added in the~end, as conversions between two-dimensional C arrays and Fortran-style one-dimensional arrays would lead to
significant performance losses.