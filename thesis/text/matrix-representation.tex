% This is a source file for the Matrix representation in computer memory chapter of Vít Břichňáč's bachelor thesis, Task-based Cholesky decomposition

\chapter{Matrix representation in computer memory}

There are many ways in which programs can represent a~matrix in memory. Arguably the~most natural way to store matrices
is the~\textbf{row major full storage} layout \cite{IntelStorageSchemes}, where all entries are stored 
as a~one-dimensional array of numbers in a~contiguous area~in memory, ordered primarily by their row index and
secondarily by their column index. As this storage scheme can be used for all matrices, the~reader may ask themselves
why other storage schemes are needed. The~main reason is that some matrices are known to have special properties
which can be taken advantage~of in~order~to:
\begin{itemize}
	\item Use less memory space (for example, the~packed storage scheme for upper
		  triangular matrices only stores entries on/above the~main diagonal \cite{NetlibStorageSchemes})
	\item Improve the~efficiency of matrix algorithms (for example, the~compressed diagonal storage
		  makes matrix-vector products more efficient for some matrices \cite{NetlibSparseStorage})
\end{itemize}

Unless specified otherwise, row and column entries will be addressed starting from $1$ for the~rest of this chapter. Also, storage schemes
will be sometimes referred to as storage formats.

\section{Sparse matrix representation}\label{sec-sparse-matrix-storage}

The~content of this section follows the~structure of \cite{NetlibSparseStorage}.

Sparse matrices are matrices with a~sufficiently large amount of zero entries, for which storing all entries proves inefficient.
Many matrices that arise as approximations of systems of differential equations are sparse, and sparse matrices can be found in various other
areas of mathematics or other scientific disciplines. Some sparse storage schemes (e.g., the~compressed row/column storage scheme) are
designed to be used for any sparse matrices without particular assumptions about the~properties of those matrices, while others
(for example the~block compressed storage scheme) are designed for specific matrix subclasses with certain properties.

The~\textbf{compressed row storage} (CRS) \textbf{scheme} stores the~value and the~column index of every non-zero entry. There is an~array named \texttt{values}
storing the~values and a~separate array for the~column indices named \texttt{column\_indices}, both ordered first by the~row indices and then by the~column indices of the~saved entries.
A~pair of values on the~same position in both of those arrays always corresponds to the~same matrix entry
(that is, if \texttt{values}$(k) = a_{ij}$, then \texttt{column\_indices}$(k) = j$). However, using only those two arrays would make the~stored matrices ambiguous.
For instance, see Figure \ref{fig-crs-ambiguity}, where both of the~$2\times 2$ matrices would have an~identical representation.

\begin{figure}[H]
	\caption{Ambiguity of representation using only the~\texttt{values} and \texttt{column\_indices} arrays.}
	\label{fig-crs-ambiguity}
	
	\begin{center}
		\begin{tikzpicture}
			\node at (0,1.5) {$\begin{pmatrix}0&\color{DarkOrange}\mathbf{1}\\0&0\end{pmatrix}$};
			\node at (0,0) {$\begin{pmatrix}0&0\\0&\color{violet}\mathbf{1}\end{pmatrix}$};
	
			\node at (5,1.75) {\begin{tabular}{|C{18ex}|C{2ex}|}\hline \texttt{values}&\textbf{\color{DarkOrange}1}\\\hline\end{tabular}};
			\node at (5,1.25) {\begin{tabular}{|C{18ex}|C{2ex}|}\hline \texttt{column\_indices}& 2\\\hline\end{tabular}};
			\node at (5,0.25) {\begin{tabular}{|C{18ex}|C{2ex}|}\hline \texttt{values}&\textbf{\color{violet}1}\\\hline\end{tabular}};
			\node at (5,-0.25) {\begin{tabular}{|C{18ex}|C{2ex}|}\hline \texttt{column\_indices}& 2\\\hline\end{tabular}};
		\end{tikzpicture}
	\end{center}
\end{figure}

Due to this ambiguity, we introduce a~third array that stores the~index in the~\texttt{values} and \texttt{column\_indices}
arrays of the~first non-zero entry in every row. We name it \texttt{row\_ptrs}.
If there are no non-zero entries in the~$i$th row, the~\texttt{row\_ptrs}$(i)$ value is determined as follows:
\begin{itemize}
	\item If $i=1$, then \texttt{row\_ptrs}$(i)=0$
	\item If $i>1$, then \texttt{row\_ptrs}$(i)=$\texttt{ row\_ptrs}$(i-1)$
\end{itemize}

We denote the~number of non-zero entries in the~matrix by $nnz$.
Sometimes, $(nnz+1)$ is appended to the~end of \texttt{row\_ptrs} to make matrix traversal easier to implement.
In total, using the~CRS scheme reduces the~amount of stored numbers from $n^2$ to $2nnz + n + 1$ \cite{NetlibSparseStorage}.

\begin{example}{Compressed row storage example.}
\end{example}

\begin{center}
	\begin{tikzpicture}
		\node at (0,0) {$\begin{pmatrix}0&0&0&0&0\\0&5&0&-2&0\\3&0&0&1&6\\0&0&0&0&0\\0&-4&0&0&7\end{pmatrix}$};

		\node at (7,0.6) {\begin{tabular}{|C{18ex}|C{2ex}|C{2ex}|C{2ex}|C{2ex}|C{2ex}|C{2ex}|C{2ex}|}\hline \texttt{values}&5&-2&3&1&6&-4&7\\\hline\end{tabular}};
		\node at (7,0.1) {\begin{tabular}{|C{18ex}|C{2ex}|C{2ex}|C{2ex}|C{2ex}|C{2ex}|C{2ex}|C{2ex}|}\hline \texttt{column\_indices}&2&4&1&4&5&2&5\\\hline\end{tabular}};
		\node at (7,-0.6) {\begin{tabular}{|C{18ex}|C{2ex}|C{2ex}|C{2ex}|C{2ex}|C{2ex}|C{2ex}|}\hline \texttt{row\_ptrs}&0&1&3&3&6&8\\\hline\end{tabular}};
	\end{tikzpicture}
\end{center}

The~\textbf{compressed column storage} (CCS) \textbf{scheme} operates in a~similar way as the~compressed row storage scheme, but
the~matrix is transposed before saving, and the~arrays are renamed to \texttt{row\_indices} and \texttt{column\_ptrs}.
Effectively, the~original matrix is traversed by columns, unlike in CRS, where it is traversed by rows.

The~\textbf{block compressed row storage scheme} is used to store matrices where non-zero entries arise only in square submatrices (blocks)
with a~regular pattern. Those matrices are usually a~result of discretization of a~system of partial differential equations.
In the~block compressed row storage scheme, the~blocks are handled as dense matrices. This approach leads to $nnzb\times n_b$ numbers
being stored, where $nnzb$ is the~number of blocks and $n_b$ is the~size of each block \cite{NetlibSparseStorage}.

\section{Dense matrix representation}

In this section, we will present matrix storage schemes which are used in dense numerical linear algebra libraries, mainly BLAS libraries
(see Chapter \ref{chapter-blas-routines}) and LAPACK (see Section \ref{sec-lapack}). Some of the~matrix classes that these storage schemes
are designed for may be considered sparse, since not all of the entries of those matrices are stored.
However, in this text, they will be labeled as dense.

This section mainly follows the~structure of \cite{IntelStorageSchemes} and \cite{NetlibStorageSchemes}.

\subsection{Full storage scheme}

The~\textbf{full storage scheme} (or \textbf{conventional storage}) is ideal for storing matrices that we have no initial assumptions about.
In this storage format, every matrix entry is stored, including the~zero ones. There are several methods to map the~entries from the
two-dimensional matrix into the~one-dimensional computer memory, which will be described in the~following subsections.

\subsubsection{Row major storage}\label{subsubsec-row-major-full-storage}

The~\textbf{row major full storage scheme} stores every matrix row in a~contiguous memory area, but these areas
may not have consecutive addresses. In those areas, the~order of the~matrix entries is determined by their
column index in the~original matrix. The~spacing between the~first bytes of those areas is defined by a~parameter called the
\textbf{leading dimension} of the~matrix, which we will label \texttt{LDA} in this section. Naturally, \texttt{LDA} may not be smaller
than the~number of columns, as that would make those memory areas overlap, meaning that the~computer would have to store
more than 1 byte at a~single memory address.

The~element $A_{ij}$ of a~matrix $A$ is thus stored at address
\begin{equation}\label{eq-row-major-full-storage}
	\texttt{A} + \big((i - 1) \cdot \texttt{LDA} + (j - 1)\big)\cdot \texttt{size}
\end{equation}
where \texttt{A} is the~address of the~first byte available for storing $A$ and \texttt{size} is the~number of bytes that a~single
entry of $A$ occupies.

\begin{example}
	Example of row major full storage of a~$5\times 5$ matrix with $\texttt{LDA}=6$.
\end{example}

\begin{center}
	\begin{tikzpicture}
		\node at (-0.4,1.8) {$A = \begin{pmatrix}1&2&3&4&5\\6&7&8&9&10\\11&12&13&14&15\\16&17&18&19&20\\21&22&23&24&25\end{pmatrix}$};

		\foreach \idx in {-4.0,-1.6,0.8,3.2}{
			\filldraw[red!50!white] (\idx - 0.2,-0.1) -- (\idx - 0.2,0.0) -- (\idx,-0.2) -- (\idx - 0.1,-0.2);
			\filldraw[red!50!white] (\idx - 0.2,0.1) -- (\idx - 0.2,0.2) -- (\idx + 0.2,-0.2) -- (\idx + 0.1,-0.2);
			\filldraw[red!50!white] (\idx - 0.1,0.2) -- (\idx,0.2) -- (\idx + 0.2,0.0) -- (\idx + 0.2,-0.1);
			\filldraw[red!50!white] (\idx + 0.1,0.2) -- (\idx + 0.2,0.2) -- (\idx + 0.2,0.1);
		}

		\draw (-6.2,0.2) to (-6.2,-0.2);
		\draw (-5.8,0.2) to (-5.8,-0.2);
		\draw (-5.4,0.2) to (-5.4,-0.2);
		\draw (-5.0,0.2) to (-5.0,-0.2);
		\draw (-4.6,0.2) to (-4.6,-0.2);
		\draw (-4.2,0.2) to (-4.2,-0.2);
		\draw (-3.8,0.2) to (-3.8,-0.2);
		\draw (-3.4,0.2) to (-3.4,-0.2);
		\draw (-3.0,0.2) to (-3.0,-0.2);
		\draw (-2.6,0.2) to (-2.6,-0.2);
		\draw (-2.2,0.2) to (-2.2,-0.2);
		\draw (-1.8,0.2) to (-1.8,-0.2);
		\draw (-1.4,0.2) to (-1.4,-0.2);
		\draw (-1.0,0.2) to (-1.0,-0.2);
		\draw (-0.6,0.2) to (-0.6,-0.2);
		\draw (-0.2,0.2) to (-0.2,-0.2);
		\draw (0.2,0.2) to (0.2,-0.2);
		\draw (0.6,0.2) to (0.6,-0.2);
		\draw (1.0,0.2) to (1.0,-0.2);
		\draw (1.4,0.2) to (1.4,-0.2);
		\draw (1.8,0.2) to (1.8,-0.2);
		\draw (2.2,0.2) to (2.2,-0.2);
		\draw (2.6,0.2) to (2.6,-0.2);
		\draw (3.0,0.2) to (3.0,-0.2);
		\draw (3.4,0.2) to (3.4,-0.2);
		\draw (3.8,0.2) to (3.8,-0.2);
		\draw (4.2,0.2) to (4.2,-0.2);
		\draw (4.6,0.2) to (4.6,-0.2);
		\draw (5.0,0.2) to (5.0,-0.2);
		\draw (5.4,0.2) to (5.4,-0.2);

		\draw (-6.4,0.2) to (5.6,0.2);
		\draw (-6.4,-0.2) to (5.6,-0.2);

		\node at (-6.0,0) {\small 1};
		\node at (-5.6,0) {\small 2};
		\node at (-5.2,0) {\small 3};
		\node at (-4.8,0) {\small 4};
		\node at (-4.4,0) {\small 5};

		\node at (-3.6,0) {\small 6};
		\node at (-3.2,0) {\small 7};
		\node at (-2.8,0) {\small 8};
		\node at (-2.4,0) {\small 9};
		\node at (-2.0,0) {\small 10};

		\node at (-1.2,0) {\small 11};
		\node at (-0.8,0) {\small 12};
		\node at (-0.4,0) {\small 13};
		\node at (0.0,0) {\small 14};
		\node at (0.4,0) {\small 15};

		\node at (1.2,0) {\small 16};
		\node at (1.6,0) {\small 17};
		\node at (2.0,0) {\small 18};
		\node at (2.4,0) {\small 19};
		\node at (2.8,0) {\small 20};

		\node at (3.6,0) {\small 21};
		\node at (4.0,0) {\small 22};
		\node at (4.4,0) {\small 23};
		\node at (4.8,0) {\small 24};
		\node at (5.2,0) {\small 25};

		\node at (-6.0,-0.5) {\texttt{A}};
		\node at (-3.6,-0.5) {$\texttt{A}+6$};
		\node at (-1.2,-0.5) {$\texttt{A}+12$};
		\node at (1.2,-0.5) {$\texttt{A}+18$};
		\node at (3.6,-0.5) {$\texttt{A}+24$};

		\draw[thick, DarkGreen] (-6.2,0.4) to (-3.8,0.4);
		\node[DarkGreen] at (-5.0,0.6) {\texttt{LDA}};

		\node at (-6.8,0.0) {\dots};
		\node at (6.0,0.0) {\dots};
	\end{tikzpicture}
\end{center}

\subsubsection{Column major storage}

The~matrix $A$ is stored using the~\textbf{column major full storage scheme} in the~same way as $A^T$ is stored using
the~row major full storage scheme. That is, the~element $A_{ij}$ is stored at address
\begin{equation}\label{eq-column-major-full-storage}
	\texttt{A} + \big((j - 1) \cdot \texttt{LDA} + (i - 1)\big)\cdot \texttt{size}
\end{equation}

Unlike in row major storage, \texttt{LDA} may be lesser than the~number of columns, but it has to be greater than
or equal to the~number of rows.

\begin{example}
	Example of column major full storage of a~$5\times 5$ matrix with $\texttt{LDA}=6$.
\end{example}

\begin{center}
	\begin{tikzpicture}
		\node at (-0.4,1.8) {$A = \begin{pmatrix}1&2&3&4&5\\6&7&8&9&10\\11&12&13&14&15\\16&17&18&19&20\\21&22&23&24&25\end{pmatrix}$};

		\foreach \idx in {-4.0,-1.6,0.8,3.2}{
			\filldraw[red!50!white] (\idx - 0.2,-0.1) -- (\idx - 0.2,0.0) -- (\idx,-0.2) -- (\idx - 0.1,-0.2);
			\filldraw[red!50!white] (\idx - 0.2,0.1) -- (\idx - 0.2,0.2) -- (\idx + 0.2,-0.2) -- (\idx + 0.1,-0.2);
			\filldraw[red!50!white] (\idx - 0.1,0.2) -- (\idx,0.2) -- (\idx + 0.2,0.0) -- (\idx + 0.2,-0.1);
			\filldraw[red!50!white] (\idx + 0.1,0.2) -- (\idx + 0.2,0.2) -- (\idx + 0.2,0.1);
		}

		\draw (-6.2,0.2) to (-6.2,-0.2);
		\draw (-5.8,0.2) to (-5.8,-0.2);
		\draw (-5.4,0.2) to (-5.4,-0.2);
		\draw (-5.0,0.2) to (-5.0,-0.2);
		\draw (-4.6,0.2) to (-4.6,-0.2);
		\draw (-4.2,0.2) to (-4.2,-0.2);
		\draw (-3.8,0.2) to (-3.8,-0.2);
		\draw (-3.4,0.2) to (-3.4,-0.2);
		\draw (-3.0,0.2) to (-3.0,-0.2);
		\draw (-2.6,0.2) to (-2.6,-0.2);
		\draw (-2.2,0.2) to (-2.2,-0.2);
		\draw (-1.8,0.2) to (-1.8,-0.2);
		\draw (-1.4,0.2) to (-1.4,-0.2);
		\draw (-1.0,0.2) to (-1.0,-0.2);
		\draw (-0.6,0.2) to (-0.6,-0.2);
		\draw (-0.2,0.2) to (-0.2,-0.2);
		\draw (0.2,0.2) to (0.2,-0.2);
		\draw (0.6,0.2) to (0.6,-0.2);
		\draw (1.0,0.2) to (1.0,-0.2);
		\draw (1.4,0.2) to (1.4,-0.2);
		\draw (1.8,0.2) to (1.8,-0.2);
		\draw (2.2,0.2) to (2.2,-0.2);
		\draw (2.6,0.2) to (2.6,-0.2);
		\draw (3.0,0.2) to (3.0,-0.2);
		\draw (3.4,0.2) to (3.4,-0.2);
		\draw (3.8,0.2) to (3.8,-0.2);
		\draw (4.2,0.2) to (4.2,-0.2);
		\draw (4.6,0.2) to (4.6,-0.2);
		\draw (5.0,0.2) to (5.0,-0.2);
		\draw (5.4,0.2) to (5.4,-0.2);

		\draw (-6.4,0.2) to (5.6,0.2);
		\draw (-6.4,-0.2) to (5.6,-0.2);

		\node at (-6.0,0) {\small 1};
		\node at (-5.6,0) {\small 6};
		\node at (-5.2,0) {\small 11};
		\node at (-4.8,0) {\small 16};
		\node at (-4.4,0) {\small 21};

		\node at (-3.6,0) {\small 2};
		\node at (-3.2,0) {\small 7};
		\node at (-2.8,0) {\small 12};
		\node at (-2.4,0) {\small 17};
		\node at (-2.0,0) {\small 22};

		\node at (-1.2,0) {\small 3};
		\node at (-0.8,0) {\small 8};
		\node at (-0.4,0) {\small 13};
		\node at (0.0,0) {\small 18};
		\node at (0.4,0) {\small 23};

		\node at (1.2,0) {\small 4};
		\node at (1.6,0) {\small 9};
		\node at (2.0,0) {\small 14};
		\node at (2.4,0) {\small 19};
		\node at (2.8,0) {\small 24};

		\node at (3.6,0) {\small 5};
		\node at (4.0,0) {\small 10};
		\node at (4.4,0) {\small 15};
		\node at (4.8,0) {\small 20};
		\node at (5.2,0) {\small 25};

		\node at (-6.0,-0.5) {\texttt{A}};
		\node at (-3.6,-0.5) {$\texttt{A}+6$};
		\node at (-1.2,-0.5) {$\texttt{A}+12$};
		\node at (1.2,-0.5) {$\texttt{A}+18$};
		\node at (3.6,-0.5) {$\texttt{A}+24$};

		\draw[thick, DarkGreen] (-6.2,0.4) to (-3.8,0.4);
		\node[DarkGreen] at (-5.0,0.6) {\texttt{LDA}};

		\node at (-6.8,0.0) {\dots};
		\node at (6.0,0.0) {\dots};
	\end{tikzpicture}
\end{center}

\subsubsection{Block storage}

When running blocked algorithms on matrices, it can be beneficial to store the~entries of a~single block in a~compact way.
If all blocks not containing entries from the~last row or the~last column of the~matrix (we call those the~main blocks) have the~same size,
and the~other blocks do not have a~larger width or height than the~main blocks, we can utilize the~\textbf{block storage scheme}.
It works by storing the~individual blocks as matrices using the~row major or column major
full storage scheme in contiguous memory areas (that is, $LDA = n$ or $LDA = m$, respectively). The~blocks are stored consecutively,
and their order is determined by the~same column major or row major full storage scheme. It is also possible to use row major storage
for the~individual blocks and the~column major scheme for the~block order, or vice versa.

The~indices of the~block that a~particular entry belongs to can be determined as floor values of the~entry indices divided by the~main block
width or height (both the~block and the~entry indices are assumed to start from 0 in this case).

\begin{example}
	Example of column major block storage of a~$5\times 5$ matrix with $2\times 2$ main blocks.
\end{example}

\begin{center}
	\begin{tikzpicture}
		\node at (-0.4,1.8) {$A = \begin{pmatrix}1&2&3&4&5\\6&7&8&9&10\\11&12&13&14&15\\16&17&18&19&20\\21&22&23&24&25\end{pmatrix}$};

		\draw[red,thick] (-1.65,2.85) -- (-0.5,2.85) -- (-0.5,2.08) -- (-1.65,2.07) -- (-1.65,2.85);
		\draw[DarkBlue,thick] (-1.65,2.02) -- (-0.5,2.02) -- (-0.5,1.22) -- (-1.65,1.22) -- (-1.65,2.02);
		\draw[DarkGreen,thick] (-1.65,1.16) -- (-0.5,1.16) -- (-0.5,0.8) -- (-1.65,0.8) -- (-1.65,1.16);

		\draw[DarkOrange,thick] (-0.25,2.85) -- (0.9,2.85) -- (0.9,2.08) -- (-0.25,2.07) -- (-0.25,2.85);
		\draw[violet,thick] (-0.25,2.02) -- (0.9,2.02) -- (0.9,1.22) -- (-0.25,1.22) -- (-0.25,2.02);
		\draw[Lime,thick] (-0.25,1.16) -- (0.9,1.16) -- (0.9,0.8) -- (-0.25,0.8) -- (-0.25,1.16);

		\draw[Salmon,thick] (1.15,2.85) -- (1.6,2.85) -- (1.6,2.08) -- (1.15,2.07) -- (1.15,2.85);
		\draw[magenta,thick] (1.15,2.02) -- (1.6,2.02) -- (1.6,1.22) -- (1.15,1.22) -- (1.15,2.02);
		\draw[DimGray,thick] (1.15,1.16) -- (1.6,1.16) -- (1.6,0.8) -- (1.15,0.8) -- (1.15,1.16);

		\draw (-5.4,0.2) to (-5.4,-0.2);
		\draw (-5.0,0.2) to (-5.0,-0.2);
		\draw (-4.6,0.2) to (-4.6,-0.2);
		\draw (-4.2,0.2) to (-4.2,-0.2);
		\draw (-3.8,0.2) to (-3.8,-0.2);
		\draw (-3.4,0.2) to (-3.4,-0.2);
		\draw (-3.0,0.2) to (-3.0,-0.2);
		\draw (-2.6,0.2) to (-2.6,-0.2);
		\draw (-2.2,0.2) to (-2.2,-0.2);
		\draw (-1.8,0.2) to (-1.8,-0.2);
		\draw (-1.4,0.2) to (-1.4,-0.2);
		\draw (-1.0,0.2) to (-1.0,-0.2);
		\draw (-0.6,0.2) to (-0.6,-0.2);
		\draw (-0.2,0.2) to (-0.2,-0.2);
		\draw (0.2,0.2) to (0.2,-0.2);
		\draw (0.6,0.2) to (0.6,-0.2);
		\draw (1.0,0.2) to (1.0,-0.2);
		\draw (1.4,0.2) to (1.4,-0.2);
		\draw (1.8,0.2) to (1.8,-0.2);
		\draw (2.2,0.2) to (2.2,-0.2);
		\draw (2.6,0.2) to (2.6,-0.2);
		\draw (3.0,0.2) to (3.0,-0.2);
		\draw (3.4,0.2) to (3.4,-0.2);
		\draw (3.8,0.2) to (3.8,-0.2);
		\draw (4.2,0.2) to (4.2,-0.2);
		\draw (4.6,0.2) to (4.6,-0.2);

		\draw (-5.6,0.2) to (4.8,0.2);
		\draw (-5.6,-0.2) to (4.8,-0.2);

		\node at (-5.2,0) {\small 1};
		\node at (-4.8,0) {\small 6};
		\node at (-4.4,0) {\small 2};
		\node at (-4.0,0) {\small 7};

		\node at (-3.6,0) {\small 11};
		\node at (-3.2,0) {\small 16};
		\node at (-2.8,0) {\small 12};
		\node at (-2.4,0) {\small 17};

		\node at (-2.0,0) {\small 21};
		\node at (-1.6,0) {\small 22};

		\node at (-1.2,0) {\small 3};
		\node at (-0.8,0) {\small 8};
		\node at (-0.4,0) {\small 4};
		\node at (0.0,0) {\small 9};

		\node at (0.4,0) {\small 13};
		\node at (0.8,0) {\small 18};
		\node at (1.2,0) {\small 14};
		\node at (1.6,0) {\small 19};

		\node at (2.0,0) {\small 23};
		\node at (2.4,0) {\small 24};
		
		\node at (2.8,0) {\small 5};
		\node at (3.2,0) {\small 10};

		\node at (3.6,0) {\small 15};
		\node at (4.0,0) {\small 20};
		
		\node at (4.4,0) {\small 25};

		\draw[red, thick] (-5.4,0.4) to (-3.85,0.4);
		\draw[DarkBlue, thick] (-3.75,0.4) to (-2.25,0.4);
		\draw[DarkGreen, thick] (-2.15,0.4) to (-1.45,0.4);

		\draw[DarkOrange, thick] (-1.35,0.4) to (0.15,0.4);
		\draw[violet, thick] (0.25,0.4) to (1.75,0.4);
		\draw[Lime, thick] (1.85,0.4) to (2.55,0.4);

		\draw[Salmon, thick] (2.65,0.4) to (3.35,0.4);
		\draw[Magenta, thick] (3.45,0.4) to (4.15,0.4);
		\draw[DimGray, thick] (4.25,0.4) to (4.6,0.4);

		\node at (-5.2,-0.5) {\texttt{A}};

		\node at (-6.0,0.0) {\dots};
		\node at (5.2,0.0) {\dots};
	\end{tikzpicture}
\end{center}

\subsection{Triangular and symmetric matrix representation}\label{subsec-triangular-symmetric-matrix-representation}

There are two main options for storing a~triangular matrix.

Firstly, we can modify the~full storage scheme to only store and access the~elements above and on the~main diagonal
(for upper triangular matrices) or below and on the~main diagonal (for lower triangular matrices). That leads to less space being used
compared to full storage,
although the~extra free space is created in non-contiguous chunks of bytes. It usually improves the~performance of matrix operations
due to fewer cache misses. The~formula~used to determine the~address of the~$i$,$j$th element of matrix $A$ is identical to
\eqref{eq-row-major-full-storage} or \eqref{eq-column-major-full-storage}, depending on whether the~row major or the~column major
modified full storage scheme is used.

\begin{example}
	Example of modified column major full storage of a~$5\times 5$ lower triangular matrix with $\texttt{LDA}=6$.
\end{example}

\begin{center}
	\begin{tikzpicture}
		\node at (-0.4,1.8) {$A = \begin{pmatrix}1&0&0&0&0\\6&7&0&0&0\\11&12&13&0&0\\16&17&18&19&0\\21&22&23&24&25\end{pmatrix}$};

		\foreach \idx in {-4.0,-3.6,-1.6,-1.2,-0.8,0.8,1.2,1.6,2.0,3.2,3.6,4.0,4.4,4.8}{
			\filldraw[red!50!white] (\idx - 0.2,-0.1) -- (\idx - 0.2,0.0) -- (\idx,-0.2) -- (\idx - 0.1,-0.2);
			\filldraw[red!50!white] (\idx - 0.2,0.1) -- (\idx - 0.2,0.2) -- (\idx + 0.2,-0.2) -- (\idx + 0.1,-0.2);
			\filldraw[red!50!white] (\idx - 0.1,0.2) -- (\idx,0.2) -- (\idx + 0.2,0.0) -- (\idx + 0.2,-0.1);
			\filldraw[red!50!white] (\idx + 0.1,0.2) -- (\idx + 0.2,0.2) -- (\idx + 0.2,0.1);
		}

		\draw (-6.2,0.2) to (-6.2,-0.2);
		\draw (-5.8,0.2) to (-5.8,-0.2);
		\draw (-5.4,0.2) to (-5.4,-0.2);
		\draw (-5.0,0.2) to (-5.0,-0.2);
		\draw (-4.6,0.2) to (-4.6,-0.2);
		\draw (-4.2,0.2) to (-4.2,-0.2);
		\draw (-3.8,0.2) to (-3.8,-0.2);
		\draw (-3.4,0.2) to (-3.4,-0.2);
		\draw (-3.0,0.2) to (-3.0,-0.2);
		\draw (-2.6,0.2) to (-2.6,-0.2);
		\draw (-2.2,0.2) to (-2.2,-0.2);
		\draw (-1.8,0.2) to (-1.8,-0.2);
		\draw (-1.4,0.2) to (-1.4,-0.2);
		\draw (-1.0,0.2) to (-1.0,-0.2);
		\draw (-0.6,0.2) to (-0.6,-0.2);
		\draw (-0.2,0.2) to (-0.2,-0.2);
		\draw (0.2,0.2) to (0.2,-0.2);
		\draw (0.6,0.2) to (0.6,-0.2);
		\draw (1.0,0.2) to (1.0,-0.2);
		\draw (1.4,0.2) to (1.4,-0.2);
		\draw (1.8,0.2) to (1.8,-0.2);
		\draw (2.2,0.2) to (2.2,-0.2);
		\draw (2.6,0.2) to (2.6,-0.2);
		\draw (3.0,0.2) to (3.0,-0.2);
		\draw (3.4,0.2) to (3.4,-0.2);
		\draw (3.8,0.2) to (3.8,-0.2);
		\draw (4.2,0.2) to (4.2,-0.2);
		\draw (4.6,0.2) to (4.6,-0.2);
		\draw (5.0,0.2) to (5.0,-0.2);
		\draw (5.4,0.2) to (5.4,-0.2);

		\draw (-6.4,0.2) to (5.6,0.2);
		\draw (-6.4,-0.2) to (5.6,-0.2);

		\node at (-6.0,0) {\small 1};
		\node at (-5.6,0) {\small 6};
		\node at (-5.2,0) {\small 11};
		\node at (-4.8,0) {\small 16};
		\node at (-4.4,0) {\small 21};

		\node at (-3.2,0) {\small 7};
		\node at (-2.8,0) {\small 12};
		\node at (-2.4,0) {\small 17};
		\node at (-2.0,0) {\small 22};

		\node at (-0.4,0) {\small 13};
		\node at (0.0,0) {\small 18};
		\node at (0.4,0) {\small 23};

		\node at (2.4,0) {\small 19};
		\node at (2.8,0) {\small 24};

		\node at (5.2,0) {\small 25};

		\node at (-6.0,-0.5) {\texttt{A}};
		\node at (-3.6,-0.5) {$\texttt{A}+6$};
		\node at (-1.2,-0.5) {$\texttt{A}+12$};
		\node at (1.2,-0.5) {$\texttt{A}+18$};
		\node at (3.6,-0.5) {$\texttt{A}+24$};

		\draw[thick, DarkGreen] (-6.2,0.4) to (-3.8,0.4);
		\node[DarkGreen] at (-5.0,0.6) {\texttt{LDA}};

		\node at (-6.8,0.0) {\dots};
		\node at (6.0,0.0) {\dots};
	\end{tikzpicture}
\end{center}

The~second option is to use the~\textbf{packed storage scheme}. In this thesis, only the~\textbf{column major packed storage scheme}
will be introduced, although there is a~row major variant of this storage format. For upper triangular matrices, we store elements above or on the~main diagonal
from every column in a~contiguous vector ordered by their row index. Those vectors are then stored consecutively in an
ascending order determined by size.

The~$i,j$th element of $A$, where $i\le j$, is therefore stored on the~address
\begin{equation}\label{eq-packed-storage-upper}
	\texttt{A} + \Big((i-1) + \frac{j\cdot(j-1)}{2}\Big)\cdot\texttt{size}
\end{equation}
where \texttt{A} and \texttt{size} have the~same meaning as in the~previous section.

For lower triangular matrices, we only store the~part of each column containing the~elements below or on the~main diagonal.
Those vectors are then stored consecutively from largest to smallest. For $i\ge j$, we store the~entry $A_{ij}$ of a~lower triangular 
matrix $A\in\R^{n,n}$ on address
\begin{equation}\label{eq-packed-storage-lower}
	\texttt{A} + \Big((i-1) + \frac{(j-1)\cdot(2\cdot n-j)}{2}\Big)\cdot\texttt{size}
\end{equation}

\begin{example}
	Example of column major packed storage of a~$5\times 5$ lower triangular matrix.
\end{example}

\begin{center}
	\begin{tikzpicture}
		\node at (-0.4,1.8) {$A = \begin{pmatrix}1&0&0&0&0\\6&7&0&0&0\\11&12&13&0&0\\16&17&18&19&0\\21&22&23&24&25\end{pmatrix}$};

		\draw (-3.4,0.2) to (-3.4,-0.2);
		\draw (-3.0,0.2) to (-3.0,-0.2);
		\draw (-2.6,0.2) to (-2.6,-0.2);
		\draw (-2.2,0.2) to (-2.2,-0.2);
		\draw (-1.8,0.2) to (-1.8,-0.2);
		\draw (-1.4,0.2) to (-1.4,-0.2);
		\draw (-1.0,0.2) to (-1.0,-0.2);
		\draw (-0.6,0.2) to (-0.6,-0.2);
		\draw (-0.2,0.2) to (-0.2,-0.2);
		\draw (0.2,0.2) to (0.2,-0.2);
		\draw (0.6,0.2) to (0.6,-0.2);
		\draw (1.0,0.2) to (1.0,-0.2);
		\draw (1.4,0.2) to (1.4,-0.2);
		\draw (1.8,0.2) to (1.8,-0.2);
		\draw (2.2,0.2) to (2.2,-0.2);
		\draw (2.6,0.2) to (2.6,-0.2);

		\draw (-3.6,0.2) to (2.8,0.2);
		\draw (-3.6,-0.2) to (2.8,-0.2);

		\node at (-3.2,0) {\small 1};
		\node at (-2.8,0) {\small 6};
		\node at (-2.4,0) {\small 11};
		\node at (-2.0,0) {\small 16};
		\node at (-1.6,0) {\small 21};

		\node at (-1.2,0) {\small 7};
		\node at (-0.8,0) {\small 12};
		\node at (-0.4,0) {\small 17};
		\node at (0.0,0) {\small 22};

		\node at (0.4,0) {\small 13};
		\node at (0.8,0) {\small 18};
		\node at (1.2,0) {\small 23};

		\node at (1.6,0) {\small 19};
		\node at (2.0,0) {\small 24};

		\node at (2.4,0) {\small 25};

		\node at (-3.2,-0.5) {\texttt{A}};

		\node at (-4.0,0.0) {\dots};
		\node at (3.2,0.0) {\dots};
	\end{tikzpicture}
\end{center}

Symmetric and hermitian matrices can utilize the~same storage schemes as triangular matrices, as the~entries below the~diagonal
are fully defined by the~corresponding entries above the~diagonal, so only one of these parts of a~matrix needs to be stored.
However, matrix operations on symmetric and Hermitian matrices have to be implemented separately from triangular matrix operations.

\subsection{Band matrix representation}

\begin{definition}
	A~band matrix with $kl$ subdiagonals and $ku$ superdiagonals is a~matrix where for each $i,j\in\lbrace1,\dots,n\rbrace$:
	$$j-i>ku\Rightarrow A_{ij}=0 \quad \text{and} \quad i-j>kl\Rightarrow A_{ij}=0$$
\end{definition}

In the~\textbf{band storage scheme}, we store a~band matrix $A$ with $ku$ superdiagonals and $kl$ subdiagonals as a~$(kl+ku+1)\times n$
matrix $AB$ using the~column major full storage scheme with $\texttt{LDAB}\ge ku+kl+1$, where the~entries $j-ku$ to $j+kl$ of the
$j$th column of $A$ are stored as the~$j$th column of $AB$ (the~entries whose indices are out of bounds are left out).

In summary, the~elements $A_{ij}$ for $j-i\le ku$ and $i-j\le kl$ is stored in the~address
\begin{equation}
	\texttt{A} + \big(i + ku - j + (j + 1)\cdot \texttt{LDAB}\big)\cdot\texttt{size}
\end{equation}
\newpage

\begin{example}
	Example of band storage of a~$5\times 5$ band triangular matrix with\\$ku=2,kl=1,\texttt{LDAB}=6$.
\end{example}

\begin{center}
	\begin{tikzpicture}
		\node at (0.0,1.8) {$A = \begin{pmatrix}1&2&0&0&0\\6&7&8&9&0\\0&12&13&0&15\\0&0&18&19&20\\0&0&0&0&25\end{pmatrix}$};

		\foreach \idx in {-4.4,-4.0,-2.4,-2.0,0.0,2.4,4.4}{
			\filldraw[red!50!white] (\idx - 0.2,-0.1) -- (\idx - 0.2,0.0) -- (\idx,-0.2) -- (\idx - 0.1,-0.2);
			\filldraw[red!50!white] (\idx - 0.2,0.1) -- (\idx - 0.2,0.2) -- (\idx + 0.2,-0.2) -- (\idx + 0.1,-0.2);
			\filldraw[red!50!white] (\idx - 0.1,0.2) -- (\idx,0.2) -- (\idx + 0.2,0.0) -- (\idx + 0.2,-0.1);
			\filldraw[red!50!white] (\idx + 0.1,0.2) -- (\idx + 0.2,0.2) -- (\idx + 0.2,0.1);
		}


		\draw (-4.6,0.2) to (-4.6,-0.2);
		\draw (-4.2,0.2) to (-4.2,-0.2);
		\draw (-3.8,0.2) to (-3.8,-0.2);
		\draw (-3.4,0.2) to (-3.4,-0.2);
		\draw (-3.0,0.2) to (-3.0,-0.2);
		\draw (-2.6,0.2) to (-2.6,-0.2);
		\draw (-2.2,0.2) to (-2.2,-0.2);
		\draw (-1.8,0.2) to (-1.8,-0.2);
		\draw (-1.4,0.2) to (-1.4,-0.2);
		\draw (-1.0,0.2) to (-1.0,-0.2);
		\draw (-0.6,0.2) to (-0.6,-0.2);
		\draw (-0.2,0.2) to (-0.2,-0.2);
		\draw (0.2,0.2) to (0.2,-0.2);
		\draw (0.6,0.2) to (0.6,-0.2);
		\draw (1.0,0.2) to (1.0,-0.2);
		\draw (1.4,0.2) to (1.4,-0.2);
		\draw (1.8,0.2) to (1.8,-0.2);
		\draw (2.2,0.2) to (2.2,-0.2);
		\draw (2.6,0.2) to (2.6,-0.2);
		\draw (3.0,0.2) to (3.0,-0.2);
		\draw (3.4,0.2) to (3.4,-0.2);
		\draw (3.8,0.2) to (3.8,-0.2);
		\draw (4.2,0.2) to (4.2,-0.2);
		\draw (4.6,0.2) to (4.6,-0.2);

		\draw (-4.8,0.2) to (4.8,0.2);
		\draw (-4.8,-0.2) to (4.8,-0.2);

		\node at (-3.6,0) {\small 0};
		\node at (-3.2,0) {\small 9};
		\node at (-2.8,0) {\small 15};

		\node at (-1.6,0) {\small 2};
		\node at (-1.2,0) {\small 8};
		\node at (-0.8,0) {\small 0};
		\node at (-0.4,0) {\small 20};

		\node at (0.4,0) {\small 1};
		\node at (0.8,0) {\small 7};
		\node at (1.2,0) {\small 13};
		\node at (1.6,0) {\small 19};
		\node at (2.0,0) {\small 25};

		\node at (2.8,0) {\small 6};
		\node at (3.2,0) {\small 12};
		\node at (3.6,0) {\small 18};
		\node at (4.0,0) {\small 0};

		\node at (-4.4,-0.5) {\texttt{A}};
		\node at (-2.0,-0.5) {$\texttt{A}+6$};
		\node at (0.4,-0.5) {$\texttt{A}+12$};
		\node at (2.8,-0.5) {$\texttt{A}+18$};

		\draw[thick, DarkGreen] (-4.6,0.4) to (-2.2,0.4);
		\node[DarkGreen] at (-3.4,0.6) {\texttt{LDA}};

		\node at (-5.2,0.0) {\dots};
		\node at (5.2,0.0) {\dots};
	\end{tikzpicture}
\end{center}