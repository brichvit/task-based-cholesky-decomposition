% This is a source file for the Task-based runtime systems chapter of Vít Břichňáč's bachelor thesis, Task-based Cholesky decomposition

\chapter{Task-based runtime systems}

In scientific computations, programs are rarely ever sequential. Most often, calculations are performed on several CPU cores, multiple
different CPUs or even multiple processing units with different architectures. As such, numerical programs have to be \textbf{parallelized}.

One way to parallelize existing programs is to utilize standard threading libraries, such as the~\texttt{pthread} library on UNIX systems.
The~disadvantage to this approach is that the~programmer is solely responsible for managing dependencies between sections of code
(e.g., making sure that a~function is run only after another function is already finished).
Such code is thus very prone to race conditions -- program errors which arise when the~same memory
section is either written to by multiple threads simultaneously or read by one or more threads while it is written to by another thread.
These libraries usually provide tools for synchronization, such as mutual exclusion locks, semaphores, atomic data types or barriers,
which can theoretically be used even for programs with high amounts of dependencies. That approach, however, leads to code that is difficult
to understand and therefore difficult to debug.

For many programs with large amounts of dependencies, we can split the~code into sections and describe how each of those code sections
accesses shared memory areas (whether it reads the~memory, writes to the~memory, or both) and what other code sections need to be finished
before this code section can be run. An~object that holds this information about a~particular code section is called a~\textbf{task}. The~memory
sections that the~code section of a~certain task accesses, along with the~corresponding data access modes, are called its \textbf{data dependencies}
(or \textbf{memory dependencies}) and the~tasks which need to be executed before a~certain task is run are called its \textbf{task dependencies} \cite{HPC2NIntroduction}.

The~programmer can either deduce the~task dependencies from the~data dependencies and the~sequential code themselves and express them explicitly,
or they can describe the~data dependencies to a~\textbf{task-based runtime system}, which then implicitly derives the~task dependencies from
the~data dependencies and the~order that the~tasks were added in \cite{HPC2NIntroduction}.

\section{Task-based Cholesky decomposition algorithm}

The~parallel Algorithm \ref{alg-parallelized-blocked-cholesky} can be further improved using task-based programming. For example,
if the~value $A_{(j+1)(j+1)}$ is already updated on line \ref{parallelized-blocked-cholesky-gemm-step} but some of the~other values
are still being recalculated, the~algorithm waits for those computations to finish. The~computation of the~value $L_{(j+1)(j+1)}$ could,
however, be launched on the~core that $A_{(j+1)(j+1)}$ was recalculated on. As such, we can achieve better parallelization by describing
the~data dependencies and letting the~runtime system implicitly construct the~task dependencies:\\

\begin{algorithm} [H]
	\label{alg-task-based-blocked-cholesky}
	\caption{Task-based Cholesky decomposition (variant of \cite{LinearAlgebraSoftware2016}, Algorithm 3)}
	\KwData{Symmetric positive definite matrix $A\in\R^{n,n}$}
	\KwResult{Lower triangular matrix $L$ with positive diagonal entries such that $A = LL^T$}
	\For{$j\gets 1;j\le nb;j\gets j+1$}{
		Insert task $A_{jj}\gets \texttt{POTF2}(A_{jj})$ with data dependency $A_{jj}$ (R + W)\\
		\For{$i\gets j+1;i\le nb;i\gets i+1$}{
			Insert task $A_{ij}\gets A_{ij}\cdot \big((A_{jj})^T\big)^{-1}$ with data deps. $L_{jj}$ (R) and $A_{ij}$ (R + W)\\
		}
		\For{$k\gets j+1;k\le nb;k\gets k+1$}{
			Insert task $A_{kk}\gets A_{kk} - A_{kj}\cdot (A_{kj})^T$ with data deps. $A_{kj}$ (R) and $A_{kk}$ (R + W)\\
			\For{$i\gets k+1;i\le nb;i\gets i+1$}{
				Insert task $A_{ik}\gets A_{ik} - A_{ij}\cdot (A_{kj})^T$ with data dependencies $A_{ij}$, $A_{kj}$ (R) and $A_{ik}$ (R + W)\\
			}
		}
	}
	\Return the~lower triangular part of $A$
\end{algorithm}

\noindent This algorithm was derived from the~in-place variant of Algorithm \ref{alg-improved-blocked-cholesky}.

\section{Task graphs}

By considering tasks as vertices of an~oriented graph and the~task dependencies as edges, we can characterize the~task-based program by a
\textbf{task graph}. Task graphs can also be called \textbf{directed acyclic graphs} or \textbf{DAG}s for short, meaning graphs that contain no cycles.
In the~context of task-based runtime systems, we define a~cycle in a~task graph of a~program $P$ as a~finite sequence of tasks
$a_1,a_2,\dots,a_n$ in $P$ where $a_i$ is a~task dependency of $a_{i+1}$ for all $i\in\lbrace1,\dots,n-1\rbrace$ and $a_n$ is a~task dependency of $a_1$.
The~following theorem shows that the~task graph of a~finite program is always acyclic:

\begin{theorem}
	Let $P$ be a~task-based program. If all tasks of $P$ finish in a~finite amount of time, then the~task graph of $P$ is acyclic.
\end{theorem}

\begin{proof}
	We will prove the~contraposition of the~statement.

	Suppose that a~task-based program $P$ contains a~cycle, or equivalently, there are tasks $a_1,a_2,\dots,a_n$ in $P$
	such that $a_i$ is a~task dependency of $a_{i+1}$ for all $i\in\lbrace1,\dots,n-1\rbrace$ and $a_n$ is a~task dependency of $a_1$.
	Using induction, we can prove that the~task $a_1$ has to be finished before the~runtime system executes the~task $a_n$:
	\begin{description}
		\item[Base step] $a_1$ is a~task dependency of $a_2$, so $a_1$ has to be finished before the~system starts $a_2$
		\item[Inductive step] Suppose that $a_i$ can be started only after $a_1$ is finished for any $i\in\lbrace2,\dots,n-1\rbrace$.
			                 Due to $a_i$ being a~task dependency of $a_{i+1}$, $a_{i+1}$ can be run only after $a_i$ is finished, which,
							 following our supposition, is only after $a_1$ is finished.
	\end{description}

	\noindent We have thus established using the~induction principle that $a_n$ can only be executed after $a_1$ is finished. However, since $a_n$ is a
	task dependency of $a_1$, $a_1$ can only be run after $a_n$ is finished. Running either $a_1$ or $a_n$ (or both at once) would break
	at least one of those two conditions, therefore $a_1$ and $a_n$ will not be launched at all. Hence, $P$ never finishes.
\end{proof}

To provide an~example, we present the~task graph of Algorithm \ref{alg-task-based-blocked-cholesky} for an~input matrix with $3\times 3$
blocks:

\begin{example}
	Task graph of Algorithm \ref{alg-task-based-blocked-cholesky} for an~input matrix with $3\times 3$ blocks.
\end{example}

\includegraphics[width=\textwidth, angle=0]{3x3dag.pdf}

\section{Task states and scheduling}

This section follows \cite{HPC2NIntroduction}.

At any moment when running a~task-based program, each task can be in exactly one of the~following states:
\begin{description}
	\item[Submitted] One or more task dependencies of this task are not finished yet
	\item[Ready] All of the~task dependencies are finished, but the~task has not been executed yet
	\item[Active] The~task is being executed at the~moment
	\item[Completed] The~execution of the~task has been finished.
\end{description}

The~runtime system stores a~list of all tasks that are ready called a~\textbf{ready pool}. Once an~active task $a$ is complete, the~system
iterates through all submitted tasks that have $a$ as a~dependency and marks the~dependency as satisfied -- this may be implemented as simply
removing the~dependency from the~task dependency list. If any of those submitted tasks have no unsatisfied dependencies, they are moved to
the~ready pool. Then, the~system selects a~task from the~ready pool and executes it on a~core that is currently free.
The~component of the~runtime system that selects tasks for execution is called the~\textbf{scheduler}. Most schedulers allow
programmers to influence the~task selection process by setting \textbf{task priorities}.

\section{Task granularity}\label{sec-task-granularity}

When designing parallel programs, it is crucial for the~programmer to choose the~right granularity of the~tasks -- i.e., how many tasks
will the~problem be split into and how long will those tasks take to execute.

If we have a~small amount of tasks that take long to execute, the~CPU may not be utilizing all of its cores at all times, and so the
program might take longer to finish.
On the~contrary, if we have a~large amount of short tasks, it is going to be more difficult (and more time consuming) for the~runtime
system to manage all of those tasks, their dependencies and priorities. So while the~workload may be better distributed among the
CPU cores, the~system is going to require more time to manage the~tasks, so the~overall time may increase as well \cite{HPC2NIntroduction}.

To address these issues, we usually run the~task-based program for various different task granularities and select the~case with the~
lowest overall execution time. In our case of the~blocked Cholesky decomposition algorithm, task granularity is controlled by the~block
size -- smaller blocks lead to a~higher amount of smaller tasks and vice versa. 

\section{Trace}

A~trace of a~runtime program is a~visualization of the~individual tasks' lifecycles. It contains a~horizontal timeline for each CPU core,
and each of the~colored areas on one of the~timelines represents the~time that a~particular task was active. Tasks may be colored according
to their name.

A~trace may help us spot unwanted dependencies or tune the~task granularity:
\begin{itemize}
	\item If there is a~long time period at the~end of the~program where not all cores are being used, it may be better to split the~program
	into more granular tasks
	\item If there are many tasks that are switching rapidly, it may be better to join the~tasks
	into less granular ones
\end{itemize}

\begin{example}
	A~trace of Algorithm \ref{alg-task-based-blocked-cholesky} for an~input matrix with $3\times 3$ blocks on a~machine with
	two cores.
\end{example}

\includegraphics[width=0.9\textwidth, angle=0]{3x3trace.pdf}

\section{OpenMP}

OpenMP is an~interface for programming shared-memory parallel systems published in 1997 \cite{OpenMP2007}.
Recently, extensions have been released to also provide an~interface for GPU computing functionalities. OpenMP can be used in
C/C++ as a~set of preprocessor pragmas, all of which have the~form \texttt{\#pragma omp <construct> \lbrack options\rbrack}. OpenMP
also has a~Fortran interface, but the~syntax differs from the~C/C++ interface.

In task-based programming, the~most important OpenMP construct is the~\texttt{task} construct. A~\texttt{task} construct followed by a
brace-enclosed code block creates a~task, which is either executed immediately, or delayed until it is scheduled for execution.
Data dependencies of the~task can be specified using the~\texttt{depend} clause and the~priority of a~task can be set using the
\texttt{priority} clause. The~variable access rules can be specified using the~following clauses \cite{OpenMPRefBook}:
\begin{description}
	\item[private] Creates a~new variable for each thread and ignores the~variable with the~same name in the~outer scope,
	if one exists.
	\item[firstprivate] Creates a~new variable for each thread with the~same value as the~eponymous variable in the~outer scope.
	\item[shared] Shares the~variable in the~outer scope across all threads.
\end{description}

Another important construct is \texttt{barrier}, which waits for all previously created tasks to finish. In case we want
to wait for child tasks only (i.e., tasks that were created inside another task), the~\texttt{taskwait} construct can be used
\cite{OpenMPRefBook}.

OpenMP also offers many constructs outside of the task-based programming domain. A~code block can be run in the~fork-join model by multiple threads
using the~\texttt{parallel} construct, though there is no option to specify the~dependencies. The~\texttt{single} construct may then
be used to tell OpenMP to execute a~code block inside a~\texttt{parallel} region by only one thread. If we want to execute a~code block
by a~single thread at a~time, we can use the~\texttt{critical} construct. A~for loop in C/C++ can be parallelized using a~\texttt{loop} construct
\cite{OpenMPRefBook}.

As OpenMP is an~interface, it cannot be distributed as a~standard C/C++ or Fortran library, but compiler vendors may choose to implement
the~OpenMP preprocessor pragmas in their compiler. Examples of widely used compilers that implement the~OpenMP interface are the
\texttt{gcc} compiler developed by the~GNU initiative, the~\texttt{icc} compiler by Intel Corporation and the~\texttt{clang} compiler,
which is a~part of the~LLVM project \cite{OpenMPCompilersAndTools}.

\section{StarPU}

\textbf{StarPU} is a~task-based runtime system developed at the~University of Bordeaux and released in 2009. The~main motivation
behind its development was to design a~high-level, easy to use, portable runtime system for heterogeneous architecture systems.
Reference \cite{AugonnetThibaltNamystWacreiner2009} states that the~most successful interface for programming heterogeneous architectures prior to the~development of StarPU was OpenCL,
but it was too low level to be considered a~runtime system.

Data used by StarPU tasks has to be registered in the~system using a~function, which then returns a~\textbf{handle}. A~handle is a~structure
that contains information about a~data block that may be specified as a~data dependency of a~task.
Data blocks have different types in StarPU, for example, a~\textbf{vector} data block represents a~contiguous array of elements and the
\textbf{matrix} data block represents a~matrix stored in the~row major full storage format (see Subsection \ref{subsubsec-row-major-full-storage}).
There are also data block types for some of the~sparse storage formats described in Section \ref{sec-sparse-matrix-storage}
\cite{StarPUDocumentation}.
The~ability to run tasks across devices with different architectures is accomplished by using \textbf{codelets},
which can be thought of as a~structure similar to tasks, with the~difference that multiple implementations can be provided for the~same
task -- usually one implementation per device architecture. That allows programmers to utilize different libraries or even different
programming languages for each architecture that the~task may be run on. Every codelet also specifies the~data dependencies, that is,
the~data blocks that the~code accesses along with the~corresponding data access modes (read, write or read + write).
StarPU also manages the~data transfers between devices in a~way that all of the~data blocks that a~task is dependent on are present
in the~memory of the~device that runs the~task.

StarPU has an~interface for the~C/C++, Java, Python and Fortran programming languages. Using the~KStar source-to-source compiler,
programmers can seamlessly translate a~program that uses OpenMP pragmas to a~program that uses StarPU \cite{StarPUHomepage}.

Unlike OpenMP, StarPU is also well suited for distributed memory systems, as it integrates very well with the~Message Passing Interface
(MPI). Since version 1.2, it also supports data offloading to disks. Several tools exist to visualize the~task graphs of StarPU programs
(Temanejo, ViTe) or to generate a~trace (the~FxT library) \cite{StarPUHomepage}.

