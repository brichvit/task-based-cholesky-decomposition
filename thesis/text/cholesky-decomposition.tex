% This is a source file for the Cholesky decomposition chapter of Vít Břichňáč's bachelor thesis, Task-based Cholesky decomposition

\chapter{Cholesky decomposition}

Lay, Lay and McDonald \cite{LayLayMcDonald2014} define a~\textbf{matrix factorization} (also called a~\textbf{matrix decomposition}) as an~equation that represents a~matrix as
a~product of two or more matrices, which we call \textbf{factors}. We typically want the~factors to have specific properties,
such as being upper or lower triangular, diagonal or orthogonal \cite{LA2}. Optimized routines that perform those factorizations
can then be used to solve other problems efficiently, for example:

\begin{itemize}
	\item The~LU, Cholesky and QR factorizations are used to solve systems of linear equations
	\item The~QR factorization is used in analysis of variance (ANOVA) methods
	\item The~singular value decomposition (SVD) is used in principal component analysis\footnote{A~linear method of dimensionality reduction used in machine learning.}
	\item The~spectral decomposition is used in locally linear embedding\footnote{A~non-linear method of dimensionality reduction used in machine learning.}
\end{itemize}

One of the~most important factorizations is the~Cholesky factorization, which is defined for symmetric positive definite matrices.
Along with the~LU and QR factorizations, it is considered to be one of the~three elementary matrix factorizations
(also called the~``Three Amigos'') used in numerical linear algebra. 

\begin{definition}[\cite{TrefethenBau2022}]
	A~\textbf{Cholesky factorization} (or \textbf{Cholesky decomposition}) of a~symmetric positive definite matrix $A\in\R^{n,n}$
	is an~equation that has the~form

	\begin{equation}\label{eq-cholesky-decomposition}
		A = LL^T
	\end{equation}

	\noindent where $L$ is a~lower triangular matrix with $L_{ii}>0$ for all $i\in\lbrace1,\dots,n\rbrace$.
\end{definition}

By finding the~Cholesky factorization of a~symmetric positive definite matrix $A\in\R^{n,n}$, we typically mean finding the~factor $L$,
as the~other factor $L^{T}$ can be constructed from $L$ trivially.

In general, the~Cholesky decomposition can be defined for Hermitian matrices
(matrices with complex entries that are equal to the~transpose of their complex conjugate). In this thesis, however, we limit ourselves
to matrices with real-valued entries, for which the~complex conjugate transpose equals the~transpose.

In some definitions of the~Cholesky factorization (including the~definition in Reference \cite{TrefethenBau2022}), the~defining equation has the~form

\begin{equation}\label{eq-cholesky-decomposition-alternative}
	A = U^TU,
\end{equation}

\noindent where $U$ is an~upper triangular matrix with positive entries on the~main diagonal. Both of these definitions are equivalent --
if we know the~matrix $U$ used in \eqref{eq-cholesky-decomposition-alternative},
we can obtain the~matrix $L$ from \eqref{eq-cholesky-decomposition} as $L=U^T$, since

\begin{equation*}
	A = A^T = (U^TU)^T = U^T(U^T)^T = LL^T.
\end{equation*}

We will now show that every symmetric positive definite matrix has a~Cholesky decomposition:

\begin{lemma}\label{lemma-cholesky-decomposition-l-positive-diagonal}
	Let $A\in\R^{n,n}$ be a~symmetric positive definite matrix and $L\in\R^{n,n}$ a~lower triangular matrix
	such that $A=LL^T$. Then $L_{ii}\neq0$ for all $i\in\lbrace1,\dots,n\rbrace$.
\end{lemma}

\begin{proof}
	Suppose that $L_{ii} = 0$ for some $i\in\lbrace1,\dots,n\rbrace$. Then $L$ is singular as per Theorem \ref{theorem-determinant} since
	$\det L = 0$. Following Theorem \ref{theorem-nonsingular-transpose}, $L^T$ is also singular. Thus, according to Theorem
	\ref{theorem-nonsingular-property}, there is a~non-zero vector $x\in\R^{n,1}$ such that:

	\begin{align*}
		\|L^Tx\| &= 0\\
		L^Tx &= \theta\\
		LL^Tx &= \theta\\
		Ax &= \theta\\
		x^TAx &= 0
	\end{align*}

	\noindent which is contradictory to the~assumption that $A$ is positive definite.
\end{proof}

\begin{theorem}\label{theorem-existence-uniqueness-cholesky}
	Let $A\in\R^{n,n}$ be a~symmetric positive definite matrix. Then $A$ has a~unique Cholesky decomposition, i.e., there exists
	precisely one lower triangular matrix $L\in\R^{n,n}$ with positive entries on the~main diagonal such that $A=LL^T$.
\end{theorem}

\begin{proof}
	We will show both the~existence and the~uniqueness of $L$ by finding an~explicit formula for all of its entries.

	For all $i,j\in\lbrace1,\dots,n\rbrace, i\ge j$, we have:

	$$A_{ij} = \sum_{k=1}^{n}L_{ik}(L^T)_{kj} = \sum_{k=1}^{n}L_{ik}L_{jk}$$

	\noindent Since $L$ is lower triangular and $i\ge j$, we have $L_{ik}L_{jk}=0$ for all $k>j$:
	
	\begin{equation}\label{eq-cholesky-entry-generic}
		A_{ij} = \sum_{k=1}^{j}L_{ik}L_{jk}.
	\end{equation}

	\noindent If $i=j$, we then obtain:
	\begin{align*}
		A_{ii} &= \sum_{k=1}^{i}L_{ik}^2\\
		A_{ii} &= L_{ii}^2 + \sum_{k=1}^{i-1}L_{ik}^2\\
		L_{ii}^2 &= A_{ii} - \sum_{k=1}^{i-1}L_{ik}^2\\
		|L_{ii}| &= \sqrt{A_{ii} - \sum_{k=1}^{i-1}L_{ik}^2}
	\end{align*}

	\noindent Since the~diagonal entries of $L$ are required by definition to be positive, we set:

	\begin{equation}\label{eq-cholesky-entry-diagonal}
		L_{ii} = \sqrt{A_{ii} - \sum_{k=1}^{i-1}L_{ik}^2}
	\end{equation}

	We can see that $L_{ii}>0$, since a~square root of a~non-negative number is non-negative and
	we know that $L_{ii}\neq0$ from Lemma \ref{lemma-cholesky-decomposition-l-positive-diagonal}.

	Following \eqref{eq-cholesky-entry-generic} for $i>j$, we obtain:
	\begin{align*}
		A_{ij} &= \sum_{k=1}^{j}L_{ik}L_{jk}\\
		A_{ij} &= L_{ij}L_{jj} + \sum_{k=1}^{j-1}L_{ik}L_{jk}\\
		L_{ij}L_{jj} &= A_{ij} - \sum_{k=1}^{j-1}L_{ik}L_{jk}\\
	\end{align*}

	\begin{equation}\label{eq-cholesky-entry-non-diagonal}
		L_{ij} = \frac{A_{ij} - \sum_{k=1}^{j-1}L_{ik}L_{jk}}{L_{jj}}
	\end{equation}

	The~correctness of the~last line follows from Lemma \ref{lemma-cholesky-decomposition-l-positive-diagonal}.

\end{proof}

\section{Unblocked algorithm}

In the~proof of Theorem \ref{theorem-existence-uniqueness-cholesky}, we found the~explicit formulas for
all entries of the~matrix $L$. Both of the~formulas \eqref{eq-cholesky-entry-diagonal} and \eqref{eq-cholesky-entry-non-diagonal}
only require the~entries in columns $1$ to $j-1$ to be known when calculating an~entry in the~$j$th column.
Thus, we can calculate the~Cholesky decomposition using formulas \eqref{eq-cholesky-entry-diagonal} and \eqref{eq-cholesky-entry-non-diagonal}
in a~column-wise order:

\begin{algorithm} [H]
	\label{alg-unblocked-cholesky}
	\caption{Unblocked Cholesky decomposition (variant of \cite{TrefethenBau2022}, Algorithm 23.1)}
	\KwData{Symmetric positive definite matrix $A\in\R^{n,n}$}
	\KwResult{Lower triangular matrix $L$ with positive diagonal entries such that $A = LL^T$}
	$L\gets\Theta$\label{unblocked-cholesky-l-initialization}\\
	\For{$j\gets 1;j\le n;j\gets j+1$}{
		\label{unblocked-cholesky-outer-loop}
		$L_{jj}\gets \sqrt{A_{jj} - \sum_{k=1}^{j-1}L_{jk}^2}$\label{unblocked-cholesky-diagonal-step}\\
		\For{$i\gets j+1;j\le n;i\gets i+1$}{
			\label{unblocked-cholesky-inner-loop}
			$L_{ij}\gets (A_{ij} - \sum_{k=1}^{j-1}L_{ik}L_{jk})/L_{jj}$\label{unblocked-cholesky-non-diagonal-step}\\
		}
	}
	\Return L
\end{algorithm}

Note that when calculating the~entry $L_{jj}$ on line \ref{unblocked-cholesky-diagonal-step}, we only use $A_{jj}$ and the~entries of $L$
that are already calculated. Similarly, we only access $A_{ij}$ and the~already calculated entries of $L$ when calculating $L_{ij}$ on line
\ref{unblocked-cholesky-non-diagonal-step}. Thus, if the~programmer does not require the~matrix $A$ to remain unchanged,
we can perform all of the~computations in place -- that is, remove line \ref{unblocked-cholesky-l-initialization} and change all
memory accesses to $L$ to memory accesses to $A$ instead. Since we do not overwrite the~entries above the~diagonal, the~returned matrix
will likely not be lower triangular. However, if we consider the~entries of the~returned matrix above the~main diagonal to be zero,
the~correctness of the~algorithm still holds.

\subsection{Time complexity of the~unblocked algorithm}\label{subsec-cholesky-number-of-flops}

On line \ref{unblocked-cholesky-diagonal-step}, we perform $j-1$ multiplications, $j-1$ additions/subtractions and a~square root computation.
The~overall amount of floating point operations executed on line \ref{unblocked-cholesky-diagonal-step} is thus
\begin{align*}
	\sum_{j=1}^{n}j-1+j-1+1 = \sum_{j=1}^{n}2j-1 = 2\frac{n(n+1)}{2}-n = n(n+1)-n = n^2
\end{align*}

On line \ref{unblocked-cholesky-non-diagonal-step}, $j-1$ multiplications and $j-1$ additions/subtractions are performed as well as
a~single floating point division. The~number of floating point operations performed on line \ref{unblocked-cholesky-non-diagonal-step} is
\begin{align*}
	\sum_{j=1}^{n}\sum_{i=j+1}^{n}j-1+j-1+1 &= \sum_{j=1}^{n}(n-j)(2j-1) =
	2n\Big(\sum_{j=1}^{n}j\Big) - 2\Big(\sum_{j=1}^{n}j^2\Big) - n^2 + \Big(\sum_{j=1}^{n}j\Big) = \\
	&= 2n\frac{n(n+1)}{2} - 2\frac{n(n+1)(2n+1)}{6} + n^2 - \frac{n(n+1)}{2} = \\
	&= n^3 + n^2 - \frac{2n^3+3n^2+n}{3} - n^2 + \frac{n^2 + n}{2} = \\
	&= \frac{1}{3}n^3 - \frac{1}{2}n^2 + \frac{1}{6}n
\end{align*}

In total, the~number of floating point operations performed during the~algorithm execution is $\frac{1}{3}n^3 + \frac{1}{2}n^2 + \frac{1}{6}n$.

In the~following sections, we will denote the~first factor in the~Cholesky decomposition of a~symmetric positive definite
matrix $A$ by $\texttt{POTF2}(A)$ (that is, if $A=LL^T$, then we represent $L$ by $\texttt{POTF2}(A)$).
This comes from the~fact that the~routine implementing the~unblocked Cholesky decomposition algorithm in the~LAPACK library
(see Section \ref{sec-lapack}) is named \texttt{POTF2}.

\section{Blocked algorithm}\label{sec-cholesky-blocked}

\newcommand{\noblocks}{\lceil\frac{n}{nb}\rceil}

In this section, we split the~matrices $A$ and $L$ into blocks. We first fix a~number $nb\in\lbrace 1, \dots, n\rbrace$
and we define:
\begin{itemize}
	\item $A_{ij}$ for $i,j\in\lbrace1,\dots,\lceil n/nb\rceil\rbrace$ as a~submatrix of $A$ obtained by removing the~first
	$i\dots nb$ rows and the~first $i\dots nb$ columns, and then removing all but the~first $nb$ rows and columns
	\item $L_{ij}$ for $i,j\in\lbrace1,\dots,\lceil n/nb\rceil\rbrace$ as a~submatrix of $L$ obtained by removing the~first
	$i\dots nb$ rows and the~first $i\dots nb$ columns, and then removing all but the~first $nb$ rows and columns
\end{itemize}

In this section, $A_{ij}$ will not denote the~$i,j$th entry of $A$, but the~$i,j$th block of $A$, unless stated otherwise.\\

\begin{algorithm}[H]
	\label{alg-blocked-cholesky}
	\caption{Blocked Cholesky decomposition (\cite{LinearAlgebraSoftware2016}, Algorithm 3)}
	\KwData{Symmetric positive definite matrix $A\in\R^{n,n}$}
	\KwResult{Lower triangular matrix $L$ with positive diagonal entries such that $A = LL^T$}
	$L\gets\Theta$\\
	\For{$j\gets1;j\le nb;j\gets j+1$}{
		\label{blocked-cholesky-outer-loop}
		$L_{jj}\gets \texttt{POTF2}(A_{jj})$\label{blocked-cholesky-potf2-step}\\
		\For{$i\gets j+1;i\le nb;i\gets i+1$}{
			$L_{ij}\gets A_{ij}\cdot \big((L_{jj})^T\big)^{-1}$\quad\Comment{// this can be calculated using TRSM}\label{blocked-cholesky-trsm-step}
		}
		\For{$k\gets j+1;k\le nb;k\gets k+1$}{
			\For{$i\gets k+1;i\le nb;i\gets i+1$}{
				\uIf{$i=k$}{
					$A_{kk}\gets A_{kk} - L_{kj}\cdot (L_{kj})^T$\quad\Comment{// this can be calculated using SYRK}\label{blocked-cholesky-syrk-step}
				}
				\Else{
					$A_{ik}\gets A_{ik} - L_{ij}\cdot (L_{kj})^T$\quad\Comment{// this can be calculated using GEMM}\label{blocked-cholesky-gemm-step}
				}
			}
		}
	}
	\Return L
\end{algorithm}

For the~purposes of proving the~correctness of Algorithm \ref{alg-blocked-cholesky}, we denote the~submatrix of $A$ at the~start of
the~$(j+1)$th iteration of the~outer loop on line \ref{blocked-cholesky-outer-loop} obtained by removing the~first $j\cdot nb$ rows
and the~first $j\cdot nb$ columns by $B_j$. Trivially, we can see that $A=B_0$.

For a~particular $j\in\lbrace 1,\dots,\noblocks-1\rbrace$, we partition $B_j=\begin{pmatrix}D&W^T\\W&K\end{pmatrix}$,
where $D\in\R^{nb,nb}$, $W\in\R^{n-(j + 1)\cdot nb,nb}$ and $K\in\R^{n-(j + 1)\cdot nb,n-(j + 1)\cdot nb}$.
Then by definition of $B_j$:
\begin{itemize}
	\item $D$ has the~same value as $A_{(j+1)(j+1)}$ at the~start of the~$(j+1)$th iteration of the~outer loop
	\item $W$ has the~same value as the~block matrix $\begin{pmatrix}A_{(j+2)(j+1)}\\\vdots\\A_{\lceil\frac{n}{nb}\rceil(j+1)}\end{pmatrix}$ at the~start of the~$(j+1)$th iteration of the~outer loop
	\item $K$ has the~same value as the~block matrix $\begin{pmatrix}A_{(j+2)(j+2)}&\hdots&A_{(j+2)\noblocks}\\\vdots&\ddots&\vdots\\A_{\noblocks(j+2)}&\hdots&A_{\noblocks\noblocks}\end{pmatrix}$ at the~start of the~$(j+1)$th iteration of the~outer loop
\end{itemize}

We further denote the~submatrix of $L$ obtained by removing the~first
$j\cdot nb$ rows and the~first $j\cdot nb$ columns by $L_j$. The~first column of the~matrix $L_j$ is thus
comprised solely of the~blocks of $L$ that are modified during the~$(j+1)$th iteration of the~outer loop.

\begin{lemma}\label{lemma-blocked-cholesky-correctness-1}
	Fix $j\in\lbrace 1,\dots,\noblocks-1\rbrace$ and partition $B_j=\begin{pmatrix}D&W^T\\W&K\end{pmatrix}$ as described above.
	If $B_j$ is symmetric positive definite, then $B_{j+1} = K - WD^{-1}W^T$.
\end{lemma}

\begin{proof}
	Since $D=(B_j)_{11}$ is a~leading principal submatrix of $B_j$, it is symmetric positive definite following Theorem
	\ref{theorem-positive-definitess-of-principal-submatrix}. We denote $L_D=\texttt{POTF2}(D)$.

	During the~$(j+1)$th iteration of the~outer loop, $L_{jj}$ is set to $\texttt{POTF2}(A_{jj})$ and $L_{ij}$ to $A_{ij}\cdot (L_{jj}^T)^{-1}$
	for all $i\in\lbrace j+1,\dots,nb\rbrace$.
	The~first column of $L_j$ thus becomes
	
	$$\begin{pmatrix}
		(L_j){11}\\(L_j){21}\\\vdots\\(L_j){n1}
	\end{pmatrix} = \begin{pmatrix}
		\texttt{POTF2}\big((B_j)_{11}\big)\\(B_j)_{21}\big((L_j)_{11}^T\big)^{-1}\\\vdots\\(B_j)_{(\noblocks-j)1}\big((L_j)_{11}^T\big)^{-1}
	\end{pmatrix} = \begin{pmatrix}
		L_D\\(B_j)_{21}\big(L_D^T\big)^{-1}\\\vdots\\(B_j)_{(\noblocks-j)1}\big(L_D^T\big)^{-1}
	\end{pmatrix}.$$

	For all $i,k\in\lbrace j+1,\dots,\noblocks\rbrace$ where $i\neq k$:
	\begin{itemize}
		\item $A_{kk}$ is set to $A_{kk}-L_{kj}\cdot(L_{kj})^T$ on line \ref{blocked-cholesky-syrk-step}, and since no other modifications
		of $A_{kj}$ and $L_{kj}$ are performed during the~$(j+1)$th iteration, we can conclude that
		\begin{align*}
			(B_{j+1})_{(k-j)(k-j)} &= (B_j)_{(k-j+1)(k-j+1)} - L_{kj}\cdot(L_{kj})^T = \\
			&= (B_j)_{(k-j+1)(k-j+1)} - (L_j)_{(k-j+1)1}\cdot(L_j)_{(k-j+1)1}^T
		\end{align*}
		\item $A_{ik}$ is set to $A_{ik}-L_{ij}\cdot(L_{kj})^T$ on line \ref{blocked-cholesky-gemm-step}, and because the~only lines
		where $A_{ik}$, $L_{ij}$ and $L_{kj}$ are modified during the~$(j+1)$th iteration are \ref{blocked-cholesky-trsm-step} and
		\ref{blocked-cholesky-gemm-step}, we observe that
		\begin{align*}
			(B_{j+1})_{(i-j)(k-j)} &= (B_j)_{(i-j+1)(k-j+1)} - L_{ij}\cdot(L_{kj})^T = \\
			&= (B_j)_{(i-j+1)(k-j+1)} - (L_j)_{(i-j+1)1}\cdot(L_j)_{(k-j+1)1}^T
		\end{align*}
	\end{itemize}

	We thus obtain:

	\begin{align*}
		B_{j+1} &= \begin{pmatrix}
			(B_{j+1})_{11}&\hdots&(B_{j+1})_{1(\noblocks-j-1)}\\
			\vdots&\ddots&\vdots\\
			(B_{j+1})_{(\noblocks-j-1)1}&\hdots&(B_{j+1})_{(\noblocks-j-1)(\noblocks-j-1)}
		\end{pmatrix} = \\
		&= K - \begin{pmatrix}
			(L_{j})_{21}\cdot\big((L_{j})_{21}\big)^{T}&\hdots&(L_{j})_{21}\cdot\big((L_{j})_{(\noblocks-j)1}\big)^{T}\\
			\vdots&\ddots&\vdots\\
			(L_{j})_{(\noblocks-j)1}\cdot\big((L_{j})_{21}\big)^{T}&\hdots&(L_{j})_{(\noblocks-j)1}\cdot\big((L_{j})_{(\noblocks-j)1}\big)^{T}
		\end{pmatrix} = \\
		&= K - \begin{pmatrix}
			(L_{j})_{21}\\\vdots\\(L_{j})_{(\noblocks-j)1}
		\end{pmatrix} \begin{pmatrix}
			(L_{j})_{21}\\\vdots\\(L_{j})_{(\noblocks-j)1}
		\end{pmatrix}^T = \\
		&= K - \begin{pmatrix}
			(B_{j})_{21}(L_D^T)^{-1}\\\vdots\\(B_{j})_{(\noblocks-j)1}(L_D^T)^{-1}
		\end{pmatrix} \begin{pmatrix}
			(B_{j})_{21}(L_D^T)^{-1}\\\vdots\\(B_{j})_{(\noblocks-j)1}(L_D^T)^{-1}
		\end{pmatrix}^T = \\
		&= K - \begin{pmatrix}
			(B_{j})_{21}(L_D^T)^{-1}\\\vdots\\(B_{j})_{(\noblocks-j)1}(L_D^T)^{-1}
		\end{pmatrix} \begin{pmatrix}
			(B_{j})_{21}(L_D^{-1})^{T}\\\vdots\\(B_{j})_{(\noblocks-j)1}(L_D^{-1})^{T}
		\end{pmatrix}^T = \\
		&= K - \begin{pmatrix}
			(B_{j})_{21}(L_D^T)^{-1}\\\vdots\\(B_{j})_{(\noblocks-j)1}(L_D^T)^{-1}
		\end{pmatrix} \begin{pmatrix}
			L_D^{-1}(B_{j})_{21}^T&\hdots&L_D^{-1}(B_{j})_{(\noblocks-j)1}^T
		\end{pmatrix} = \\
		&= K - W(L_D^T)^{-1} L_D^{-1}W^T = K - W(L_DL_D^T)^{-1}W^T = K - WD^{-1}W^T
	\end{align*}
\end{proof}

\begin{lemma}\label{lemma-blocked-cholesky-correctness-2}
	For a~symmetric positive definite matrix $A\in\R^{n,n}$, the~matrix $A_{jj}$ on line \textup{\ref{blocked-cholesky-potf2-step}} is symmetric positive definite
	in all iterations of the~outer loop.
\end{lemma}

\begin{proof}[Proof \textmd{(inspired by the~proof in \cite{TrefethenBau2022}, lecture 23)}]
	\hspace{\fill} \\

	\noindent We will first prove by induction that all $B_j$ are symmetric positive definite:
	\begin{description}
		\item[Base step] $B_0=A$ is symmetric positive definite.
		\item[Inductive step] Let $B_j$ be symmetric positive definite.
		Then $D$ is also symmetric positive definite following Theorem
		\ref{theorem-positive-definitess-of-principal-submatrix}.

		We set $L_D = \texttt{POTF2(D)}$, $\tilde{L} = \begin{pmatrix}L_D&\theta^T\\W(L_D^T)^{-1}&I\end{pmatrix}$ and
		$\tilde{B} = \begin{pmatrix}I&\theta^T\\\theta&B_{j+1}\end{pmatrix}$ such that\\$\tilde{L},\tilde{B}\in\R^{n-j\cdot nb,n-j\cdot nb}$, then:
		\begin{align*}
			\tilde{L}\tilde{B}\tilde{L}^T &= 
			\begin{pmatrix}
				L_D&\theta^T\\W(L_D^T)^{-1}&I
			\end{pmatrix} \begin{pmatrix}
				I&\theta^T\\\theta&B_{j+1}
			\end{pmatrix} \begin{pmatrix}
				L_D^T&\big(W(L_D^T)^{-1}\big)^T\\\theta&I
			\end{pmatrix} = \\
			&= \begin{pmatrix}
				L_D&\theta^T\\W(L_D^T)^{-1}&B_{j+1}
			\end{pmatrix} \begin{pmatrix}
				L_D^T&\big(W(L_D^T)^{-1}\big)^T\\\theta&I
			\end{pmatrix} = \\
			&= \begin{pmatrix}
				L_DL_D^T&L_D\big(W(L_D^T)^{-1}\big)^T\\W(L_D^T)^{-1}L_D^T&W(L_D^T)^{-1}\big(W(L_D^T)^{-1}\big)^T + B_{j+1}
			\end{pmatrix} = \\
			&= \begin{pmatrix}
				L_DL_D^T&L_D\big(W(L_D^{-1})^T\big)^T\\W(L_D^T)^{-1}L_D^T&W(L_D^T)^{-1}\big(W(L_D^{-1})^T\big)^T + B_{j+1}
			\end{pmatrix} = \\
			&= \begin{pmatrix}
				L_DL_D^T&L_DL_D^{-1}W^T\\W(L_D^T)^{-1}L_D^T&W(L_D^T)^{-1}L_D^{-1}W^T + B_{j+1}
			\end{pmatrix} = \\
			&= \begin{pmatrix}
				D&W^T\\W&W(L_DL_D^T)^{-1}W^T + B_{j+1}
			\end{pmatrix}
		\end{align*}
		Following Lemma \ref{lemma-blocked-cholesky-correctness-1},
		\begin{align*}
			\begin{pmatrix}
				D&W^T\\W&W(L_DL_D^T)^{-1}W^T + B_{j+1}
			\end{pmatrix}
			&= \begin{pmatrix}
				D&W^T\\W&WD^{-1}W^T + K - WD^{-1}W^T
			\end{pmatrix} =
			\begin{pmatrix}
				D&W^T\\W&K
			\end{pmatrix} =\\
			&= B_j
		\end{align*}

		Following Theorem \ref{theorem-determinant} and Lemma \ref{lemma-cholesky-decomposition-l-positive-diagonal}, we can observe that 
		$\tilde{L}$ is nonsingular since all of its diagonal entries are non-zero. $\tilde{L}^T$ and $(\tilde{L}^T)^{-1}$ are then also nonsingular
		thanks to Theorem \ref{theorem-nonsingular-transpose}. Given our supposition that $B_j$ is positive definite,
		we have $x^TB_jx>0$ for all non-zero $x\in\R^{n-j\cdot nb,1}$. We also know from Theorem \ref{theorem-nonsingular-property}
		that $(\tilde{L}^T)^{-1}x$ is non-zero. We then obtain
		\begin{align*}
			x^T\tilde{B}x = x^T\big(\tilde{L}^{-1}B_j(\tilde{L}^T)^{-1}\big)x = \big((\tilde{L}^T)^{-1}x\big)^TB_j\big((\tilde{L}^T)^{-1}x\big).
		\end{align*}
		which is positive due to the~positive definiteness of $B_j$. As such, $\tilde{B}$ is positive definite and so is its principal submatrix
		$B_{j+1}$.
	\end{description}

	\noindent Finally, we can observe that at the~start of the~$j$th iteration of the~outer loop, the~matrix $A_{jj}$ is positive definite, since
	it is a~leading principal submatrix of $B_{j-1}$.
\end{proof}

\begin{theorem}\label{theorem-blocked-cholesky-correctness}
	For a~symmetric positive definite matrix $A\in\R^{n,n}$, Algorithm \textup{\ref{alg-blocked-cholesky}} returns the~matrix
	$\texttt{POTF2}(A)$.
\end{theorem}

\begin{proof}
	Using backward induction, we will prove that $L_{j} = \texttt{POTF2}(B_j)$ for all $j\in\lbrace1,\dots,\noblocks\rbrace$:
	\begin{description}
		\item[Base step] $L_{\noblocks\noblocks}$ is set to $\texttt{POTF2}(A_{\noblocks\noblocks})$ on line \ref{blocked-cholesky-potf2-step}
		during the~last iteration of the~outer loop.
		\item[Inductive step] Suppose that $L_{j+1} = \texttt{POTF2}(B_{j+1})$ for any  $j\in\lbrace1,\dots,\noblocks - 1\rbrace$.

		Using observations from the~proof of Lemma \ref{lemma-blocked-cholesky-correctness-1}, we have 
		\begin{align*}
			\begin{pmatrix}
				(L_j)_{11}\\(L_j)_{21}\\\vdots\\(L_j)_{n1}
			\end{pmatrix} = \begin{pmatrix}
				L_D\\(B_j)_{21}\big(L_D^T\big)^{-1}\\\vdots\\(B_j)_{(\noblocks-j)1}\big(L_D^T\big)^{-1}
			\end{pmatrix} = \begin{pmatrix}
				L_D\\W\big(L_D^T\big)^{-1}
			\end{pmatrix}.
		\end{align*}

		Thus:

		\begin{align*}
			L_jL_j^T &= \begin{pmatrix}
				L_D&\Theta\\W\big(L_D^T\big)^{-1}&\texttt{POTF2}(B_{j+1})
			\end{pmatrix} \begin{pmatrix}
				L_D&\Theta\\W\big(L_D^T\big)^{-1}&\texttt{POTF2}(B_{j+1})
			\end{pmatrix}^T = \\
			&= \begin{pmatrix}
				L_D&\Theta\\W\big(L_D^T\big)^{-1}&\texttt{POTF2}(B_{j+1})
			\end{pmatrix} \begin{pmatrix}
				L_D^T&\Big(W\big(L_D^T\big)^{-1}\Big)^T\\\Theta^T&\texttt{POTF2}(B_{j+1})^T
			\end{pmatrix} = \\
			&= \begin{pmatrix}
				L_D&\Theta\\W\big(L_D^T\big)^{-1}&\texttt{POTF2}(B_{j+1})
			\end{pmatrix} \begin{pmatrix}
				L_D^T&L_D^{-1}W^T\\\Theta^T&\texttt{POTF2}(B_{j+1})^T
			\end{pmatrix} = \\
			&= \begin{pmatrix}
				L_DL_D^T&L_DL_D^{-1}W^T\\
				W\big(L_D^T\big)^{-1}L_D^T&W\big(L_D^T\big)^{-1}L_D^{-1}W^T + \texttt{POTF2}(B_{j+1})\texttt{POTF2}(B_{j+1})^T
			\end{pmatrix} = \\
			&= \begin{pmatrix}
				D&W^T\\
				W&WD^{-1}W^T + B_{j+1}
			\end{pmatrix}
		\end{align*}

		We know that $K = WD^{-1}W^T + B_{j+1}$ from Lemma \ref{lemma-blocked-cholesky-correctness-1}, and as such, we have

		\begin{align*}
			\begin{pmatrix}
				D&W^T\\
				W&WD^{-1}W^T + B_{j+1}
			\end{pmatrix} = \begin{pmatrix}
				D&W^T\\
				W&K
			\end{pmatrix} = B_j.
		\end{align*}
	\end{description}

	We have obtained the~identity $L_{j} = \texttt{POTF2}(B_j)$ for all $j\in\lbrace1,\dots,\noblocks\rbrace$.
	Specifically for $j=0$, we have $L = L_0 = \texttt{POTF2}(B_0) = \texttt{POTF2}(A)$.
\end{proof}

In Algorithm \ref{alg-blocked-cholesky}, we can notice that the~blocks $A_{ik}$ for $i<k$
are only accessed in a~single iteration of line \ref{blocked-cholesky-gemm-step}, where they are modified.
As such, we can eliminate some redundant floating point operations by rewriting the~algorithm in the~following form:\\

\begin{algorithm} [H]
	\label{alg-improved-blocked-cholesky}
	\caption{Improved blocked Cholesky decomposition (variant of \cite{LinearAlgebraSoftware2016}, Algorithm 3)}
	\KwData{Symmetric positive definite matrix $A\in\R^{n,n}$}
	\KwResult{Lower triangular matrix $L$ with positive diagonal entries such that $A = LL^T$}
	$L\gets\Theta$\label{improved-cholesky-l-initialization}\\
	\For{$j\gets 1;j\le nb;j\gets j+1$}{
		\label{improved-blocked-cholesky-outer-loop}
		$L_{jj}\gets \texttt{POTF2}(A_{jj})$\label{improved-blocked-cholesky-potf2-step}\\
		\For{$i\gets j+1;i\le nb;i\gets i+1$}{
			$L_{ij}\gets A_{ij}\cdot \big((L_{jj})^T\big)^{-1}$\quad\Comment{// this can be calculated using TRSM}\label{improved-blocked-cholesky-trsm-step}
		}
		\For{$k\gets j+1;k\le nb;k\gets k+1$}{
			$A_{kk}\gets A_{kk} - L_{kj}\cdot (L_{kj})^T$\quad\Comment{// this can be calculated using SYRK}\label{improved-blocked-cholesky-syrk-step}
			\For{$i\gets k+1;i\le nb;i\gets i+1$}{
				$A_{ik}\gets A_{ik} - L_{ij}\cdot (L_{kj})^T$\quad\Comment{// this can be calculated using GEMM}\label{improved-blocked-cholesky-gemm-step}
			}
		}
	}
	\Return L
\end{algorithm}

Note that the~reason why we first introduced Algorithm \ref{alg-blocked-cholesky} was a~more straightforward proof of correctness -- the
matrices $B_j$ would not always be symmetric if we used Algorithm \ref{alg-improved-blocked-cholesky}, but the~matrices obtained by 
``mirroring'' the~lower triangular part of $B_j$ along the~main diagonal would.

A~second improvement is to perform the~calculation in place -- that would be achieved by removing line \ref{improved-cholesky-l-initialization}
and replacing all references to $L$ with references to $A$ in Algorithm \ref{alg-improved-blocked-cholesky}.

\subsection{Parallelization of the blocked Cholesky decomposition algorithm}

In Algorithm \ref{alg-improved-blocked-cholesky}, we can notice that on lines \ref{improved-blocked-cholesky-trsm-step},
\ref{improved-blocked-cholesky-syrk-step} and \ref{improved-blocked-cholesky-gemm-step}, we do not need to perform all of the~computations in series.
On line \ref{improved-blocked-cholesky-trsm-step}, we only have to know the~value of the~block $L_{jj}$ in order to compute the~blocks
$L_{j(j+1)}$ to $L_{jn}$, but not the~other blocks with the~column index $j$. As such, the~computations of the~blocks $L_{j(j+1)}$ to $L_{jn}$
are independent, and can thus be performed in parallel, because the~only memory accessed by all computing threads is the~block $L_{jj}$, which
is only read, but not overwritten.

In a~similar fashion, we can perform all of the~computations on line \ref{improved-blocked-cholesky-syrk-step} in a~single iteration of
\ref{improved-blocked-cholesky-outer-loop} simultaneously, since we do not need to access any of the~diagonal blocks $A_{(j+1)(j+1)},\dots,A_{nn}$
except for $A_{kk}$ to be able to update the~block $A_{kk}$. The~block $L_{kj}$ can be accessed by all of the~threads running on line
\ref{improved-blocked-cholesky-syrk-step} at once due to the~fact that $L_{ij}$ is only read, but not written to.

Using the~same reasoning, we can also perform all of the~computations on line \ref{improved-blocked-cholesky-gemm-step} in one iteration
of \ref{improved-blocked-cholesky-outer-loop} in parallel.

In summary, the~parallelized algorithm can be expressed as follows:\\

\begin{algorithm} [H]
	\label{alg-parallelized-blocked-cholesky}
	\caption{Parallelized version of blocked Cholesky decomposition}
	\KwData{Symmetric positive definite matrix $A\in\R^{n,n}$}
	\KwResult{Lower triangular matrix $L$ with positive diagonal entries such that $A = LL^T$}
	$L\gets\Theta$\\
	\For{$j\gets1;j\le nb;j\gets j+1$}{
		\label{paralellized-blocked-cholesky-loop}
		$L_{jj}\gets \texttt{POTF2}(A_{jj})$\label{paralellized-blocked-cholesky-potf2-step}\\
		Compute $L_{ij}\gets A_{ij}\cdot \big((L_{jj})^T\big)^{-1}$ using \texttt{TRSM} for $i\in\lbrace j+1,\dots,nb\rbrace$ in parallel\\
		Compute $A_{kk}\gets A_{kk} - L_{kj}\cdot (L_{kj})^T$ using \texttt{SYRK} for $k\in\lbrace j+1,\dots,nb\rbrace$ in parallel\\
		Compute $A_{ik}\gets A_{ik} - L_{ij}\cdot (L_{kj})^T$ using \texttt{GEMM} for $k\in\lbrace j+1,\dots,nb\rbrace$ and $i\in\lbrace k+1,\dots,nb\rbrace$ in parallel\label{parallelized-blocked-cholesky-gemm-step}\\
	}
	\Return L
\end{algorithm}

\section{Solving systems of linear equations using Cholesky decomposition}

Cholesky decomposition can be used to solve systems of linear equations $Ax=b$ with a~symmetric positive definite matrix $A$
in the~following way:

\begin{algorithm} [H]
	\label{alg-cholesky-sle-solution}
	\caption{Solution of a~system of linear equations $Ax=b$ with a~symmetric positive definite matrix $A$ using Cholesky decomposition}
	\KwData{Symmetric positive definite matrix $A\in\R^{n,n}$}
	\KwData{Vector $b\in\R^{n,1}$}
	\KwResult{Vector $x\in\R^{n,1}$ such that $Ax=b$}
	$L\gets \texttt{POTF2}(A)$\label{cholesky-sle-solution-potf2-step}\\
	$y\gets L^{-1}b$\label{cholesky-sle-solution-forward-step}\\
	$x\gets (L^T)^{-1}y$\label{cholesky-sle-solution-backward-step}\\
	\Return $x$
\end{algorithm}

The~calculations on lines \ref{cholesky-sle-solution-forward-step} and \ref{cholesky-sle-solution-backward-step} can be performed
using the~\texttt{TRSV} Level 2 BLAS routine.

\begin{theorem}\label{theorem-cholesky-sle-correctness}
	For a~symmetric positive definite matrix $A\in\R^{n,n}$ and a~vector $b\in\R^{n,1}$, Algorithm \textup{\ref{alg-cholesky-sle-solution}}
	returns a~vector $\tilde{x}$ that is the~only solution of the~equation system $Ax=b$.
\end{theorem}

\begin{proof}
	We know that both $L$ and $L^T$ are nonsingular from Lemma \ref{lemma-cholesky-decomposition-l-positive-diagonal} and Theorem
	\ref{theorem-determinant}. We then have 
	$$A\tilde{x} = A(L^T)^{-1}y = A(L^T)^{-1}L^{-1}b = \big(LL^T\big)(L^T)^{-1}L^{-1}b = LL^T(L^T)^{-1}L^{-1}b = LL^{-1}b = b.$$

	Following Theorem \ref{theorem-nonsingular-product}, $A$ is also nonsingular. By multiplying both sides of $Ax=b$ by $A^{-1}$ from the~left,
	we obtain
	$$x = A^{-1}b.$$
	Since both $A^{-1}$ and $b$ are determined uniquely, $x$ is also unique.
\end{proof}

If $A$ is not positive definite, other decomposition algorithms are used to solve the~system of linear equations
(typically the~LU factorization) \cite{LA2}. The~reason why solution using the~Cholesky decomposition algorithm is attempted first
is that it performs approximately $\frac{1}{3}n^3$ floating point operations, whereas algorithms for solving systems of linear equations
with general matrices require roughly $\frac{2}{3}n^3$ operations \cite{TrefethenBau2022}.

We can show that if Algorithm
\ref{alg-cholesky-sle-solution} fails to return a~solution $x$, then the~input matrix $A$ was not positive definite,
regardless of the~algorithm used on line \ref{cholesky-sle-solution-potf2-step}:

\begin{theorem}
	A~square matrix $A\in\R^{n,n}$ has a~Cholesky decomposition if and only if it is symmetric positive definite.
\end{theorem}

\begin{proof}
	The~converse implication ($\Leftarrow$) has already been proven in Theorem \ref{theorem-existence-uniqueness-cholesky}.

	A~matrix $A\in\R^{n,n}$ with a~Cholesky decomposition $A = LL^T$ is symmetric positive definite following Lemmas
	\ref{lemma-symmetric-matrix-properties}
	and \ref{lemma-positive-definiteness-of-product}, since $L$ has a~non-zero determinant and is therefore nonsingular.
\end{proof}

In summary, if the~algorithm used on line \ref{cholesky-sle-solution-potf2-step} fails to produce the~matrix $L$,
then $A$ is not symmetric positive definite and thus some other algorithm for solving systems of linear equations must be used.