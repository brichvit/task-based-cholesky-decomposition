% This is a source file for the Implementation chapter of Vít Břichňáč's bachelor thesis, Task-based Cholesky decomposition

\chapter{Implementation}

The~blocked algorithm for Cholesky factorization was implemented in the~C language, as both OpenMP and StarPU offer a~C interface and
the~language is commonly used in many modern numerical libraries (e.g., libFLAME, BLIS and OpenBLAS). The~implementation was tested
on Linux systems with the~\texttt{gcc} and \texttt{gfortran} compilers only (version 10.2.0 for both), although it may work on other UNIX-like systems with other compilers as well.

When compiled, the~following artifacts are produced by default:
\begin{itemize}
	\item A~dynamic library -- \texttt{libcholesky.so}
	\item A~C/C++ header file for the~library -- \texttt{cholesky.h}
	\item Three test binaries --  \texttt{blas\_test}, \texttt{potf2\_test}, \texttt{cholesky\_test}
	\item A~benchmarking utility -- \texttt{cholesky\_benchmark}
\end{itemize}

A~static library build may be enabled in the~configuration script as well (see Section \ref{sec-configuration-script}).

The~code is freely available on
\href{https://gitlab.fit.cvut.cz/brichvit/task-based-cholesky-decomposition/-/tree/thesis}{GitLab} under a~2-clause BSD license.

\section{Implementation of BLAS routines}\label{sec-implementation-blas-routines}

The~\texttt{GEMM}, \texttt{SYRK} and \texttt{TRSM} routines are implemented in the~\texttt{src} directory. Only the~double-precision real
number variants are implemented, though the~code would not be difficult to edit in order to implement the~single-precision number routines
\texttt{SGEMM}, \texttt{SSYRK}, and \texttt{STRSM} or the~complex number routines prefixed with \texttt{Z} or \texttt{C}.

The~implementations closely resemble Algorithms \ref{alg-naive-gemm}, \ref{alg-naive-syrk} and \ref{alg-naive-trsm},
though the~C implementations are split in four branches (for \texttt{SYRK}) or eight branches (for \texttt{GEMM} and \texttt{TRSM})
according to the~different values of the~\texttt{TRANS}, \texttt{TRANSA}, \texttt{TRANSB}, \texttt{UPLO} and \texttt{SIDE} parameters.

The~interface to the~BLAS routines is highly inspired by the~original Fortran interface (see Section \ref{sec-level-3-blas}), but with
the~following changes:
\begin{itemize}
	\item The~function names consist of non-capital letters only (\texttt{dgemm}, \texttt{dsyrk} and \texttt{dtrsm})
	\item The~\texttt{TRANS}, \texttt{TRANSA}, \texttt{TRANSB}, \texttt{UPLO} and \texttt{SIDE} parameters are passed by value, not reference (the~parameters are of type \texttt{char},
	not \texttt{char*}, but the~meaning of the~values remains the~same)
	\item The~\texttt{ALPHA} and \texttt{BETA} parameters are passed by value, not reference (these parameters have the~\texttt{double} type
	as opposed to \texttt{double*})
	\item All matrix size and leading dimension parameters are passed by value, not reference (these parameters have the~\texttt{int} type
	as opposed to \texttt{int*})
	\item The~error code is not passed by the~\texttt{INFO} output variable, but by the~return value
\end{itemize}

Similarly to the~Fortran BLAS interface, the~routines assume all matrices in column major full storage.

\subsection{Cache access optimization}
The~only optimizations performed were the~reordering of the~nested for loops in order to avoid unnecessary cache misses, i.e., accesses to
memory addresses that are not loaded in the~cache memory. As cache memories are known to perform better when the~memory is accessed sequentially,
we try to reorder the~loops in a~way that minimizes the~offsets between subsequent accesses.

For instance, in the~\texttt{DGEMM} routine, the~code in listing \ref{lst-unoptimized-gemm-code} is not very efficient, since most subsequent memory accesses in matrices $A$
and $B$ have an~offset of $\texttt{ldc}$ entries, which leads to large amounts of cache misses:

\begin{lstlisting}[language=C,tabsize=2,backgroundcolor=\color{LightBlue},caption={Unoptimized code from the~\texttt{DGEMM} implementation.},label={lst-unoptimized-gemm-code}]
for (int i = 0; i < M; i++) {
	for (int k = 0; k < K; k++) {
		...
		C[i + j * ldc] += alpha * A[i + k * ldc] * B[j + k * ldc];
		...
	}
}
\end{lstlisting}

To the~contrary, the~code in listing \ref{lst-optimized-gemm-code} accesses the~entries of $A$ and $C$ sequentially in an~ascending order, and is therefore more efficient
due to a~lower amount of cache misses:

\begin{lstlisting}[language=C,tabsize=2,backgroundcolor=\color{LightBlue},caption={Cache access optimized code from the~\texttt{DGEMM} implementation.},label={lst-optimized-gemm-code}]
for (int k = 0; k < K; k++) {
	for (int i = 0; i < M; i++) {
		...
		C[i + j * ldc] += alpha * A[i + k * ldc] * B[j + k * ldc];
		...
	}
}
\end{lstlisting}

The~only branches that are optimized for cache accesses are the~ones used in Algorithm \ref{alg-improved-blocked-cholesky}. Those are:
\begin{itemize}
	\item $\texttt{TRANSA}=\texttt{N}$ and $\texttt{TRANSB}=\texttt{T}$ for \texttt{GEMM}
	\item $\texttt{UPLO}=\texttt{L}$ and $\texttt{TRANS}=\texttt{N}$ for \texttt{SYRK}
	\item $\texttt{SIDE}=\texttt{R}$, $\texttt{UPLO}=\texttt{L}$ and $\texttt{TRANSA}=\texttt{T}$ for \texttt{TRSM}
\end{itemize}

\subsection{Further possible optimizations}

The~BLAS routine implementations could be further optimized by utilizing SIMD vector register instructions, such as SSE or AVX instructions
on certain Intel processors. These register have a~larger size than standard registers, but allow the~same operations to be performed on more
parts of the~data at once. This allows multiple floating point operations to be performed on multiple floating point numbers
during one instruction cycle.

Optimized BLAS routines also often make use of the~Fused-Multiply-Add (FMA) instruction, which combines the~floating point addition and multiplication
operations into one instruction. In combination with SIMD instructions, it can lead to a~throughput may times higher than the~one of unoptimized routines.

The~BLAS routines could themselves be parallelized using a~threading library or a~runtime system, but that is outside of the~scope of this
thesis.

It should be noted that we only use optimized BLAS routines from numerical libraries in the~final benchmarks.

\section{Implementation of the~Cholesky decomposition routines}

The~unblocked Cholesky routine is implemented in the~\texttt{src/potf2} subdirectory using a~modified variant of Algorithm \ref{alg-unblocked-cholesky}.
The~interface is similar to the~one of BLAS routines (described in the~Section \ref{sec-implementation-blas-routines}), with the~difference
that invalid parameter values are signaled with negative return codes, while positive return codes indicate an~input matrix
that is not positive definite. The~return value then marks the~index of the~main loop iteration where a~negative diagonal element was
detected before computing the~square root.

The~blocked Cholesky decomposition algorithm implementation comprises two routines in the~\texttt{src/cholesky} subdirectory,
where one of them uses OpenMP tasks and the~other
uses the~StarPU runtime system. Apart from the~pointer to the~memory area where the~matrix is stored and the~matrix size, the~routines
also retrieve the~block size used for partitioning the~matrix and the~translation method as parameters.

The~Cholesky routines are implemented using the~task-based Algorithm \ref{alg-task-based-blocked-cholesky}.
In both routines, we assume that the~input matrix $A\in\R^{n,n}$ is stored in the~column major full storage format with the~leading dimension
equal to $n$.

The~routines start by partitioning the~input matrix into blocks.
The~information about
\makebox[\textwidth][s]{the~blocks is stored in a~matrix (the~information about a~particular block is stored in}
\makebox[\textwidth][s]{the~\texttt{starpu\_data\_handle\_t} structure for the~StarPU variant and a~custom structure}
\texttt{block\_data\_t} for the~OpenMP variant). According to the~value of the~\texttt{translation\_method}
parameter, one of the~following operations is performed on the~matrix:
\begin{itemize}
	\item The~matrix is left as it is (the~\texttt{NO\_TRANSLATION} value)
	\item The~matrix is translated into the~block storage format sequentially\\(the~\texttt{SEQUENTIAL\_TRANSLATION} value)
	\item The~matrix is translated into the~block storage format using StarPU or OpenMP where the~translation of each block is inserted as a~task
	(the~\texttt{PARALLEL\_TRANSLATION} value)
\end{itemize}

At the~end, the~computed matrix is translated back into the~column major format if necessary (again, in sequential or in parallel, according to the~value
of the~\texttt{translation\_method} parameter) and all memory blocks associated with matrices created in the~routine are freed.

\section{Testing of BLAS routines}

The~\texttt{DGEMM}, \texttt{DSYRK} and \texttt{DTRSM} routines were tested using a~modified version of the~\texttt{DBLAT3} program
-- a~testing program for double-precision Level 3 BLAS routines from the~reference BLAS library. Function calls that test other Level 3 BLAS
routines were deleted and only the~function calls to test routines \texttt{DPOT1}, \texttt{DPOT3} and \texttt{DPOT4} were left in, as
they test the~three aforementioned BLAS routines.

These test routines use the~reference \texttt{DMMCH} function. It receives matrices $A$, $B$, $CT$ and $CC$ as input parameters and
performs a~computation of the~general matrix product
\begin{equation}\label{eq-dmmch}
	CT \leftarrow \alpha \op(A) \op(B) + \beta CT
\end{equation}

It then computes the~maximum error ratio among all of the~entries and reports failure if at least one of the~results is less than half accurate \cite{Lapack}.

Testing \texttt{DGEMM} using \texttt{DMMCH} is straightforward, as both of the~routines perform the~same computation. The~computation
that \texttt{SYRK} performs (equation \eqref{eq-syrk-non-transposed} or \eqref{eq-syrk-transposed}) is a~special case of equation \eqref{eq-dmmch},
so \texttt{SYRK} can be tested using \texttt{DMMCH} in a~straightforward way as well. \texttt{TRSM} performs fundamentally different computations,
but after $B$ has been updated according to equations \eqref{eq-trsm-left} or \eqref{eq-trsm-right}, the~value of the~original matrix $B$
can be obtained as $\alpha^{-1}\op(A)B$ or $\alpha^{-1}B\op(A)$, respectively. Both of those formulas are specific cases of \eqref{eq-dmmch},
so \texttt{TRSM} can be tested using the~\texttt{DMMCH} routine, too.

\section{Testing of the~Cholesky decomposition routines}\label{sec-testing-cholesky}

\makebox[\textwidth][s]{The~main blocked Cholesky decomposition routine was tested using a~modified version of}\\the~\texttt{DCHKPO} LAPACK routine rewritten to C.
The~tests performed in the~routine were purposely limited to exclude testing features that are outside of the~scope of this thesis. Those are:
\begin{itemize}
	\item Testing error exits for invalid parameter values
	\item Testing error exits for input matrices that are not symmetric positive definite
	\item Testing the~$\texttt{UPLO}=\texttt{U}$ variant of the~\texttt{DPOTRF} routine (the~$A=U^TU$ decomposition)
\end{itemize}

All of the~remaining tests are run for several block sizes.

For a~single set of input parameters (matrix size, matrix type, block size, translation method and right-hand side count $rhs$),
an~$n\times rhs$ right-hand side matrix is generated and the~following tests are performed \cite{Lapack}:

\begin{enumerate}
	\item Reconstructing $\tilde{A}=LL^T$ and computing the~matrix 1-norm $\|A-\tilde{A}\|$
	\item Calculating the~inverse $\tilde{A}^{-1}=(L^T)^{-1}L^{-1}$ and computing the~matrix 1-norm $\|A\tilde{A}^{-1}-I\|$
	\item Solving $Ax=b$ for each right hand side $b$ and computing the~vector 1-norm $\|A\tilde{x}-b\|$
	\item Computing the~difference between the~solution obtained in the~previous test and the~true solution to $Ax=b$
	(there is only a~single true solution, see Theorem \ref{theorem-cholesky-sle-correctness})
	\item Iteratively refining the~computed solution and computing the~error bounds
	\item Estimating the~condition number of a~matrix and comparing the~estimate with the~true value
\end{enumerate}

The~unblocked routine \texttt{dpotf2} was tested using a~stripped down version of the~\texttt{DCHKAA} program, where all test routines except
for \texttt{DCHKPO} (the~routine testing symmetric positive definite matrix operations) were removed. In order to be able to call the
\texttt{xerbla} error handling routine from LAPACK with strings of size that is unknown at compile time, a~custom helper Fortran routine is
provided in the~file \texttt{xerbla\_helper.f}.

The~source code for all tests can be found in the~\texttt{test} directory.

\section{Implementation of the~benchmarking utility}\label{sec-implementation-benchmarking-utility}

The~benchmarking utility measures the~performance of the~blocked Cholesky routine. Its source code is found in the~\texttt{src/benchmark} directory.
It is a~Linux command line executable with the
following arguments:
\begin{itemize}
	\item Matrix size: \texttt{-m}, \texttt{--matrix-size}
	\item Block size: \texttt{-b}, \texttt{--block-size}
	\item Number of threads used: \texttt{-t}, \texttt{--no-threads}
	\item The~random seed for matrix generation: \texttt{-s}, \texttt{--seed}
	\item Number of iterations for each matrix size and block size: \texttt{-i}, \texttt{--iterations}
	\item Additional correctness testing: \texttt{-C}, \texttt{--check-correctness}
	\item Running the~custom implementations only: \texttt{-c}, \texttt{--custom-only}
	\item Selection of the~translation method: \texttt{-T}, \texttt{--translation-method}
	\item Ensuring correct trace generation using the~FxT library and StarPU: \texttt{-f}, \texttt{--fxt-trace}
\end{itemize}

The~\texttt{--matrix-size} and \texttt{--block-size} parameter values may be specified in the~form of \texttt{start:end:step} ranges. If so,
the~benchmarking utility iterates through every matrix size and every block size specified in the~ranges.

In every iteration, the~utility first generates a~symmetric positive definite matrix using Algorithm
\ref{alg-spd-matrix-generation} with the~entries of $R$ being random uniform numbers from the~interval $\langle0,1\rangle$ and $\lambda=1$.

Then, the~custom blocked Cholesky routine is run and the~execution time is measured using the~\texttt{clock\_gettime(CLOCK\_MONOTONIC, ...)}
function from \texttt{time.h}. The~Gflop/s metric (introduced in Section \ref{sec-gflops}) is then calculated and the~results are printed.
The~translation method used is determined by the~\texttt{--translation-method} argument, the~possible values are \texttt{no} for
not performing scheme translation at all, \texttt{sequential} for sequential translation and \texttt{parallel} for parallelized translation
using OpenMP or StarPU.

If additional correctness testing is enabled, the~utility performs a~test equivalent to test 1 from the~list in Section \ref{sec-testing-cholesky}
and prints the~test ratio and the~test result. If the~\texttt{--custom-only} option is not enabled and if the~program has been linked
with one of the~numerical libraries, the~same steps are then taken for the~library implementation of the~\texttt{DPOTRF} routine, which
computes the~Cholesky factorization.

To make a~fair comparison between the~library implementation and the~custom implementation, we need the~driver routines (\texttt{DGEMM},
\texttt{DSYRK}, \texttt{DTRSM} and \texttt{DPOTRF}/\texttt{DPOTF2}) to be executed sequentially during the~custom implementation, because
the~program is already parallelized by the~runtime system and parallelizing the~driver routines would lead to very granular ``subtasks'' with
a~big overhead. On the~other hand, those routines need to be parallelized
in the~library implementation. We use builtin library functions (usually with the~name \texttt{<libname>\_set\_num\_threads}) to set the~desired
number of threads for both implementations.

Note that in order for the~AOCL implementation to work properly, the~libFLAME library has to be built with the~LAPACK2FLAME option enabled.
Similarly, the~OpenBLAS implementation requires the~usage of the~\texttt{USE\_THREAD=1} and \texttt{USE\_OPENMP=0} options when compiling.

\section{The configuration script}\label{sec-configuration-script}

The~implementation contains a~configuration script which automatically detects the~\texttt{gcc} and \texttt{gfortran} compilers, StarPU and OpenMP. Using the
\texttt{--numerical-lib} option, one of the~following numerical libraries/library sets can be linked:
\begin{itemize}
	\item Intel MKL -- value \texttt{MKL}
	\item AOCL (both libFLAME and BLIS have to be available) -- value \texttt{AOCL}
	\item OpenBLAS -- value \texttt{OpenBLAS}
	\item Arm Performance libraries -- value \texttt{ArmPL}
\end{itemize}

When using the~\texttt{--numerical-lib} option with one of these values, the~script tries to detect the~selected library. If the~library
files are not present in one of the~directories specified in the~\texttt{LIBRARY\_PATH} environment variable or if the~include files are
not present in a~location specified in the~\texttt{C\_INCLUDE\_PATH} variable, the~script returns an~error. If the~library is found, its implementations of the~driver routines are then used instead of the~builtin ones
and its \texttt{DPOTRF} implementation is run in the~benchmarking utility (see Section \ref{sec-implementation-benchmarking-utility}).
If the~option is omitted, the~builtin implementations of the~driver routines are used and only the~custom implementations are run in the
benchmarking utility.

The~used runtime systems can be specified with the~\texttt{--use-openmp} and \texttt{--use-starpu} options. Both options
may be used at once, but at least one of them has to be used, otherwise the~script returns an~error. The~script tries to detect the~associated
libraries and exits with an~error in case of a~failure.

The~user can also opt to build a~static library by using the~\texttt{--build-static} option. After building, a~file named \texttt{libcholesky.a}
is produced as an~artifact.

The~directory where the~artifacts will be installed can be specified with the~\texttt{--prefix} option, with the~default directory being
\texttt{/usr/local}.

\subsection{Makefile}

The~implementation is built with the~GNU \texttt{make} utility using the~Makefile in the~top-level directory.

Available targets include:
\begin{description}
	\item[all] builds all artifacts (default target)
	\item[check] runs the~tests and prints their results
	\item[install] installs the~compiled libraries, header file and benchmarking utility into the~directory specified by \texttt{--prefix}
	(or \texttt{/usr/local} by default)
	\item[uninstall] uninstalls the~installed artifacts
	\item[clean] removes the~artifacts produced by building
	\item[distclean] removes the~artifacts produced by building and the~files generated by the~configuration script  
\end{description}

The~installation procedure thus consists of running the~configuration script with the~desired options and running \texttt{make} and
subsequently \texttt{make install}. Optionally, \texttt{make check} may be run to test the~library before installation.
