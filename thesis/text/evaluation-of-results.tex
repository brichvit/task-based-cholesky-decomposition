% This is a source file for the Evaluation of results chapter of Vít Břichňáč's bachelor thesis, Task-based Cholesky decomposition

\chapter{Evaluation of results}

\section{Measuring performance in GFlop/s}\label{sec-gflops}

When comparing the~performance of several implementations of the~same numerical routine, the~overall execution time (sometimes called the
\textbf{wall time}) may be used as a~performance metric~-- the~lower the~wall time, the~better the~implementation (assuming that all of the
implementations are tested with the~same set of inputs). It is, however, impractical to compare implementations of different numerical operations
using the~execution time, because those operations may perform a~different number of floating point operations.

To account for that difference, we may use the~\textbf{Flop/s} metric, which is computed as follows:

$$\text{Flop/s}=\frac{\text{floating point operations required to perform the numerical computation}}{\text{wall time}}$$

Note that the~number of floating point operations used in the~numerator
does not depend on the~algorithm used for the~computation. Instead, we use the~number of floating point operations that have to be carried out
in order to perform the~computation -- i.e., the~number of operations of the~best known algorithm for the~computation. For instance,
we use $\frac{1}{3}n^3+\frac{1}{2}n^2+\frac{1}{6}n$ for computations of the~Cholesky decomposition, where $n$ is the~matrix size
(see Subsection \ref{subsec-cholesky-number-of-flops}).

Using this metric, we may compare implementations of different numerical computations, where higher values mean better performance. The~values
of Flop/s obtained on modern multicore processors are usually in the~order of billions, which is why the~\textbf{Gflop/s} metric calculated as

$$\text{Gflop/s}=\frac{\text{Flop/s}}{10^9}$$

\noindent is used more often.

\section{Performance evaluation}

In this section, we examine the~performance of the~main Cholesky decomposition routine using the~benchmarking utility described in Section
\ref{sec-implementation-benchmarking-utility}. All plots presented in this section were generated using the~ggplot2 library of the~
R programming language. Each data point in a~plot represents an~average value across 5 iterations. All tests apart from the~ones in
Subsection \ref{sec-parallelization-test-results} have been run on all available CPU cores. Only the~custom implementations using StarPU
are presented in this chapter, as the~OpenMP custom implementations showed inferior performance. 

\subsection{Used hardware}
Performance of the~Cholesky decomposition routine was evaluated on three types of multicore compute nodes with different CPU architectures.
All of the~nodes are part of the~IT4Innovations National Supercomputing Center, which belongs to the~Technical University of Ostrava.

%Note: ARM as an architecture, Arm as a company
The~used compute nodes have the~following specifications \cite{IT4IDocs}:
\begin{enumerate}
	\item Node of the~Karolina supercomputer (without accelerators)
	\begin{itemize}
		\item Processors: $2\times$AMD EPYC 7H12
		\item Processor architecture: x86-64
		\item Processor cores: 64 per processor (128 in total) 
		\item CPU frequency: $2.6$GHz
		\item Instruction set extensions: Streaming SIMD Extensions 4.2 (SSE4.2), Advanced Vector Extensions 2 (AVX2)
		\item Theoretical peak performance: $5324.8$ Gflop/s
	\end{itemize}
	\item Node of the~Barbora supercomputer (without accelerators)
	\begin{itemize}
		\item Processors: $2\times$Intel Cascade Lake 6240
		\item Processor architecture: x86-64
		\item Processor cores: $18$ per processor ($36$ in total) 
		\item CPU frequency: $2.6$GHz
		\item Instruction set extensions: SSE4.2, AVX2
		\item Theoretical peak performance: $2995.2$ Gflop/s
	\end{itemize}
	\item Complementary systems partition 1 node (Arm node)
	\begin{itemize}
		\item Processors: $1\times$Fujitsu A64FX
		\item Processor architecture: ARMv8.2-A
		\item Processor cores: $48$
		\item CPU frequency: $2$GHz
		\item Instruction set extensions: Scalable Vector Extension (SVE)
		\item Theoretical peak performance: $3072$ Gflop/s (according to~\cite{A64FXDatasheet})
	\end{itemize}
\end{enumerate}

\subsection{Block size benchmarking results}
Figure \ref{fig-plot-karolina-block-size-benchmark} shows the~performance of the~custom blocked Cholesky decomposition routine for
different block sizes. All results were generated using the~StarPU version of the~routine on all 128 cores of the~Karolina compute node
with $10000\times10000$ input matrices. Note that the~Arm Performance Libraries were left out,
as they are only available on Arm-based processors~\cite{ArmPLOverview}.

Figure \ref{fig-plot-cs-block-size-benchmark} examines the~relationship between performance and block size for the~custom implementations
using Arm Performance Libraries and OpenBLAS on the~A64FX CPU, also with input matrices of size $10000\times10000$.

Both figures were generated with the~\texttt{PARALLEL\_TRANSLATION} translation method. The~optimal block size is thus influenced
by line sizes of various level cache memories on the~processor, among other factors.

The~plots show the~effects described in Section \ref{sec-task-granularity} -- large block sizes lead to a~low number of long tasks, which
means less parallelization near the~end (sometimes also the~start) of the~program. On the~other hand, small block sizes lead to many short tasks, which increases
the~overhead and, as a~result, the~overall execution time. We can see from Figure \ref{fig-plot-karolina-block-size-benchmark} that for this matrix size,
the~tasks hit the~right level of granularity for block sizes approximately between $400$ and $600$.

We will now focus on the~implementation using MKL driver routines (the~orange line in Figure \ref{fig-plot-karolina-block-size-benchmark}). 
The~optimal block size for $10000\times 10000$ matrices will likely be smaller than the~optimal block size for $20000\times20000$ matrices,
as the~overall number of blocks is proportional to the~square of the~matrix size (assuming a~fixed block size). In other words, smaller
block sizes tend to yield better performance for smaller matrices and larger block sizes usually perform better on larger matrices.
This phenomenon can be demonstrated on Figure \ref{fig-plot-karolina-mkl-flops-benchmark-block-size-comparison}, where we compare the~performance
of custom implementation using StarPU and MKL driver routines across varying matrix sizes (along the~$x$ axis) for different block sizes
(represented by the~colored lines).

The~effect that varying task granularities have on performance can also be shown on the~traces. The~trace in Figure
\ref{fig-trace-too-many-tasks} was generated on one of the~Karolina nodes using StarPU and MKL with a~$10000\times10000$
matrix with $200\times 200$ blocks, limited to 36 CPU cores for readability. This combination of input parameters leads to many short,
rapidly changing tasks. We can compare this behavior with the~trace in Figure \ref{fig-trace-too-few-tasks},
which was generated with the~same matrix size and core count, but with $800\times 800$ blocks. As we can see, the~individual tasks
have considerably longer execution times, which causes the~underutilization of CPU cores towards the~end of the~program.

\begin{figure}[H]
	\caption{Performance vs. block size plot for different custom implementations using StarPU and routines from several numerical libraries.
			The~performance was measured on a~Karolina compute node with $10000\times 10000$ input matrices.
			The~\texttt{PARALLEL\_TRANSLATION} translation method was used.}
	\label{fig-plot-karolina-block-size-benchmark}
	\includegraphics[angle=0,width=\textwidth]{karolina-block-size-benchmark.pdf}
\end{figure}

\begin{figure}[H]
	\caption{Performance vs. block size plot for different custom implementations using StarPU and routines from several numerical libraries.
			The~performance was measured on the~Arm node with $10000\times 10000$ input matrices.
			The~\texttt{PARALLEL\_TRANSLATION} translation method was used.}
	\label{fig-plot-cs-block-size-benchmark}
	\includegraphics[angle=0,width=\textwidth]{cs-block-size-benchmark.pdf}
\end{figure}

\begin{figure}[H]
	\caption{Performance vs. matrix size plot for the~custom implementation using StarPU and MKL routines.
			The~performance was measured on a~Karolina compute node.
			The~\texttt{NO\_TRANSLATION} translation method was used.}
	\label{fig-plot-karolina-mkl-flops-benchmark-block-size-comparison}
	\includegraphics[angle=0,width=\textwidth]{karolina-mkl-flops-benchmark-block-size-comparison.pdf}
\end{figure}

\begin{figure}[H]
	\caption{Trace of the~custom implementation using StarPU and MKL routines generated
			on a~Karolina compute node limited to 36 cores with a~$10000\times10000$ input matrix and blocks of size $200\times 200$.
			The~\texttt{NO\_TRANSLATION} translation method was used.}
	\label{fig-trace-too-many-tasks}
	\includegraphics[angle=0,width=0.95\textwidth]{trace-too-many-tasks.pdf}
\end{figure}

\begin{figure}[H]
	\caption{Trace of the~custom implementation using StarPU and MKL routines generated
			on a~Karolina compute node limited to 36 cores with a~$10000\times10000$ input matrix and blocks of size $800\times 800$.
			The~\texttt{NO\_TRANSLATION} translation method was used.}
	\label{fig-trace-too-few-tasks}
	\includegraphics[angle=0,width=0.95\textwidth]{trace-too-few-tasks.pdf}
\end{figure}


\subsection{Performance benchmarking results}

In this section, we will compare the~performance of individual implementations. Figure \ref{fig-plot-karolina-flops-benchmark}
shows the~performance on the~Karolina compute nodes (with block sizes set to 500 for all three custom implementations) and Figure
\ref{fig-plot-barbara-flops-benchmark} presents the~same comparison of the~Barbora compute nodes, also with $500\times 500$ blocks.

In these benchmarks, we compare our StarPU implementation with calls to optimized driver routines used by tasks with the~parallel
Cholesky routines from the~same libraries.

We can see that on both processors with the~x86-64 architecture, the~custom implementation has the~best results when linked with MKL
routines, which is consistent with the~fact that the~MKL implementation of \texttt{DPOTRF} yields the~overall best performance on both
processors. The~custom implementation reaches more than $95\%$ of the~performance of MKL \texttt{DPOTRF} for some block sizes
and on the~Karolina node, it seems to perform well even on smaller matrices.

On the~Complementary systems' A64FX processor, the~custom implementation using Arm Performance Libraries driver routines shows a
competitive performance for larger matrices, even surpassing the~Arm Performance Libraries implementation for $20000\times20000$ matrices.
The~custom implementation does not yield a~good performance for smaller matrices, but the~performance may be improved by using a~smaller
block size.

An~interesting observation is that the~maximum measured performances (in Gflop/s) are noticeably lower than the~theoretical peak performances
on all three compute nodes. On the~Barbora and Karolina nodes, this might be partially explained by the~relatively high-latency communication
between the~two CPUs that make up the~compute node, as examined in Section \ref{sec-parallelization-test-results}.

Figure \ref{fig-plot-karolina-mkl-translation-methods-flops-benchmark} compares the~performance of the~three different translation methods
on a~Karolina compute node, using the~StarPU custom implementation with MKL driver routines. It shows that the~presumed performance gain
from localizing the~blocks by scheme translation (which may lead to a~lower amount of cache misses with the~use of an~appropriate block size)
has been outweighed by the~time it takes to perform the~translation.

\begin{figure}[H]
	\caption{Performance vs. matrix size plot for several custom and library implementations.
			The~performance was measured on a~Karolina compute node with blocks of size $500\times 500$.
			The~\texttt{NO\_TRANSLATION} translation method was used.}
	\label{fig-plot-karolina-flops-benchmark}
	\includegraphics[angle=0,width=\textwidth]{karolina-flops-benchmark.pdf}
\end{figure}

\begin{figure}[H]
	\caption{Performance vs. matrix size plot for several custom and library implementations.
			The~performance was measured on a~Barbora compute node with blocks of size $500\times 500$.
			The~\texttt{NO\_TRANSLATION} translation method was used.}
	\label{fig-plot-barbara-flops-benchmark}
	\includegraphics[angle=0,width=\textwidth]{barbora-flops-benchmark.pdf}
\end{figure}

\begin{figure}[H]
	\caption{Performance vs. matrix size plot for several custom and library implementations.
			The~performance was measured on the~Arm node with blocks of size $1000\times 1000$.
			The~\texttt{NO\_TRANSLATION} translation method was used.}
	\label{fig-plot-cs-flops-benchmark}
	\includegraphics[angle=0,width=\textwidth]{cs-flops-benchmark.pdf}
\end{figure}

\begin{figure}[H]
	\caption{Performance vs. matrix size plot for all translation methods for the~custom implementation using StarPU MKL routines.
			The~performance was measured on a~Karolina compute node with blocks of size $500\times 500$.}
	\label{fig-plot-karolina-mkl-translation-methods-flops-benchmark}
	\includegraphics[angle=0,width=\textwidth]{karolina-mkl-translation-methods-flops-benchmark.pdf}
\end{figure}

\subsection{Parallelization benchmarking results}\label{sec-parallelization-test-results}

Figure \ref{fig-plot-karolina-mkl-parallelization-benchmark} shows the~relationship between performance and the~number of processor cores
that we allow the~runtime system to use (which is equivalent to the~number of threads). Note that the~$x$ axis of the~figure is
in logarithmic scale.

We can notice that the~performance improves with a~higher number of cores, up until the~64 core threshold. That number is precisely the
number of cores of one of the~two AMD EPYC 7H12 processors which make up the~compute node, so the~slowdown from 64 to 128 cores might be
explained by the~need for the~CPUs to communicate with each other. Inter-processor communication naturally has a~higher latency than
communication between cores on the~same CPU.

\begin{figure}[H]
	\caption{Performance vs. matrix size plot for the~custom implementation using StarPU and MKL routines.
			The~performance was measured on a~Karolina compute node with $10000\times10000$ input matrices and blocks of size $500\times 500$.
			The~\texttt{PARALLEL\_TRANSLATION} translation method was used.}
	\label{fig-plot-karolina-mkl-parallelization-benchmark}
	\includegraphics[angle=0,width=\textwidth]{karolina-mkl-parallelization-benchmark.pdf}
\end{figure}