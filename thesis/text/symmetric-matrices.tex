% This is a source file for the Properties of symmetric matrices chapter of Vít Břichňáč's bachelor thesis, Task-based Cholesky decomposition

\chapter{Properties of symmetric matrices}

\section{Matrices and vectors}
% Ref: Hefferon, Chapter 1 (Linear Systems), Section I (Solving Linear Systems), page 15
Hefferon \cite{Hefferon2002} defines an~$m\times n$ \textbf{matrix} as a~rectangular array of numbers with $m$ rows and $n$ columns.
Matrices are typically named with capital letters.

Each number in a~matrix is called an~\textbf{entry}.
The~entry in the~$i$th row and the~$j$th column of a~matrix $A$ is denoted $A_{ij}$.

The~set of all $m\times n$ matrices whose entries are real numbers is labeled $\R^{m,n}$.

% Ref: Hefferon, Chapter 3 (Vector Spaces), Section III (Basis and Dimension), page 138
The~\textbf{transpose} of a~matrix $A$ is a~matrix obtained by interchanging the~corresponding rows and columns of $A$ \cite{Hefferon2002}:
\begin{definition}\label{definition-transpose}
	A~transpose of a~matrix $A\in\R^{m,n}$ is a~matrix $A^T\in\R^{n,m}$ such that $A_{ij} = A^T_{ji}$
	for all $i\in\lbrace1, \dots, m\rbrace$ and $j\in\lbrace1, \dots, n\rbrace$.
\end{definition}

% Ref: Hefferon, Chapter 3 (Maps Between Spaces), Section IV (Matrix Operations), page 233
A~sum of two matrices of the~same size and the~scalar multiple of a~matrix are both defined element-wise \cite{Hefferon2002}:

\begin{definition}
	A~\textbf{sum} of two $m\times n$ matrices $A$ and $B$ is a~matrix $A+B\in\R^{m,n}$ such that
	\begin{equation*}
		(A+B)_{ij} = A_{ij} + B_{ij}
	\end{equation*}
	for each $i\in\lbrace1, \dots, m\rbrace$ and $j\in\lbrace1, \dots, n\rbrace$.
\end{definition}

\begin{definition}
	Let $\alpha\in\R$. A~\textbf{scalar multiple} of an~$m\times n$ matrix $A$  is a~matrix $\alpha A\in\R^{m,n}$ such that
	\begin{equation*}
		(\alpha A)_{ij} = \alpha A_{ij}
	\end{equation*}
	for each $i\in\lbrace1, \dots, m\rbrace$ and $j\in\lbrace1, \dots, n\rbrace$.
\end{definition}

% Ref: Hefferon, Chapter 3 (Maps Between Spaces), Section IV (Matrix Operations), page 237
A~matrix product is defined as follows:

\begin{definition}[\cite{Hefferon2002}, Definition 2.3]
	Let $A$ be an~$m\times r$ matrix and $B$ an~$r\times n$ matrix. A~\textbf{product} of $A$ and $B$ is a~matrix $AB\in\R^{m,n}$ such that
	\begin{equation*}
		(AB)_{ij} = \sum_{k=1}^{r} A_{ik} B_{kj}
	\end{equation*}
	for each $i\in\lbrace1, \dots, m\rbrace$ and $j\in\lbrace1, \dots, n\rbrace$.
\end{definition}

The product of matrices $A$ and $B$ can also be denoted by $A\cdot B$.

\newpage

\begin{theorem}\label{theorem-matrix-properties}
	Let $A\in\R^{m,n}, B, D\in\R^{n,o}, C\in\R^{o,p}$ and $\alpha\in\R$. Then:
	\begin{enumerate}
		\item $(AB)C = A(BC)$
		\item $A(\alpha B) = (\alpha A)B = \alpha (AB)$
		\item $A(B + D) = AB + AD$
		\item $(B + D)C = BC + DC$
		\item $A^TB^T = (BA)^T$
	\end{enumerate}
\end{theorem}


\begin{proof}
	\hspace{\fill}
	\begin{enumerate}
		\item $\begin{aligned}[t]\big((AB)C\big)_{ij} &= \Sum{l=1}{o}(AB)_{il}C_{lj} = \Sum{l=1}{o}\Sum{k=1}{n}(A_{ik}B_{kl})C_{lj} = 
		\Sum{k=1}{n}\Sum{l=1}{o}A_{ik}(B_{kl}C_{lj}) =\\
		&= \Sum{k=1}{n}A_{ik}(BC)_{kj} = \big(A(BC)\big)_{ij}\end{aligned}$
		\item $\begin{aligned}[t]\big(A(\alpha B)\big)_{ij} &= \Sum{k=1}{n}A_{ik}(\alpha B)_{kj} =
		\Sum{k=1}{n}\alpha A_{ik}B_{kj} = \Sum{k=1}{n}(\alpha A)_{ik}B_{kj} = \big((\alpha A)B\big)_{ij} =\\
		&= \alpha\Sum{k=1}{n}A_{ik}B_{kj} = \big(\alpha(AB)\big)_{ij}\end{aligned}$
		\item $\big(A(B + D)\big)_{ij} = \Sum{k=1}{n}A_{ik}(B+D)_{kj} = \Sum{k=1}{n}A_{ik}(B_{kj}+D_{kj}) =
		\Sum{k=1}{n}A_{ik}B_{kj}+A_{ik}D_{kj} = (AB + AD)_{ij}$
		\item $\big((B + D)C\big)_{ij} = \Sum{k=1}{n}(B+D)_{ik}C_{kj} = \Sum{k=1}{n}(B_{ik}+D_{ik})C_{kj} =
		\Sum{k=1}{n}B_{ik}C_{kj}+D_{ik}C_{kj} = (BC + DC)_{ij}$
		\item $(A^TB^T)_{ij} = \Sum{k=1}{n}(A^T)_{ik}(B^T)_{kj} = \Sum{k=1}{n}A_{ki}B_{jk} = \Sum{k=1}{n}B_{jk}A_{ki} =
		(BA)_{ji} = \big((BA)^T\big)_{ij}$
	\end{enumerate}
\end{proof}

\subsection{Square matrices}

An~$n\times n$ matrix is called a~\textbf{square matrix} of size $n$.

% Ref: Lay & Lay & McDonald, Section 2.1 (Matrix Operations), page 94
A~diagonal matrix is a~square matrix whose non-diagonal entries are zero \cite{LayLayMcDonald2014}, or more precisely:
\begin{definition}
	A~square matrix $A\in\R^{n,n}$ is called \textbf{diagonal} if $A_{ij}=0$ for all\\$i,j\in\lbrace1, \dots, n\rbrace, i\neq j$.
\end{definition}

By relaxing the~restriction so that only the~entries above or below the~main diagonal need to be zero, we obtain a~triangular matrix:
\begin{definition}
	A~square matrix $A\in\R^{n,n}$ is:
	\begin{itemize}
		\item \textbf{lower triangular}, if $A_{ij} = 0$ for all $i,j\in\lbrace1, \dots, n\rbrace, i < j$
		\item \textbf{upper triangular}, if $A_{ij} = 0$ for all $i,j\in\lbrace1, \dots, n\rbrace, i > j$
	\end{itemize}
\end{definition}

A~special case of a~diagonal matrix is an~identity matrix:
% Ref: Lay & Lay & McDonald, Section 2.1 (Matrix Operations), page 94
\begin{definition}[\cite{LayLayMcDonald2014}]
	An~\textbf{identity matrix} of size $n$ is a~square matrix $I\in\R^{n,n}$,
	where $$\forall i,j\in\lbrace1, \dots, n\rbrace: \quad
	I_{ij} = \begin{cases}
		1, &\text{if }i=j,\\
		0, &\text{if }i\neq j.
	\end{cases}$$
\end{definition}

% Ref: Hefferon, Chapter 3 (Maps Between Spaces), Section IV (Matrix Operations), page 248
For the~rest of this thesis, the~identity matrix will always be labeled $I$.
The~identity matrix acts as the~identity element with respect to matrix multiplication \cite{Hefferon2002}:

\begin{theorem}
	Let $A\in\R^{m,n}, B\in\R^{n,m}$ and let $I$ be the~$n\times n$ identity matrix. Then:
	$$ AI = A \quad \text{and} \quad IB = B.$$
\end{theorem}

\begin{proof}
	$$(AI)_{ij} = \Sum{k=1}{n}A_{ik}I_{kj} = \Sum{\substack{k\in\lbrace1,\dots,n\rbrace\\k\neq j}}{}A_{ik}I_{kj}
	+ A_{ij}I_{jj} = \Sum{\substack{k\in\lbrace1,\dots,n\rbrace\\k\neq j}}{}A_{ik}\cdot 0 + A_{ij}\cdot 1 = A_{ij}$$
	$$(IB)_{ij} = \Sum{k=1}{n}I_{ik}B_{kj} = \Sum{\substack{k\in\lbrace1,\dots,n\rbrace\\k\neq i}}{}I_{ik}B_{kj}
	+ I_{ii}B_{ij} = \Sum{\substack{k\in\lbrace1,\dots,n\rbrace\\k\neq i}}{}0\cdot B_{kj} + 1\cdot B_{ij} = B_{ij}$$	
\end{proof}

An~important property of square matrices that has many different (but equivalent) definitions across different linear algebra
texts or textbooks, is nonsingularity:

% Ref: Lay & Lay & McDonald, Chapter 2 (Matrix Algebra), Section 2.2 (The Inverse of a Matrix), page 105
\begin{definition}[\cite{LayLayMcDonald2014}]
	A~square matrix $A\in\R^{n,n}$ is called \textbf{nonsingular} (or \textbf{invertible}) if~there exists a~matrix $C\in\R^{n,n}$ such that
	\begin{equation*}
		AC=I \quad \text{and} \quad CA=I.
	\end{equation*}
	$C$ is then called an~\textbf{inverse} of $A$. A~matrix that is not invertible is called \textbf{singular}.
\end{definition}

\begin{theorem}
	The~inverse of a~nonsingular matrix $A\in\R^{n,n}$ is determined uniquely, that is, if there are two
	$n\times n$ matrices $B$ and $C$ such that $AB=BA=AC=CA=I$, then $B=C$.
\end{theorem}

\begin{proof}
	Consider $B, C\in\R^{n,n}$ such that $AB=BA=AC=CA=I$. Then:
	\begin{align*}
		AB &= AC\\
		B(AB) &= B(AC)\\
		(BA)B &= (BA)C\\
		IB &= IC\\
		B &= C
	\end{align*}
\end{proof}

% Ref: Lay & Lay & McDonald, Chapter 2 (Matrix Algebra), Section 2.2 (The Inverse of a Matrix), page 105
The~(unique) inverse of a~matrix $A$ is denoted by $A^{-1}$ \cite{LayLayMcDonald2014}.

\begin{theorem}\label{theorem-nonsingular-transpose}
	$A\in\R^{n,n}$ is a~nonsingular matrix if and only if $A^T$ is nonsingular.
	Moreover, if $A$ is nonsingular, then $(A^T)^{-1} = (A^{-1})^T$.
\end{theorem}

\begin{proof}
	If $A$ is nonsingular, we can show that $(A^{-1})^T$ is the~inverse of $A^T$ using Theorem \ref{theorem-matrix-properties}:
	\begin{align*}
		A^T(A^{-1})^T = (A^{-1}A)^T = I^T = I\\
		(A^{-1})^TA^T = (AA^{-1})^T = I^T = I
	\end{align*}

	Furthermore, if $A^T$ is nonsingular, then $A^{-1} = \big((A^T)^{-1}\big)^T$ since $A = (A^T)^T$.
\end{proof}

\begin{theorem}\label{theorem-nonsingular-product}
	Let $A,B\in\R^{n,n}$ be two nonsingular matrices. Then $AB$ is nonsingular with the~inverse $(AB)^{-1}=B^{-1}A^{-1}$.
\end{theorem}

\begin{proof}
	\begin{align*}
		(B^{-1}A^{-1})AB &= B^{-1}(A^{-1}A)B = B^{-1}B = I\\
		AB(B^{-1}A^{-1}) &= A(BB^{-1})A^{-1} = AA^{-1} = I
	\end{align*}
\end{proof}

A~\textbf{determinant} of a~square matrix $A$ (labeled $\det A$) is a~number that fully determines the~singularity of $A$ -- more precisely,
$\det A \neq 0$ if and only if $A$ is nonsingular. There are multiple equivalent definitions of the~determinant
\cite{Hefferon2002,LayLayMcDonald2014}, but for the~purposes of this thesis, we will present a~determinant formula for lower triangular
matrices only:

\begin{theorem}[\cite{LayLayMcDonald2014}, Theorem 3.1.2]
	Let $A\in\R^{n,n}$ be a~lower triangular matrix. Then the~determinant of $A$ is equal to
	$$\det A = \prod_{i=1}^{n}A_{ii}.$$
\end{theorem}

\begin{theorem}[\cite{LayLayMcDonald2014}, Theorem 3.2.4]
	\label{theorem-determinant}
	A~lower triangular matrix $A\in\R^{n,n}$ is invertible if and only if $\det A\neq 0$.
\end{theorem}

\subsection{Vectors}

Starting from this subsection, we will consider a~real number $x$ and a~$1\times 1$ matrix whose only entry is $x$ to be equal,
or, in other words, $x = \begin{pmatrix}x\end{pmatrix}$ for all $x\in\R$. Note that this convention does not cause any conflict
with basic matrix operations as defined above, since $\begin{pmatrix}x\end{pmatrix} + \begin{pmatrix}y\end{pmatrix} = \begin{pmatrix}x+y\end{pmatrix}$
and $\begin{pmatrix}x\end{pmatrix} \cdot \begin{pmatrix}y\end{pmatrix} = x \cdot \begin{pmatrix}y\end{pmatrix} = \begin{pmatrix}x\cdot y\end{pmatrix}$
for all $x,y\in\R$.

% Ref: Hefferon, Section 1 (solving linear systems), page 16
\begin{definition}[\cite{Hefferon2002}, Definition I.2.8]
	An~$n\times1$ matrix (that is, a~matrix with a~single column) is called a~\textbf{vector}.
\end{definition}

The~entries $x_{11}, x_{21}, \dots, x_{n1}$ of a~vector $x$ can be also represented by $x_1, x_2, \dots, x_n$.

% Ref: Hefferon, Section 1 (solving linear systems), page 16
\begin{definition}[\cite{Hefferon2002}, Definition I.2.8]
	A~\textbf{zero vector} is a~vector whose entries are all equal to zero.
\end{definition}

Zero vectors will be further labeled $\theta$.

% Ref: Hefferon, Section 2 (linear geometry), page 42
\begin{definition}[\cite{Hefferon2002}, Definition II.2.1]
	The~\textbf{length} (or more formally, the~\textbf{Euclidean norm}) of a~vector $x\in\R^{n,1}$ is defined as
	\begin{equation*}
		\|x\| = \sqrt{\sum_{i=1}^{n}x_i^2}.
	\end{equation*}
\end{definition}

It is important to note (and easy to see) that a~length of every vector is non-negative and that
a~vector has a~length of zero if and only if it is a~zero vector.

\begin{lemma}
	Let $x\in\R^{n,1}$ be a~vector. Then
	\begin{equation*}
		\|x\|^2 = x^Tx.
	\end{equation*}
\end{lemma}

\begin{proof}
	$$\|x\|^2 = \Big(\sqrt{\sum_{i=1}^{n}x_i^2}\Big)^2 = \sum_{i=1}^{n}x_i^2 =
	\sum_{i=1}^{n}(x^T)_{1i}x_{i1} = x^Tx$$
\end{proof}

The~following theorem is provided without a~proof, as the~proof usually consists of proving a~larger chain of implications.

% Ref: Lay & Lay & McDonald, Chapter 2 (Matrix Algebra), Section 2.3 (Characterizations of Invertible Matrices), site 114
\begin{theorem}[\cite{LayLayMcDonald2014}, Theorem 2.8]\label{theorem-nonsingular-property}
	Consider a~square matrix $A\in\R^{n,n}$. Then
	\begin{equation*}
		A\text{ is nonsingular} \quad \Leftrightarrow \quad \forall x\in\R^{n,1},x\neq\theta: \|Ax\|>0.
	\end{equation*}
\end{theorem}

\section{Symmetric matrices}

% Ref: ThefethenBau, Lecture 23 (Cholesky factorization), section "Hermitian positive definite matrices", site 172
A~symmetric matrix is defined by Trefethen and Bau \cite{TrefethenBau2022} as a~square matrix that is equal to its transpose.
An~equivalent definition without using matrix transposition can be formed:
\begin{definition}[\cite{TrefethenBau2022}]
	A~square matrix $A\in\R^{n,n}$ is \textbf{symmetric} if and only if $A_{ij} = A_{ji}$\\
	for all $i, j\in\lbrace1, \dots, n\rbrace$.
\end{definition}

\begin{lemma}\label{lemma-symmetric-matrix-properties}
	Symmetric matrices have the~following properties:
	\begin{enumerate}
		\item For a~general matrix $X\in\R^{m,n}$, $X^TX$ and $XX^T$ are symmetric
		\item A~sum of two symmetric matrices is symmetric
		\item A~scalar multiple of a~symmetric matrix is symmetric
		\item Every diagonal matrix is symmetric
	\end{enumerate}
\end{lemma}

\begin{proof}
	For a~general matrix $X\in\R^{m,n}$, symmetric matrices $A,B\in\R^{n,n}$ and a~diagonal matrix $D\in\R^{n,n}$:
	\begin{enumerate}
		\item $(X^TX)_{ij} = \Sum{k=1}{m}(X^T)_{ik}\cdot X_{kj} = \Sum{k=1}{m}X_{ki}\cdot (X^T)_{jk} = \Sum{k=1}{m}(X^T)_{jk}\cdot X_{ki} = (X^TX)_{ji}$ \\
		$(XX^T)_{ij} = \Sum{k=1}{m}X_{ik}\cdot (X^T)_{kj} = \Sum{k=1}{m}(X^T)_{ki}\cdot X_{jk} = \Sum{k=1}{m}X_{jk}\cdot (X^T)_{ki} = (XX^T)_{ji}$
		\item $(A+B)_{ij} = A_{ij} + B_{ij} = A_{ji} + B_{ji} = (A+B)_{ji}$
		\item $(kA)_{ij} = k\cdot A_{ij} = k \cdot A_{ji} = (kA)_{ji}$
		\item By definition, $D_{ij}=D_{ji}=0$ for all $i\neq j$
	\end{enumerate}
\end{proof}

\section{Matrix definiteness}

% Ref: Thefethen & Bau, Lecture 23 (Cholesky factorization), section "Hermitian positive definite matrices", site 173

\begin{definition}[\cite{TrefethenBau2022}]
	A~symmetric matrix $A$ is said to be \textbf{positive definite} if, for every non-zero vector $x\in\R^{n,1}$, it holds that
	\begin{equation*}
		x^TAx > 0.
	\end{equation*}
\end{definition}

\noindent The~term $x^TAx$ for a~square matrix $A\in\real^{n,n}$ is called a~\textbf{quadratic form} determined by matrix $A$ \cite{LayLayMcDonald2014}.

\begin{lemma}\label{lemma-positive-definiteness-of-product}
	Let $A\in\R^{n,n}$ be a~nonsingular matrix. Then $A^TA$ and $AA^T$ are symmetric positive definite matrices.
\end{lemma}

\begin{proof}
	We already know that $A^TA$ and $AA^T$ are symmetric from Lemma \ref{lemma-symmetric-matrix-properties}.\\
	Following Theorems \ref{theorem-nonsingular-transpose} and \ref{theorem-nonsingular-property},
	we obtain for all non-zero vectors $x\in\R^{n,1}$:
	\begin{align*}
		x^T(A^TA)x = (x^TA^T)(Ax) = (Ax)^TAx = \|Ax\|^2 > 0\\
		x^T(AA^T)x = (x^TA)(A^Tx) = (A^Tx)^TAx = \|A^Tx\|^2 > 0
	\end{align*}
\end{proof}


% Ref. Trefethen & Bau, Lecture 23 (Cholesky factorization), section "Hermitian positive definite matrices", site 172-173
A~fundamental property of symmetric positive definite matrices that we will use later
is the~positive definiteness of their principal submatrices \cite{TrefethenBau2022}.
First, we will define what a~principal submatrix is.

\begin{definition}
	Let $A\in\R^{n,n}$ and $S\subsetneq\lbrace 1,\dots,n\rbrace$. A~submatrix of $A$ obtained by removing rows with indices $S$ and
	columns with indices $S$ from $A$ is called a~\textbf{principal submatrix} of $A$. Furthermore, if $S=\lbrace i,i+1,\dots,n\rbrace$ for some
	$i\in\lbrace 1,\dots,n\rbrace$, we call the~submatrix a~\textbf{leading principal submatrix}.
\end{definition}

\begin{theorem}\label{theorem-positive-definitess-of-principal-submatrix}
	Let $A\in\R^{n,n}$ be a~symmetric positive definite matrix.
	Then its principal submatrix $A'$ obtained by removing the~rows and columns with indices $S\subsetneq\lbrace1,\dots,n\rbrace$ is also symmetric positive definite.
\end{theorem}

\begin{proof}
	Symmetry of $A'$ is trivial. Let $R_k=\lbrace1,\dots,n\rbrace\setminus S$ and let $R_k$ be the~$k$th smallest element from $R$.
	For every $x'\in\R^{|R|,1}, x'\neq\theta$, we can define $x\in\R^{n,1}$ entry-wise:
	
	$$x_i=\begin{cases}
		0,&\text{if }i\in S\\
		x'_k,&\text{if }i = R_k\text{ for some } k\in\lbrace1,\dots,|R|\rbrace
	\end{cases}$$
	Then:
	\begin{equation*}
		(x')^TA'x' = \sum_{k=1}^{|R|}x'_kA'_{kk}x'_k = \sum_{k\in S}0\cdot A_{ii}\cdot 0 + \sum_{k\in R}x_kA_{kk}x_k = \sum_{k=1}^{n}x_kA_{kk}x_k = x^TAx > 0
	\end{equation*}
	The~last inequality follows from the~positive definiteness of $A$.
\end{proof}

\subsection{Generating symmetric positive definite matrices}

We can obtain a~symmetric positive definite matrix $A$ from a~given nonsingular matrix $R$ using the~following formula
(see Lemma \ref{lemma-positive-definiteness-of-product}):
\begin{equation}\label{non-regularized-sypod-matrix-generation-formula}
	A = R^TR
\end{equation}

However, a~randomly generated square matrix is not guaranteed to be invertible.
Furthermore, even in the~case of $R$ being nonsingular, problems with numerical stability can arise for matrices $R$ that are
``nearly singular'' (i. e., they have a~very large condition number).

To prevent $R$ from being singular or nearly singular, we can add a~positive scalar multiple of an~identity matrix
to the~right-hand side of eq. \eqref{non-regularized-sypod-matrix-generation-formula}:
\begin{equation}\label{regularized-sypod-matrix-generation-formula}
	A=R^TR+\lambda I
\end{equation}
where $I$ is an~identity matrix of size $n$ and $\lambda>0$. Lemma \ref{lemma-symmetric-matrix-properties} can be used to show that
the~matrix $A$ obtained from eq. \eqref{regularized-sypod-matrix-generation-formula} is symmetric.
The~quadratic form determined by matrix $A$ is equal to:
\begin{equation}
	x^T Ax=x^T(R^TR+\lambda I)x = x^TR^TRx + x^T\lambda Ix = (Rx)^TRx + \lambda x^Tx = \|Rx\|^2 + \lambda \|x\|^2
\end{equation}
which is positive for every non-zero vector $x\in\R^n$ due to both $\lambda$ and $\|x\|$ being positive.
% Note: a zero vector is a vector with all components equal to 0, a null vector is a vector of length 0

Thus, every square matrix $A$ generated using eq. \eqref{regularized-sypod-matrix-generation-formula} is symmetric positive definite.

In summary, we have obtained the~following algorithm for generating symmetric positive definite matrices:

\vspace*{3ex}

\begin{algorithm} [H]\label{alg-spd-matrix-generation}
	\caption{Generation of symmetric positive definite matrices}
	\KwData{$\lambda>0$}
	\KwResult{a~symmetric positive definite matrix $A$}
	Generate a~random matrix $R\in\R^{n,n}$\\
	$A\gets R^TR$\\
	\Comment{// this can be done using the~GEMM procedure described in Section \ref{sec-blas-gemm}}
	\For{$i\gets1;i\le n;i\gets i+1$}{
		$A_{ii}\gets A_{ii}+\lambda$
	}
	\Return A
\end{algorithm}

\section{Block matrices}

In this thesis, we will often work with \textbf{block matrices} (or \textbf{partitioned matrices}), that is, matrices obtained by joining
smaller matrices (called blocks) in a~particular way.

\begin{definition}[reformulation of Definition 2.10 in \cite{Nicholson2021}]\label{definition-block-matrix}
	\hspace{\fill} \\
	Let $A_{11},\dots,A_{1n},A_{21},\dots,A_{2n},\dots,A_{m1},\dots,A_{mn}$ be matrices where:
	\begin{itemize}
		\item for all $i\in\lbrace1,\dots,m\rbrace$, matrices $A_{i1}$ to $A_{in}$ have $m_{i}$ rows
		\item for all $j\in\lbrace1,\dots,n\rbrace$, matrices $A_{1j}$ to $A_{mj}$ have $n_{j}$ columns
	\end{itemize}
	We define the~block matrix $A=\begin{pmatrix}
		A_{11}&\dots&A_{1n}\\
		\vdots&\ddots&\vdots\\
		A_{mn}&\dots&A_{mn}
	\end{pmatrix}$ such that its $x,y$th entry is equal to the~$u,v$th entry of $A_{ij}$, where $x=u+\Sum{k=1}{i-1}m_{k}$ and
	$y=v+\Sum{k=1}{j-1}n_{k}$.

	The~matrices $A_{11}$ to $A_{mn}$ are called the~\textbf{blocks} of $A$.
\end{definition}

In other words, we obtain the~matrix $A$ by ``joining'' the~entries of the~blocks where:
\begin{itemize}
	\item The~entries of $A_{ij}$ are directly above the~entries of $A_{(i+1)j}$ for all $i\in\lbrace1,\dots,m-1\rbrace$ and $j\in\lbrace1,\dots,n\rbrace$
	\item The~entries of $A_{ij}$ are directly to the~left of the~entries of $A_{i(j+1)}$ for all $i\in\lbrace1,\dots,m\rbrace$ and $j\in\lbrace1,\dots,n-1\rbrace$
\end{itemize}

We consider a~block matrix comprised of a~single block to be equivalent to that block, i.e., $\begin{pmatrix}
	B
\end{pmatrix} = B$ for all matrices $B\in\R^{m,n}$.

Multiplication of block matrices works in the~same way as standard entry-wise matrix multiplication if we consider the~blocks
to be the~entries of those matrices and if the~pairs of blocks being multiplied together are compatible for multiplication (that is,
every left-hand side block has the same number of columns as the number of rows of the corresponding right-hand side block).

\newpage

\begin{theorem}[reformulation of Theorem 2.3.4 in \cite{Nicholson2021}]
	\hspace{\fill} \\
	Let $A=\begin{pmatrix}
		A_{11}&\dots&A_{1p}\\
		\vdots&\ddots&\vdots\\
		A_{m1}&\dots&A_{mp}
	\end{pmatrix}$ and $B=\begin{pmatrix}
		B_{11}&\dots&B_{1n}\\
		\vdots&\ddots&\vdots\\
		B_{p1}&\dots&B_{pn}
	\end{pmatrix}$ be two block matrices for some $m,p,n\in\N$ where the~number of columns of $A_{ik}$ is equal to the~number of rows
	of $B_{kj}$ for all $i\in\lbrace1,\dots,m\rbrace$, $j\in\lbrace1,\dots,n\rbrace$ and
	$k\in\lbrace1,\dots,p\rbrace$. Then
	
	$$AB=\begin{pmatrix}
		\Sum{k=1}{p}A_{1k}B_{k1}&\dots&\Sum{k=1}{p}A_{1k}B_{kn}\\
		\vdots&\ddots&\vdots\\
		\Sum{k=1}{p}A_{mk}B_{k1}&\dots&\Sum{k=1}{p}A_{mk}B_{kn}
	\end{pmatrix}.$$
\end{theorem}

Transposition of blocked matrices is performed by reordering the~transposes of the~blocks in the~same way as we reorder the~entries
when transposing matrices entry-wise:

\begin{theorem}
	Let $A=\begin{pmatrix}
		A_{11}&\dots&A_{1n}\\
		\vdots&\ddots&\vdots\\
		A_{m1}&\dots&A_{mn}
	\end{pmatrix}$ be a~block matrix.
	
	Then $A^T = \begin{pmatrix}
		A_{11}^T&\dots&A_{m1}^T\\
		\vdots&\ddots&\vdots\\
		A_{1n}^T&\dots&A_{mn}^T
	\end{pmatrix}$.
\end{theorem}

\begin{proof}
	By Definitions \ref{definition-transpose} and \ref{definition-block-matrix}, the~following entries are equal:
	\begin{itemize}
		\item The~$y,x$th entry of $A^T$
		\item The~$x,y$th entry of $A$
		\item The~$u,v$th entry of $A_{ij}$, where $x=u+\Sum{k=1}{i-1}m_{k}$ and $y=v+\Sum{k=1}{j-1}n_{k}$
		\item The~$v,u$th entry of $A_{ij}^T$, where $x=u+\Sum{k=1}{i-1}m_{k}$ and $y=v+\Sum{k=1}{j-1}n_{k}$
	\end{itemize}

	Considering that all of the~matrices $A_{i1}^T$ to $A_{in}^T$ have $n_{i}$ columns and the~matrices
	$A_{1j}^T$ to $A_{mj}^T$ have $m_{j}$ rows, all of the~entries stated above are also equal to the~$y,x$th entry of
	$\begin{pmatrix}
		A_{11}^T&\dots&A_{m1}^T\\
		\vdots&\ddots&\vdots\\
		A_{1n}^T&\dots&A_{mn}^T
	\end{pmatrix}$
	and we thus have $A^T = \begin{pmatrix}
		A_{11}^T&\dots&A_{m1}^T\\
		\vdots&\ddots&\vdots\\
		A_{1n}^T&\dots&A_{mn}^T
	\end{pmatrix}$.
\end{proof}