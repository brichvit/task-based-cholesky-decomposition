#include "../../src/gemm/gemm.h"
#include "../../src/syrk/syrk.h"
#include "../../src/trsm/trsm.h"

// Defined in the test suite
void xerbla_ (char* SRNAME, int* INFO);

#define DEREF_PARAM(PARAM,PARAM_PTR,TYPE) TYPE PARAM = *PARAM_PTR

void dgemm_ (char* TRANSA_PTR, char* TRANSB_PTR,
			 int* M_PTR, int* N_PTR, int* K_PTR,
			 double* ALPHA_PTR, double* A, int* LDA_PTR,
			 double* B, int* LDB_PTR,
			 double* BETA_PTR, double* C, int* LDC_PTR) {
	DEREF_PARAM (TRANSA, TRANSA_PTR, char);
	DEREF_PARAM (TRANSB, TRANSB_PTR, char);

	DEREF_PARAM (M, M_PTR, int);
	DEREF_PARAM (K, K_PTR, int);
	DEREF_PARAM (N, N_PTR, int);

	DEREF_PARAM (ALPHA, ALPHA_PTR, double);
	DEREF_PARAM (BETA, BETA_PTR, double);

	DEREF_PARAM (LDA, LDA_PTR, int);
	DEREF_PARAM (LDB, LDB_PTR, int);
	DEREF_PARAM (LDC, LDC_PTR, int);

	int dgemm_result = dgemm (TRANSA, TRANSB,
							  M, N, K,
							  ALPHA, A, LDA,
							  B, LDB,
							  BETA, C, LDC);

	if (dgemm_result != 0) {
		xerbla_ ("DGEMM ", &dgemm_result);
	}
}

void dsyrk_ (char* UPLO_PTR, char* TRANS_PTR, int* N_PTR, int* K_PTR,
			double* ALPHA_PTR, double* A, int* LDA_PTR,
			double* BETA_PTR, double* C, int* LDC_PTR) {

	DEREF_PARAM (UPLO, UPLO_PTR, char);
	DEREF_PARAM (TRANS, TRANS_PTR, char);

	DEREF_PARAM (N, N_PTR, int);
	DEREF_PARAM (K, K_PTR, int);

	DEREF_PARAM (ALPHA, ALPHA_PTR, double);
	DEREF_PARAM (BETA, BETA_PTR, double);

	DEREF_PARAM (LDA, LDA_PTR, int);
	DEREF_PARAM (LDC, LDC_PTR, int);

	int dsyrk_result = dsyrk (UPLO, TRANS, N, K,
							  ALPHA, A, LDA,
							  BETA, C, LDC);

	if (dsyrk_result != 0) {
		xerbla_ ("DSYRK ", &dsyrk_result);
	}
}

void dtrsm_ (char* SIDE_PTR, char* UPLO_PTR, char* TRANS_PTR, char* DIAG_PTR,
			int* M_PTR, int* N_PTR,
			double* ALPHA_PTR, double* A, int* LDA_PTR,
			double* B, int* LDB_PTR) {
	
	DEREF_PARAM (SIDE, SIDE_PTR, char);
	DEREF_PARAM (UPLO, UPLO_PTR, char);
	DEREF_PARAM (TRANS, TRANS_PTR, char);
	DEREF_PARAM (DIAG, DIAG_PTR, char);

	DEREF_PARAM (M, M_PTR, int);
	DEREF_PARAM (N, N_PTR, int);

	DEREF_PARAM (ALPHA, ALPHA_PTR, double);

	DEREF_PARAM (LDA, LDA_PTR, int);
	DEREF_PARAM (LDB, LDB_PTR, int);

	int dtrsm_result = dtrsm (SIDE, UPLO, TRANS, DIAG,
							  M, N,
							  ALPHA, A, LDA,
							  B, LDB);

	if (dtrsm_result != 0) {
		xerbla_ ("DTRSM ", &dtrsm_result);
	}
}
