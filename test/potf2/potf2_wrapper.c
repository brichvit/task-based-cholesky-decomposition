#include "../../src/potf2/potf2.h"

// Defined in the test suite
void call_xerbla_ (char* SRNAME, int* INFO);

#define DEREF_PARAM(PARAM,PARAM_PTR,TYPE) TYPE PARAM = *PARAM_PTR

void dpotrf_ (char* UPLO_PTR, int* N_PTR, double* A, int* LDA_PTR, int* INFO_PTR) {
	DEREF_PARAM (UPLO, UPLO_PTR, char);

	DEREF_PARAM (N, N_PTR, char);
	DEREF_PARAM (LDA, LDA_PTR, char);

	*INFO_PTR = dpotf2 (UPLO, N, A, LDA);

	if (*INFO_PTR < 0) {
		int error_code = -*INFO_PTR;
		call_xerbla_ ("DPOTRF", &error_code);
	}
}

void dpotf2_ (char* UPLO_PTR, int* N_PTR, double* A, int* LDA_PTR, int* INFO_PTR) {
	DEREF_PARAM (UPLO, UPLO_PTR, char);

	DEREF_PARAM (N, N_PTR, char);
	DEREF_PARAM (LDA, LDA_PTR, char);

	*INFO_PTR = dpotf2 (UPLO, N, A, LDA);

	if (*INFO_PTR < 0) {
		int error_code = -*INFO_PTR;
		call_xerbla_ ("DPOTF2", &error_code);
	}
}
