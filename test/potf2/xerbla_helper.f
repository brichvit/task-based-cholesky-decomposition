*     A simple helper subroutine to be able to call the XERBLA error handing subroutine from LAPACK with a string of unknown length from C
*
      SUBROUTINE CALL_XERBLA( NAME, INFO )

      CHARACTER*6        NAME
      INTEGER            INFO

      EXTERNAL           XERBLA

      CALL XERBLA (NAME, INFO)
      
      END