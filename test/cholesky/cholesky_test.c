#include <stdlib.h>
#include <stdio.h>

#include "../../src/cholesky/cholesky.h"
#include "../../src/common/math_util.h"

#if defined (USE_STARPU)
#include <starpu.h>
#endif

#define STARPU_RUNTIME_SYSTEM 1
#define OPENMP_RUNTIME_SYSTEM 2

#if defined (USE_STARPU) && defined (USE_OPENMP)
int runtime_systems[] = {STARPU_RUNTIME_SYSTEM, OPENMP_RUNTIME_SYSTEM};
#elif defined (USE_STARPU)
int runtime_systems[] = {STARPU_RUNTIME_SYSTEM};
#elif defined (USE_OPENMP)
int runtime_systems[] = {OPENMP_RUNTIME_SYSTEM};
#endif

int matrix_sizes[] = {0, 1, 2, 3, 5, 10, 50, 100};
int block_sizes[] = {1, 3, 3, 3, 20, 90};
int matrix_types[] = {1, 2};
translation_method_e translation_methods[] = {NO_TRANSLATION, SEQUENTIAL_TRANSLATION, PARALLEL_TRANSLATION};
int right_hand_side_counts[] = {1, 2, 15};
int iseeds[] = {1988, 1989, 1990, 1991};

double test_threshold = 30;

#define NO_TESTS 8

// Fortran methods, we have to declare them here because of a lack of header files
void dlatb4_ (char PATH[3], int* IMAT, int* M, int* N, char* TYPE, int* KL, int* KU, double* ANORM, int* MODE, double* CNDNUM, char* DIST);
void dlatms_ (int* M, int* N, char* DIST, int ISEED[4], char* SYM, double* D, int* MODE, double* COND, double* DMAX, int* KL, int* KU, char* PACK, double* A, int* LDA, double* WORK, int* INFO);
void alaerh_ (char PATH[3], char* SUBNAM, int* INFO, int* INFOE, char* OPTS, int* M, int* N, int* KL, int* KU, int* N5, int* IMAT, int* NFAIL, int* NERRS, int* NOUT);
void xlaenv_ (int* ISPEC, int* NVALUE);
void dlacpy_ (char* UPLO, int* M, int* N, double* A, int* LDA, double* B, int* LDB);
void dpot01_ (char* UPLO, int* N, double* A, int* LDA, double* AFAC, int* LDAFAC, double* RWORK, double* RESID);
void dpotri_ (char* UPLO, int* N, double* A, int* LDA, int* INFO);
void dpot03_ (char* UPLO, int* N, double* A, int* LDA, double* AINV, int* LDAINV, double* WORK, int* LDWORK, double* RWORK, double* RCOND, double* RESID );
void alahd_ (int* IOUNIT, char PATH[3]);
void dlarhs_ (char PATH[3], char* XTYPE, char* UPLO, char* TRANS, int* M, int* N, int* KL, int* KU, int* NRHS, double* A, int* LDA, double* X, int* LDX, double* B, int* LDB, int ISEED[4], int* INFO);
void dpotrs_ (char* UPLO, int* N, int* NRHS, double* A, int* LDA, double* B, int* LDB, int* INFO);
void dpot02_ (char* UPLO, int* N, int* NRHS, double* A, int* LDA, double* X, int* LDX, double* B, int* LDB, double* RWORK, double* RESID);
void dget04_ (int* N, int* NRHS, double* X, int* LDX, double* XACT, int* LDXACT, double* RCOND, double* RESID);
void dporfs_ (char* UPLO, int* N, int* NRHS, double* A, int* LDA, double* AF, int* LDAF, double* B, int* LDB, double* X, int* LDX, double* FERR, double* BERR, double* WORK, int* IWORK, int* INFO);
void dpot05_ (char* UPLO, int* N, int* NRHS, double* A, int* LDA, double* B, int* LDB, double* X, int* LDX, double* XACT, int* LDXACT, double* FERR, double* BERR, double* RESLTS);
double dlansy_ (char* NORM, char* UPLO, int* N, double* A, int* LDA, double* WORK);
void dpocon_ (char* UPLO, int* N, double* A, int* LDA, double* ANORM, double* RCOND, double* WORK, int* IWORK, int* INFO);
double dget06_ (double* RCOND, double* RCONDC);
void alasum_ (char TYPE[3], int* NOUT, int* NFAIL, int* NRUN, int* NERRS);

// Constants to be used as arguments to Fortran routines (passed by reference)
int minus_one = -1;
int zero = 0;
int one = 1;
int six = 6;

int main () {
	int runtime_systems_len = sizeof (runtime_systems) / sizeof (*runtime_systems);
	int matrix_sizes_len = sizeof (matrix_sizes) / sizeof (*matrix_sizes);
	int block_sizes_len = sizeof (block_sizes) / sizeof (*block_sizes);
	int matrix_types_len = sizeof (matrix_types) / sizeof (*matrix_types);
	int translation_methods_len = sizeof (translation_methods) / sizeof (*translation_methods);
	int right_hand_side_counts_len = sizeof (right_hand_side_counts) / sizeof (*right_hand_side_counts);

	printf ("Tests of the block Cholesky decomposition routine");

	int max_right_hand_side_count = right_hand_side_counts[right_hand_side_counts_len - 1];

	char* path = "DPO";

	int tests_run = 0;
	int tests_failed = 0;
	int no_errors = 0;

	for (int runtime_system_id = 0; runtime_system_id < runtime_systems_len; runtime_system_id++) {
		int runtime_system = runtime_systems[runtime_system_id];

		for (int matrix_size_id = 0; matrix_size_id < matrix_sizes_len; matrix_size_id++) {
			int mat_size = matrix_sizes[matrix_size_id];

			for (int matrix_type_id = 0; matrix_type_id < matrix_types_len; matrix_type_id++) {
				int matrix_type = matrix_types[matrix_type_id];

				// Skip types 3, 4 or 5 if the matrix size is too small
				if (matrix_type >= 3 && matrix_type <= 5 && mat_size <= matrix_type - 2) {
					continue;
				}

				// Set up parameters with DLATB4 and generate a test matrix with DLATMS
				char type;
				int kl, ku;
				double anorm;
				int mode;
				double cndnum;
				char dist;
				dlatb4_ (path, &matrix_type, &mat_size, &mat_size, &type, &kl, &ku, &anorm, &mode, &cndnum, &dist);

				int leading_dimension = mat_size >= 1 ? mat_size : 1;

				double*	rwork = (double*)malloc (max (mat_size * leading_dimension, 2 * max_right_hand_side_count) * sizeof (double));
				double* mat = (double*)malloc (leading_dimension * mat_size * sizeof(double));
				double* work = (double*)malloc (max (mat_size, 3) * leading_dimension * max_right_hand_side_count * sizeof (double));

				int info;

				dlatms_ (&mat_size, &mat_size, &dist, iseeds, "P", rwork, &mode, &cndnum, &anorm, &kl, &ku, "L", mat, &leading_dimension, work, &info);

				// Check error code from DLATMS
				if (info != 0) {
					alaerh_ (path, "DLATMS", &info, &zero, "L", &mat_size, &mat_size, &minus_one, &minus_one, &minus_one, &matrix_type, &tests_failed, &no_errors, &tests_run);
				}

				int* iwork = (int*)malloc (mat_size * sizeof (int));

				// Do for each translation type
				for (int translation_method_id = 0; translation_method_id < translation_methods_len; translation_method_id++) {
					translation_method_e translation_method = translation_methods[translation_method_id];

					// Do for each block size value
					for (int block_size_id = 0; block_size_id < block_sizes_len; block_size_id++) {
						int block_size = block_sizes[block_size_id];
						xlaenv_ (&one, &block_size);

						// Compute the L*L' or U'*U factorization of the matrix.
						double* factorized_mat = (double*)malloc (leading_dimension * mat_size * sizeof(double));
						dlacpy_ ("L", &mat_size, &mat_size, mat, &leading_dimension, factorized_mat, &leading_dimension);

						if (runtime_system == STARPU_RUNTIME_SYSTEM) {
							#if defined (USE_STARPU)
							starpu_block_cholesky (factorized_mat, mat_size, block_size, translation_method, 0);
							#endif
						} else if (runtime_system == OPENMP_RUNTIME_SYSTEM) {
							#if defined (USE_OPENMP)
							openmp_block_cholesky (factorized_mat, mat_size, block_size, translation_method);
							#endif
						}

						// Test 1: Reconstruct matrix from factors and compute residual
						double* inverse_mat = (double*)malloc (leading_dimension * mat_size * sizeof(double));
						dlacpy_ ("L", &mat_size, &mat_size, factorized_mat, &leading_dimension, inverse_mat, &leading_dimension);

						double result[NO_TESTS];
						dpot01_ ("L", &mat_size, mat, &leading_dimension, inverse_mat, &leading_dimension, rwork, &result[0]);

						// Test 2: Form the inverse and compute the residual.
						dlacpy_ ("L", &mat_size, &mat_size, factorized_mat, &leading_dimension, inverse_mat, &leading_dimension);
						dpotri_ ("L", &mat_size, inverse_mat, &leading_dimension, &info);

						// Check error code from DPOTRI
						if (info != 0) {
							alaerh_ (path, "DPOTRI", &info, &zero, "L", &mat_size, &mat_size, &minus_one, &minus_one, &minus_one, &matrix_type, &tests_failed, &no_errors, &six);
						}

						double cond_number_reciprocal;
						dpot03_ ("L", &mat_size, mat, &leading_dimension, inverse_mat, &leading_dimension, work, &leading_dimension, rwork, &cond_number_reciprocal, &result[1]);

						// Print information about the tests that did not pass the threshold.

						for (int test_id = 0; test_id <= 1; test_id++) {
							if (result[test_id] >= test_threshold) {
								if (tests_failed == 0 && no_errors == 0) {
									alahd_ (&six, path);
								}
								printf (" UPLO = '%c', N = %d, NB = %d, translation method %d, type %d, test %d, ratio = %lg\n", 'L', mat_size, block_size, translation_method, matrix_type, test_id + 1, result[test_id]);
								tests_failed++;
							}
						}
						tests_run += 2;

						// Skip the rest of the tests unless this is the first block size
						if (block_size_id > 0) {
							continue;
						}

						// Do for each right-hand side count
						for (int right_hand_size_count_id = 0; right_hand_size_count_id < right_hand_side_counts_len; right_hand_size_count_id++) {
							int right_hand_side_count = right_hand_side_counts[right_hand_size_count_id];	

							// Test 3: Solve and compute residual for A * X = B
							double* right_hand_sides = (double*)malloc (leading_dimension * right_hand_side_count * sizeof (double));
							double* exact_solution = (double*)malloc (leading_dimension * right_hand_side_count * sizeof (double));
							dlarhs_ (path, "N", "L", " ", &mat_size, &mat_size, &kl, &ku, &right_hand_side_count, mat, &leading_dimension, exact_solution, &leading_dimension, right_hand_sides, &leading_dimension, iseeds, &info);

							double* X = (double*)malloc (leading_dimension * right_hand_side_count * sizeof (double));
							dlacpy_ ("Full", &mat_size, &right_hand_side_count, right_hand_sides, &leading_dimension, X, &leading_dimension);

							dpotrs_ ("L", &mat_size, &right_hand_side_count, factorized_mat, &leading_dimension, X, &leading_dimension, &info);

							// Check error code from DPOTRS
							if (info != 0) {
								alaerh_ (path, "DPOTRS", &info, &zero, "L", &mat_size, &mat_size, &minus_one, &minus_one, &right_hand_side_count, &matrix_type, &tests_failed, &no_errors, &six);
							}

							dlacpy_ ("Full", &mat_size, &right_hand_side_count, right_hand_sides, &leading_dimension, work, &leading_dimension);
							dpot02_ ("L", &mat_size, &right_hand_side_count, mat, &leading_dimension, X, &leading_dimension, work, &leading_dimension, rwork, &result[2]);

							// Test 4: Check solution from generated exact solution
							dget04_ (&mat_size, &right_hand_side_count, X, &leading_dimension, exact_solution, &leading_dimension, &cond_number_reciprocal, &result[3]);

							// Tests 5, 6 and 7: Use iterative refinement to improve the solution
							dporfs_ ("L", &mat_size, &right_hand_side_count, mat, &leading_dimension, factorized_mat, &leading_dimension, right_hand_sides, &leading_dimension, X, &leading_dimension, rwork, &rwork[right_hand_side_count], work, iwork, &info);

							// Check error code from DPORFS
							if (info != 0) {
								alaerh_ (path, "DPORFS", &info, &zero, "L", &mat_size, &mat_size, &minus_one, &minus_one, &right_hand_side_count, &matrix_type, &tests_failed, &no_errors, &six);
							}

							dget04_ (&mat_size, &right_hand_side_count, X, &leading_dimension, exact_solution, &leading_dimension, &cond_number_reciprocal, &result[4]);
							dpot05_ ("L", &mat_size, &right_hand_side_count, mat, &leading_dimension, right_hand_sides, &leading_dimension, X, &leading_dimension, exact_solution, &leading_dimension, rwork, &rwork[right_hand_side_count], &result[5]);

							// Print information about the tests that did not pass the threshold
							for (int test_id = 2; test_id <= 5; test_id++) {
								if (result[test_id] >= test_threshold) {
									if (tests_failed == 0 && no_errors == 0) {
										alahd_ (&six, path);
									}
									printf (" UPLO = '%c', N = %d, NRHS = %d, translation method %d, type %d, test (%d) = %lg\n", 'L', mat_size, right_hand_side_count, translation_method, matrix_type, test_id + 1, result[test_id]);
									tests_failed++;
								}
							}
							tests_run += 5;

							free (right_hand_sides);
							free (exact_solution);
							free (X);
						}

						// Test 8: Get an estimate of RCOND = 1/CNDNUM
						double anorm = dlansy_ ("1", "L", &mat_size, mat, &leading_dimension, rwork);
						double cond_number_reciprocal_estimate;
						dpocon_ ("L", &mat_size, factorized_mat, &leading_dimension, &anorm, &cond_number_reciprocal_estimate, work, iwork, &info);

						// Check error code from DPOCON
						if (info != 0) {
							alaerh_ (path, "DPOCON", &info, &zero, "L", &mat_size, &mat_size, &minus_one, &minus_one, &minus_one, &matrix_type, &tests_failed, &no_errors, &six);
						}

						result[7] = dget06_ (&cond_number_reciprocal_estimate, &cond_number_reciprocal);

						// Print the test ratio if it is greater than or equal to test_threshold
						if (result[7] >= test_threshold) {
							if (tests_failed == 0 && no_errors == 0) {
								alahd_ (&six, path);
							}
							printf (" UPLO = '%c', N = %d, translation method %d, type %d, test (%d) = %lg\n", 'L', mat_size, translation_method, matrix_type, 8, result[7]);
							tests_failed++;
						}
						tests_run += 1;

						free (factorized_mat);
						free (inverse_mat);
					}
				}

				free (rwork);
				free (mat);
				free (work);
				free (iwork);
			}
		}
	}

	// Print a summary of the results
	alasum_ (path, &six, &tests_failed, &tests_run, &no_errors);

	// Shutdown StarPU
	#if defined (USE_STARPU)
	starpu_shutdown ();
	#endif
	return 0;
}
