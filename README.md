# Task-based implementation of Cholesky decomposition

This is a repository for the Bachelor's thesis **Task-based implementation of Cholesky decomposition** ([ProjectsFIT](https://projects.fit.cvut.cz/theses/4516)).

- Student: **Vít Břichňáč** ([brichvit@fit.cvut.cz](mailto:brichvit@fit.cvut.cz))
- Supervisor: **Ing. Jakub Šístek, Ph.D.** ([jakub.sistek@fit.cvut.cz](mailto:jakub.sistek@fit.cvut.cz))
- Reviewer: **Ing. Karel Klouda, Ph.D.** ([karel.klouda@fit.cvut.cz](mailto:karel.klouda@fit.cvut.cz))

This repository contains the implementation associated with the thesis. After building, the following artifacts are produced:

- A dynamic library `libcholesky.so`
- A static library `libcholesky.a` (optional, only when the `--build-static` option is specified when running the configure script)
- An include file `cholesky.h`
- A benchmarking utility `cholesky`
- Test programs `cholesky_test`, `blas_test` and `potf2_test`

In the `thesis` directory, the source code for the thesis text in the LaTeX format is available.

## Requirements

The following programs are required to build the artifacts:

- `gcc` version >= 10.2.0
- `gfortran` version >= 10.2.0
- `make` version >= 4.2.1
- `ar` version >= 2.34 (only is the static library build is enabled)

Optionally, the program can also work with the following libraries:

- StarPU version >= 1.3.10
- Intel OneApi MKL version >= 2022.2.1
- AOCL (BLIS version >= 3.0.1 and libFLAME version >= 5.2.0)
- OpenBLAS version >= 0.3.12
- Arm Performance Libraries version >= 23.04

### Building the artifacts

```
./configure
make
```

The configuration script allows the user to select a numerical library to compare the custom implementation against using the
`--numerical-lib` option with one of the following arguments:
- `MKL` for Intel OneAPI MKL
- `AOCL` for AMD Optimizing CPU Libraries (BLIS + libFLAME)
- `OpenBLAS` for OpenBLAS
- `ArmPL` for Arm Performance Libraries

Upon selection, the script checks for the required include files and library files. The implementations of the four driver routines (`DGEMM`, `DSYRK`, `DTRSM` and `DPOTRF`/`DPOTF2`) in the numerical library are then used in the custom implementations. **Important**: The library files have to be in one of the directories specified by the `LIBRARY_PATH` and `LD_LIBRARY_PATH` environment variables and the include files have to be in one of the directories specified by the `C_INCLUDE_PATH` environment variable.

The installation directory may be specified using the `--prefix` option (the default value is `/usr/local`). The specified directory does not yet have to exist when invoking the configuration script, but the parent directory has to exist and be a valid directory.

The runtime system/s used for the custom implementations are specified with the `--use-openmp` and `--use-starpu` options. Both may be used at the same time, but at least one of them has to be specified.

## Running pre-installation tests
```
make check
```

## Installing the artifacts
```
make install
```

The artifacts will be installed into the `bin`, `lib` and `include` subdirectories of the prefix directory specified during configuration.

## Uninstalling the artifacts
```
make uninstall
```

## Removing the build artifacts
```
make clean
```

## Removing the build artifacts and the files produced during configuration
```
make distclean
```

## Usage of the benchmarking utility
When invoking the benchmarking utility (`cholesky_benchmark`), the following options may be specified:

- Matrix size: `-m`, `--matrix-size` (requires a positive integer or start:edn:step range argument)
- Block size: `-b`, `--block-size` (requires a positive integer or start:edn:step range argument)
- Number of threads used: `-t`, `--no-threads` (requires a positive integer argument)
- The random seed for matrix generation: `-s`, `--seed` (requires a nonnegative integer argument)
- Number of iterations for each matrix size and block size: `-i`, `--iterations` (requires a positive integer argument)
- Additional correctness testing: `-C`, `--check-correctness`
- Running the custom implementations only: `-c`, `--custom-only`
- Selection of the translation method: `-T`, `--translation-method` (requires an argument with one of the following values: `no`, `sequential`, `parallel`)
- Ensuring correct trace generation using the FxT library and StarPU: `-f`, `--fxt-trace`

The `--matrix-size` and `--block-size` parameter values may be specified in the form of
start:end:step ranges. If so, the benchmarking utility iterates through every matrix size and
every block size specified in the ranges. More information about the benchmarking utility can be found in the thesis.

## Library usage

The available library functions are `openmp_block_cholesky` and `starpu_block_cholesky`.

### Function `openmp_block_cholesky`

Usage:
```
openmp_block_cholesky (double* orig, int size, int block_size, translation_method_e translation_method);
```

Computes the Cholesky factorization of an N x N symmetric positive definite matrix A in a column major full storage format.
The computation is done in place, so the original matrix is overwritten with L such that A = L * L^T.
The entries above the main diagonal are not accessed.
 
This function utilizes the OpenMP runtime system, the DGEMM, DSYRK and DTRSM Level 3 BLAS functions,
and the DPOTF2 LAPACK routine to perform the computation.

If a numerical library was specified during configuration, its implementations of the driver routines are used.
If no numerical library was specified, the builtin driver routines are used.

Parameters:

- orig: The symmetric positive definite matrix to be factorized
- size: The width and height of the factorized matrix
- block_size: The main block size of the factorized matrix
- translation_method: The translation method used for the computation

### Function `starpu_block_cholesky`

Usage:
```
starpu_block_cholesky (double* orig, int size, int block_size, translation_method_e translation_method, int shutdown_starpu);
```

Computes the Cholesky factorization of an N x N symmetric positive definite matrix A in a column major full storage format.
The computation is done in place, so the original matrix is overwritten with L such that A = L * L^T.
The entries above the main diagonal are not accessed.
 
This function utilizes the StarPU runtime system, the DGEMM, DSYRK and DTRSM Level 3 BLAS functions,
and the DPOTF2 LAPACK routine to perform the computation.

If a numerical library was specified during configuration, its implementations of the driver routines are used.
If no numerical library was specified, the builtin driver routines are used.

StarPU may already be initialized when the function is run. If not, the function handles the StarPU initialization itself.

Parameters:

- orig: The symmetric positive definite matrix to be factorized
- size: The width and height of the factorized matrix
- block_size: The main block size of the factorized matrix
- translation_method: The translation method used for the computation
- shutdown_starpu: A value indicating whether StarPU should be shut down at the end of the computation (non-zero values for shutdown)

The function returns a zero value if the computation was finished successfully and a non-zero value if StarPU could not be initialized.