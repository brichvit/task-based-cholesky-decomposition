include $(TOPDIR)/make_config.inc

CHOLESKY_SRC	= $(TOPDIR)/src/cholesky
GEMM_SRC		= $(TOPDIR)/src/gemm
SYRK_SRC		= $(TOPDIR)/src/syrk
TRSM_SRC		= $(TOPDIR)/src/trsm
POTF2_SRC		= $(TOPDIR)/src/potf2
COMMON_SRC		= $(TOPDIR)/src/common

BENCHMARK_SRC	= $(TOPDIR)/src/benchmark

LAPACK			= $(TOPDIR)/lapack

BLAS_TEST		= $(TOPDIR)/test/blas
POTF2_TEST		= $(TOPDIR)/test/potf2
CHOLESKY_TEST	= $(TOPDIR)/test/cholesky