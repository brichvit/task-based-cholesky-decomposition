#include "potf2.h"
#include "../common/math_util.h"
#include "../common/value_checking.h"

#include <math.h>

#define A_ELEM(row,col) A[col * lda + row]

int dpotf2 (char uplo, int N, double* A, int lda) {

	// Test the argument values
	if (!VALID_UPLO_VALUE(uplo)) {
		return -1;
	} else if (N < 0) {
		return -2;
	} else if (lda < max (1, N)) {
		return -4;
	}

	if (IS_UPPER(uplo)) {
		for (int j = 0; j < N; j++) {
			double dot_prod = 0;
			for (int k = 0; k < j; k++) {
				dot_prod += A_ELEM(k, j) * A_ELEM(k, j);
			}
			double Ajj = A_ELEM(j, j) - dot_prod;

			// Return with an error if we are to take a square root of a negative number or a NaN
			// (if so, the input matrix was not symmetric positive definite)
			if (Ajj <= 0 || IS_NAN(Ajj)) {
				return j + 1;
			}

			Ajj = sqrt (Ajj);
			A_ELEM(j, j) = Ajj;

			for (int i = j + 1; i < N; i++) {
				double sum = 0;
				for (int k = 0; k < j; k++) {
					sum += A_ELEM(k, i) * A_ELEM(k, j);
				}
				A_ELEM(j, i) = (A_ELEM(j, i) - sum) / Ajj;
			}
		}
	} else {
		for (int j = 0; j < N; j++) {
			double dot_prod = 0;
			for (int k = 0; k < j; k++) {
				dot_prod += A_ELEM(j, k) * A_ELEM(j, k);
			}
			double Ajj = A_ELEM(j, j) - dot_prod;

			// Return with an error if we are to take a square root of a negative number or a NaN
			// (if so, the input matrix was not symmetric positive definite)
			if (Ajj <= 0 || IS_NAN(Ajj)) {
				return j + 1;
			}

			Ajj = sqrt (Ajj);
			A_ELEM(j, j) = Ajj;

			for (int i = j + 1; i < N; i++) {
				double sum = 0;
				for (int k = 0; k < j; k++) {
					sum += A_ELEM(i, k) * A_ELEM(j, k);
				}
				A_ELEM(i, j) = (A_ELEM(i, j) - sum) / Ajj;
			}
		}
	}

	return 0;
}
