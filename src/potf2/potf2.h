#ifndef POTF2_H_
#define POTF2_H_

/**
 * @brief Computes the Cholesky factorization of an N x N symmetric positive definite matrix A.
 * The computation is done in place, so the original matrix is overwritten with:
 * - L such that L * L^T = A if UPLO = 'L'
 * - U such that U^T * U = A if UPLO = 'U'
 * 
 * @param UPLO The computation being done (see above)
 * @param N The width and height of A
 * @param A The symmetric positive definite matrix A in a column major full storage format
 * @param LDA The leading dimension of A
 * @return int The error code (zero if the the function has finished correctly)
 */
int dpotf2 (char UPLO, int N, double* A, int LDA);

#endif /* POTF2_H */
