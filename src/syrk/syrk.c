#include "syrk.h"
#include "../common/math_util.h"
#include "../common/value_checking.h"

#include <memory.h>

// Convenience macros
#define A_ELEM(i,j) A[i + j * lda]
#define C_ELEM(i,j) C[i + j * ldc]

int dsyrk (char uplo, char trans, int N, int K,
		   double alpha, double* A, int lda,
		   double beta, double* C, int ldc) {

	// Test the argument values
	if (!VALID_UPLO_VALUE(uplo)) {
		return 1;
	} else if (!VALID_TRANS_VALUE(trans)) {
		return 2;
	} else if (N < 0) {
		return 3;
	} else if (K < 0) {
		return 4;
	} else if ((IS_NOT_TRANS(trans) && lda < max (1, N)) || (IS_TRANS(trans) && lda < max (1, K))) {
		return 7;
	} else if (ldc < max (1, N)) {
		return 10;
	}

	if (IS_UPPER(uplo)) {
		if (IS_NOT_TRANS(trans)) {
			for (int j = 0; j < N; j++) {
				for (int i = 0; i <= j; i++) {
					C_ELEM(i, j) *= beta;
					for (int k = 0; k < K; k++) {
						C_ELEM(i, j) += alpha * A_ELEM(i, k) * A_ELEM(j, k);
					}
				}
			}
		} else {
			for (int j = 0; j < N; j++) {
				for (int i = 0; i <= j; i++) {
					C_ELEM(i, j) *= beta;
					for (int k = 0; k < K; k++) {
						C_ELEM(i, j) += alpha * A_ELEM(k, i) * A_ELEM(k, j);
					}
				}
			}
		}
	} else {
		if (IS_NOT_TRANS(trans)) {
			// This is the only branch used in Cholesky decomposition, therefore it is the only optimised one
			for (int j = 0; j < N; j++) {
				for (int i = j; i < N; i++) {
					if (beta == 0) {
						C_ELEM(i, j) = 0;
					} else if (beta == -1) {
						C_ELEM(i, j) = -C_ELEM(i, j);
					} else if (beta != 1) {
						C_ELEM(i, j) *= beta;
					}
				}

				if (alpha == 0) {
					continue;
				}

				for (int k = 0; k < K; k++) {
					for (int i = j; i < N; i++) {
						if (alpha == 1) {
							C_ELEM(i, j) += A_ELEM(i, k) * A_ELEM(j, k);
						} else if (alpha == -1) {
							C_ELEM(i, j) -= A_ELEM(i, k) * A_ELEM(j, k);
						} else {
							C_ELEM(i, j) += alpha * A_ELEM(i, k) * A_ELEM(j, k);
						}
					}
				}
			}
		} else {
			for (int j = 0; j < N; j++) {
				for (int i = j; i < N; i++) {
					C_ELEM(i, j) *= beta;
					for (int k = 0; k < K; k++) {
						C_ELEM(i, j) += alpha * A_ELEM(k, i) * A_ELEM(k, j);
					}
				}
			}
		}
	}

	return 0;
}