#ifndef SYRK_H_
#define SYRK_H_

/**
 * @brief Performs the matrix-matrix computation
 * - C <- alpha * A * A^T + beta * C for an N x K matrix A in TRANSA='N'
 * - C <- alpha * A^T * A + beta * C for an K x N matrix A in TRANSA='T' or TRANSA='C'
 * 
 * In both cases, C is an N x N symmetric matrix.
 * 
 * @param UPLO The part of C that is accessed ('L' for lower, 'U' for upper)
 * @param TRANS Determines the computation (see above)
 * @param N The width and height of C
 * @param K One of the dimensions of A (see above)
 * @param ALPHA The parameter alpha used above
 * @param A The matrix A in a column major full storage format
 * @param LDA The leading dimension of A
 * @param BETA The parameter beta used above
 * @param C The symmetric matrix C in a column major full storage format
 * @param LDC The leading dimension of C
 * @return int The error code (zero if the the function has finished correctly)
 */
int dsyrk (char UPLO, char TRANS, int N, int K,
		   double ALPHA, double* A, int LDA,
		   double BETA, double* C, int LDC);

#endif /* SYRK_H_ */