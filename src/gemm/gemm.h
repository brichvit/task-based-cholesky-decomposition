#ifndef GEMM_H_
#define GEMM_H_

/**
 * @brief Performs the matrix-matrix computation
 *   C <- alpha * opA(A) * opB(B) + beta * C
 * 
 * A is an M x K matrix if TRANSA='N' and an K x M matrix otherwise.
 * B is an K x N matrix if TRANSB='N' and an N x K matrix otherwise.
 * C is an M x N matrix.
 * 
 * @param TRANSA Determines whether A should act as transposed ('N' for op(A)=A, 'T' or 'C' for op(A)=A^T)
 * @param TRANSB Determines whether B should act as transposed ('N' for opB(B)=B, 'T' or 'C' for opB(B)=B^T)
 * @param M The number of rows of C
 * @param N One of the dimensions of A and B (see above)
 * @param K The number of columns of C
 * @param ALPHA The parameter alpha used above
 * @param A The matrix A in a column major full storage format
 * @param LDA The leading dimension of A
 * @param B The matrix B in a column major full storage format
 * @param LDB The leading dimension of B
 * @param BETA The parameter beta used above
 * @param C The matrix C in a column major full storage format
 * @param LDC The leading dimension of C
 * @return int The error code (zero if the the function has finished correctly)
 */
int dgemm (char TRANSA, char TRANSB,
		   int M, int N, int K,
		   double ALPHA, double* A, int LDA,
		   double* B, int LDB,
		   double BETA, double* C, int LDC);

#endif /* GEMM_H_ */
