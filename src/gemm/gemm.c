#include "gemm.h"
#include "../common/math_util.h"
#include "../common/value_checking.h"

// Convenience macros
#define A_ELEM(i,j) A[i + j * lda]
#define B_ELEM(i,j) B[i + j * ldb]
#define C_ELEM(i,j) C[i + j * ldc]

int dgemm (char trans_A, char trans_B,
		   int M, int N, int K,
		   double alpha, double* A, int lda,
		   double* B, int ldb,
		   double beta, double* C, int ldc) {

	// Test the argument values
	if (!VALID_TRANS_VALUE(trans_A)) {
		return 1;
	} else if (!VALID_TRANS_VALUE(trans_B)) {
		return 2;
	} else if (M < 0) {
		return 3;
	} else if (N < 0) {
		return 4;
	} else if (K < 0) {
		return 5;
	} else if ((IS_NOT_TRANS(trans_A) && lda < max (1, M)) || (IS_TRANS(trans_A) && lda < max (1, K))) {
		return 8;
	} else if ((IS_NOT_TRANS(trans_B) && ldb < max (1, K)) || (IS_TRANS(trans_B) && ldb < max (1, N))) {
		return 10;
	} else if (ldc < max (1, M)) {
		return 13;
	}

	if (IS_NOT_TRANS(trans_A) && IS_NOT_TRANS(trans_B)) {
		for (int j = 0; j < N; j++) {
			for (int i = 0; i < M; i++) {
				C_ELEM(i, j) *= beta;
				for (int k = 0; k < K; k++) {
					C_ELEM(i, j) += alpha * A_ELEM(i, k) * B_ELEM(k, j);
				}
			}
		}
	} else if (IS_NOT_TRANS(trans_A)) {
		// This is the only branch used in Cholesky decomposition, therefore it is the only optimised one
		for (int j = 0; j < N; j++) {
			for (int i = 0; i < M; i++) {
				if (beta == 0) {
					C_ELEM(i, j) = 0;
				} else if (beta == -1) {
					C_ELEM(i, j) = -C_ELEM(i, j);
				} else if (beta != 1) {
					C_ELEM(i, j) *= beta;
				}
			}

			if (alpha == 0) {
				continue;
			}

			for (int k = 0; k < K; k++) {
				for (int i = 0; i < M; i++) {
					if (alpha == 1) {
						C_ELEM(i, j) += A_ELEM(i, k) * B_ELEM(j, k);
					} else if (alpha == -1) {
						C_ELEM(i, j) -= A_ELEM(i, k) * B_ELEM(j, k);
					} else {
						C_ELEM(i, j) += alpha * A_ELEM(i, k) * B_ELEM(j, k);
					}
				}
			}
		}
	} else if (IS_NOT_TRANS(trans_B)) {
		for (int j = 0; j < N; j++) {
			for (int i = 0; i < M; i++) {
				C_ELEM(i, j) *= beta;
				for (int k = 0; k < K; k++) {
					C_ELEM(i, j) += alpha * A_ELEM(k, i) * B_ELEM(k, j);
				}
			}
		}
	} else {
		for (int j = 0; j < N; j++) {
			for (int i = 0; i < M; i++) {
				C_ELEM(i, j) *= beta;
				for (int k = 0; k < K; k++) {
					C_ELEM(i, j) += alpha * A_ELEM(k, i) * B_ELEM(j, k);
				}
			}
		}
	}

	return 0;
}
