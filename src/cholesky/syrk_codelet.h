#ifndef SYRK_CODELET_H_
#define SYRK_CODELET_H_

#if defined (USE_STARPU)
#include <starpu.h>
#endif

#if defined (USE_OPENMP)
#include "block_data.h"
#endif

#if defined (USE_STARPU)
/**
 * @brief A StarPU codelet to perform the SYRK computation (see src/syrk/syrk.h) with the following parameter values:
 * - UPLO = 'L'
 * - TRANS = 'N'
 * - ALPHA = -1
 * - BETA = 1
 */
extern struct starpu_codelet syrk_codelet;
#endif

#if defined (USE_OPENMP)
/**
 * @brief Performs the SYRK computation (see src/syrk/syrk.h) with the following parameter values:
 * - UPLO = 'L'
 * - TRANS = 'N'
 * - ALPHA = -1
 * - BETA = 1
 */
void generic_syrk (block_data_t A_block_data, block_data_t C_block_data);
#endif

#endif /* SYRK_CODELET_H_ */
