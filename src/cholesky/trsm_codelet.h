#ifndef TRSM_CODELET_H_
#define TRSM_CODELET_H_

#if defined (USE_STARPU)
#include <starpu.h>
#endif

#if defined (USE_OPENMP)
#include "block_data.h"
#endif

#if defined (USE_STARPU)
/**
 * @brief A StarPU codelet to perform the TRSM computation (see src/trsm/trsm.h) with the following parameter values:
 * - SIDE = 'R'
 * - UPLO = 'L'
 * - TRANSA = 'T'
 * - DIAG = 'N'
 * - ALPHA = 1
 */
extern struct starpu_codelet trsm_codelet;
#endif

#if defined (USE_OPENMP)
/**
 * @brief Performs the TRSM computation (see src/trsm/trsm.h) with the following parameter values:
 * - SIDE = 'R'
 * - UPLO = 'L'
 * - TRANSA = 'T'
 * - DIAG = 'N'
 * - ALPHA = 1
 */
void generic_trsm (block_data_t A_block_data, block_data_t B_block_data);
#endif

#endif /* TRSM_CODELET_H_ */
