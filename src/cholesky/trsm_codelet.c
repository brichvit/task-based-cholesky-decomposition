#include "trsm_codelet.h"

#include "starpu_matrix_util.h"

#if defined (USE_MKL)
#include <mkl.h>
#elif defined (USE_AOCL) || defined (USE_OPENBLAS)
#include <cblas.h>
#elif defined (USE_ARMPL)
#include <armpl.h>
#else
#include "../trsm/trsm.h"
#endif

#if defined (USE_STARPU)
void trsm_func (void* buffers[], __attribute__ ((unused)) void* arg) {
	int M = STARPU_COL_MATRIX_GET_NY (buffers[1]);
	int N = STARPU_COL_MATRIX_GET_NX (buffers[1]);

	int LDA = STARPU_COL_MATRIX_GET_LD (buffers[0]);
	int LDB = STARPU_COL_MATRIX_GET_LD (buffers[1]);

	double* A = (double*)STARPU_MATRIX_GET_PTR (buffers[0]);
	double* B = (double*)STARPU_MATRIX_GET_PTR (buffers[1]);

	#if defined (USE_MKL) || defined (USE_AOCL) || defined (USE_OPENBLAS) || defined (USE_ARMPL)
	cblas_dtrsm (CblasColMajor, CblasRight, CblasLower, CblasTrans, CblasNonUnit, M, N, 1, A, LDA, B, LDB);
	#else
	dtrsm ('R', 'L', 'T', 'N', M, N, 1, A, LDA, B, LDB);
	#endif
}

struct starpu_codelet trsm_codelet = {
    .cpu_funcs = { trsm_func },
    .cpu_funcs_name = { "trsm" },
    .nbuffers = 2,
    .modes = { STARPU_R, STARPU_RW },
	.name = "TRSM"
};
#endif

#if defined (USE_OPENMP)
void generic_trsm (block_data_t A_block_data, block_data_t B_block_data) {
	int M = B_block_data.height;
	int N = B_block_data.width;

	int LDA = A_block_data.leading_dimension;
	int LDB = B_block_data.leading_dimension;

	double* A = A_block_data.ptr;
	double* B = B_block_data.ptr;

	#if defined (USE_MKL) || defined (USE_AOCL) || defined (USE_OPENBLAS) || defined (USE_ARMPL)
	cblas_dtrsm (CblasColMajor, CblasRight, CblasLower, CblasTrans, CblasNonUnit, M, N, 1, A, LDA, B, LDB);
	#else
	dtrsm ('R', 'L', 'T', 'N', M, N, 1, A, LDA, B, LDB);
	#endif
}
#endif
