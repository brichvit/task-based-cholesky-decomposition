#include "syrk_codelet.h"

#include "starpu_matrix_util.h"

#if defined (USE_MKL)
#include <mkl.h>
#elif defined (USE_AOCL) || defined (USE_OPENBLAS)
#include <cblas.h>
#elif defined (USE_ARMPL)
#include <armpl.h>
#else
#include "../syrk/syrk.h"
#endif

#if defined (USE_STARPU)
void syrk_func (void* buffers[], __attribute__ ((unused)) void* arg) {
	int N = STARPU_COL_MATRIX_GET_NX (buffers[1]);
	int K = STARPU_COL_MATRIX_GET_NX (buffers[0]);

	int LDA = STARPU_COL_MATRIX_GET_LD (buffers[0]);
	int LDC = STARPU_COL_MATRIX_GET_LD (buffers[1]);

	double* A = (double*)STARPU_MATRIX_GET_PTR (buffers[0]);
	double* C = (double*)STARPU_MATRIX_GET_PTR (buffers[1]);

	#if defined (USE_MKL) || defined (USE_AOCL) || defined (USE_OPENBLAS) || defined (USE_ARMPL)
	cblas_dsyrk (CblasColMajor, CblasLower, CblasNoTrans, N, K, -1, A, LDA, 1, C, LDC);
	#else
	dsyrk ('L', 'N', N, K, -1, A, LDA, 1, C, LDC);
	#endif
}

struct starpu_codelet syrk_codelet = {
    .cpu_funcs = { syrk_func },
    .cpu_funcs_name = { "syrk" },
    .nbuffers = 2,
    .modes = { STARPU_R, STARPU_RW },
	.name = "SYRK"
};
#endif

#if defined (USE_OPENMP)
void generic_syrk (block_data_t A_block_data, block_data_t C_block_data) {
	int N = C_block_data.width;
	int K = A_block_data.width;

	int LDA = A_block_data.leading_dimension;
	int LDC = C_block_data.leading_dimension;

	double* A = A_block_data.ptr;
	double* C = C_block_data.ptr;

	#if defined (USE_MKL) || defined (USE_AOCL) || defined (USE_OPENBLAS) || defined (USE_ARMPL)
	cblas_dsyrk (CblasColMajor, CblasLower, CblasNoTrans, N, K, -1, A, LDA, 1, C, LDC);
	#else
	dsyrk ('L', 'N', N, K, -1, A, LDA, 1, C, LDC);
	#endif
}
#endif
