#include "potf2_codelet.h"

#include "starpu_matrix_util.h"

#if defined (USE_MKL)
#include <mkl_lapacke.h>
#elif defined (USE_AOCL)
#include <FLAME.h>
#elif defined (USE_OPENBLAS)
#include <lapacke.h>
#elif defined (USE_ARMPL)
#include <armpl.h>
#else
#include "../potf2/potf2.h"
#endif

#if defined (USE_STARPU)
void potf2_func (void* buffers[], __attribute__ ((unused)) void* arg) {
	int N = STARPU_COL_MATRIX_GET_NX (buffers[0]);

	int LDA = STARPU_COL_MATRIX_GET_LD (buffers[0]);

	double* A = (double*)STARPU_MATRIX_GET_PTR (buffers[0]);

	#if defined (USE_MKL) || defined (USE_OPENBLAS) || defined (USE_ARMPL)
	LAPACKE_dpotrf2 (LAPACK_COL_MAJOR, 'L', N, A, LDA);
	#elif defined (USE_AOCL)
	int info;
	dpotrf_ ("L", &N, A, &LDA, &info);
	#else
	dpotf2 ('L', N, A, LDA);
	#endif
}

struct starpu_codelet potf2_codelet = {
    .cpu_funcs = { potf2_func },
    .cpu_funcs_name = { "potf2" },
    .nbuffers = 1,
    .modes = { STARPU_RW },
	.name = "POTF2"
};
#endif

#if defined (USE_OPENMP)
void generic_potf2 (block_data_t A_block_data) {
	int N = A_block_data.width;

	int LDA = A_block_data.leading_dimension;

	double* A = A_block_data.ptr;

	#if defined (USE_MKL) || defined (USE_OPENBLAS) || defined (USE_ARMPL)
	LAPACKE_dpotrf2 (LAPACK_COL_MAJOR, 'L', N, A, LDA);
	#elif defined (USE_AOCL)
	int info;
	dpotrf_ ("L", &N, A, &LDA, &info);
	#else
	dpotf2 ('L', N, A, LDA);
	#endif
}
#endif
