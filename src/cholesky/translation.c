#include "../common/math_util.h"

#include "starpu_matrix_util.h"
#include "translation.h"

#include <stdlib.h>
#include <memory.h>

#if defined (USE_STARPU)
#include <starpu.h>
#endif

#if defined (USE_STARPU)
void translate_block_columnwise_to_blockwise_func (void* buffers[], __attribute__ ((unused)) void* arg) {
	void* columnwise_buffer = buffers[0];
	void* blockwise_buffer = buffers[1];

	double* columnwise_ptr = (double*)STARPU_MATRIX_GET_PTR (columnwise_buffer);
	double* blockwise_ptr = (double*)STARPU_MATRIX_GET_PTR (blockwise_buffer);

	int blockwise_width = STARPU_COL_MATRIX_GET_NX (blockwise_buffer);
	int blockwise_height = STARPU_COL_MATRIX_GET_NY (blockwise_buffer);

	int columnwise_leading_dimension = STARPU_COL_MATRIX_GET_LD (columnwise_buffer);

	for (int blockwise_col = 0; blockwise_col < blockwise_width; blockwise_col++) {
		memcpy (blockwise_ptr + blockwise_col * blockwise_height,
				columnwise_ptr + blockwise_col * columnwise_leading_dimension,
				blockwise_height * sizeof (double));
	}
}

struct starpu_codelet translate_block_columnwise_to_blockwise_cl = {
	.cpu_funcs = {&translate_block_columnwise_to_blockwise_func},
	.cpu_funcs_name = {"translate_block_columnwise_to_blockwise_func"},
	.nbuffers = 2,
	.modes = {STARPU_R, STARPU_W},
	.name = "COL2BLOCK"
};


void translate_block_blockwise_to_columwise_func (void* buffers[], __attribute__ ((unused)) void* arg) {
	void* blockwise_buffer = buffers[0];
	void* columnwise_buffer = buffers[1];

	double* columnwise_ptr = (double*)STARPU_MATRIX_GET_PTR (columnwise_buffer);
	double* blockwise_ptr = (double*)STARPU_MATRIX_GET_PTR (blockwise_buffer);

	int blockwise_width = STARPU_COL_MATRIX_GET_NX (blockwise_buffer);
	int blockwise_height = STARPU_COL_MATRIX_GET_NY (blockwise_buffer);

	int columnwise_leading_dimension = STARPU_COL_MATRIX_GET_LD (columnwise_buffer);

	for (int blockwise_col = 0; blockwise_col < blockwise_width; blockwise_col++) {
		memcpy (columnwise_ptr + blockwise_col * columnwise_leading_dimension,
				blockwise_ptr + blockwise_col * blockwise_height,
				blockwise_height * sizeof (double));
	}
}

struct starpu_codelet translate_block_blockwise_to_columwise_cl = {
	.cpu_funcs = {&translate_block_blockwise_to_columwise_func},
	.cpu_funcs_name = {"translate_block_blockwise_to_columwise_func"},
	.nbuffers = 2,
	.modes = {STARPU_R, STARPU_W},
	.name = "BLOCK2COL"
};

void starpu_translate_columnwise_to_blockwise_parallel (starpu_data_handle_t* columnwise_handles,
												 starpu_data_handle_t* blockwise_handles,
												 int block_mat_size) {
	for (int block_column = 0; block_column < block_mat_size; block_column++) {
		for (int block_row = block_column; block_row < block_mat_size; block_row++) {
			starpu_task_insert (&translate_block_columnwise_to_blockwise_cl,
								STARPU_R, columnwise_handles[block_column * block_mat_size + block_row],
								STARPU_W, blockwise_handles[block_column * block_mat_size + block_row],
								0);
		}
	}
}

void starpu_translate_blockwise_to_columnwise_parallel (starpu_data_handle_t* blockwise_handles,
												 starpu_data_handle_t* columnwise_handles,
												 int block_mat_size) {
	for (int block_column = 0; block_column < block_mat_size; block_column++) {
		for (int block_row = block_column; block_row < block_mat_size; block_row++) {
			starpu_task_insert (&translate_block_blockwise_to_columwise_cl,
								STARPU_R, blockwise_handles[block_column * block_mat_size + block_row],
								STARPU_W, columnwise_handles[block_column * block_mat_size + block_row],
								0);
		}
	}
}
#endif

#if defined (USE_OPENMP)
void openmp_translate_block_columnwise_to_blockwise (block_data_t columnwise_block_data, block_data_t blockwise_block_data) {
	double* columnwise_ptr = columnwise_block_data.ptr;
	double* blockwise_ptr = blockwise_block_data.ptr;

	int blockwise_width = blockwise_block_data.width;
	int blockwise_height = blockwise_block_data.height;

	int columnwise_leading_dimension = columnwise_block_data.leading_dimension;

	for (int blockwise_col = 0; blockwise_col < blockwise_width; blockwise_col++) {
		memcpy (blockwise_ptr + blockwise_col * blockwise_height,
				columnwise_ptr + blockwise_col * columnwise_leading_dimension,
				blockwise_height * sizeof (double));
	}
}

void openmp_translate_block_blockwise_to_columnwise (block_data_t blockwise_block_data, block_data_t columnwise_block_data) {
	double* columnwise_ptr = columnwise_block_data.ptr;
	double* blockwise_ptr = blockwise_block_data.ptr;

	int blockwise_width = blockwise_block_data.width;
	int blockwise_height = blockwise_block_data.height;

	int columnwise_leading_dimension = columnwise_block_data.leading_dimension;

	for (int blockwise_col = 0; blockwise_col < blockwise_width; blockwise_col++) {
		memcpy (columnwise_ptr + blockwise_col * columnwise_leading_dimension,
				blockwise_ptr + blockwise_col * blockwise_height,
				blockwise_height * sizeof (double));
	}
}

void openmp_translate_columnwise_to_blockwise_parallel (block_data_t* columnwise_block_data_mat,
														block_data_t* blockwise_block_data_mat,
														int block_mat_size) {
	for (int block_column = 0; block_column < block_mat_size; block_column++) {
		for (int block_row = block_column; block_row < block_mat_size; block_row++) {
			#pragma omp task depend (in: columnwise_block_data_mat[block_column * block_mat_size + block_row]) \
				depend (out: blockwise_block_data_mat[block_column * block_mat_size + block_row])	
			{
				openmp_translate_block_columnwise_to_blockwise (columnwise_block_data_mat[block_column * block_mat_size + block_row],
																blockwise_block_data_mat[block_column * block_mat_size + block_row]);
			}
		}
	}
}

void openmp_translate_blockwise_to_columnwise_parallel (block_data_t* blockwise_block_data_mat,
														block_data_t* columnwise_block_data_mat,
														int block_mat_size) {
	for (int block_column = 0; block_column < block_mat_size; block_column++) {
		for (int block_row = block_column; block_row < block_mat_size; block_row++) {
			#pragma omp task depend (in: blockwise_block_data_mat[block_column * block_mat_size + block_row]) \
				depend (out: columnwise_block_data_mat[block_column * block_mat_size + block_row])	
			{
				openmp_translate_block_blockwise_to_columnwise (blockwise_block_data_mat[block_column * block_mat_size + block_row],
																columnwise_block_data_mat[block_column * block_mat_size + block_row]);
			}
		}
	}
}
#endif

void translate_columnwise_to_blockwise_sequential (double* mat, int size, int block_size, int leading_dimension) {
	double* curr_row_of_blocks = (double*)malloc (size * block_size * sizeof (double));

	for (int start_col = 0; start_col < size; start_col += block_size) {
		int curr_block_width = min (size - start_col, block_size);

		double* curr_row_of_blocks_ptr = curr_row_of_blocks;

		// Copy the current block row
		for (int start_row = 0; start_row < size; start_row += block_size) {
			int curr_block_height = min (size - start_row, block_size);

			for (int block_col = 0; block_col < curr_block_width; block_col++) {
				memcpy (curr_row_of_blocks_ptr,
						mat + (start_col + block_col) * leading_dimension + start_row,
						curr_block_height * sizeof (double));
				curr_row_of_blocks_ptr += curr_block_height;
			}
		}

		curr_row_of_blocks_ptr = curr_row_of_blocks;
		for (int block_col = 0; block_col < curr_block_width; block_col++) {
			memcpy (mat + (start_col + block_col) * leading_dimension, curr_row_of_blocks_ptr, size * sizeof (double));
			curr_row_of_blocks_ptr += size;
		}
	}

	free (curr_row_of_blocks);
}

void translate_blockwise_to_columnwise_sequential (double* mat, int size, int block_size, int leading_dimension) {
	double* curr_row_of_blocks = (double*)malloc (size * block_size * sizeof (double));

	for (int start_col = 0; start_col < size; start_col += block_size) {
		int curr_block_width = min (size - start_col, block_size);

		double* curr_row_of_blocks_ptr = curr_row_of_blocks;

		// Copy the current block row
		for (int block_col = 0; block_col < curr_block_width; block_col++) {
			memcpy (curr_row_of_blocks_ptr, mat + (start_col + block_col) * leading_dimension, size * sizeof (double));
			curr_row_of_blocks_ptr += size;
		}

		curr_row_of_blocks_ptr = curr_row_of_blocks;
		for (int start_row = 0; start_row < size; start_row += block_size) {
			int curr_block_height = min (size - start_row, block_size);

			for (int block_col = 0; block_col < curr_block_width; block_col++) {
				memcpy (mat + (start_col + block_col) * leading_dimension + start_row,
						curr_row_of_blocks_ptr,
						curr_block_height * sizeof (double));
				curr_row_of_blocks_ptr += curr_block_height;
			}
		}
	}

	free (curr_row_of_blocks);
}
