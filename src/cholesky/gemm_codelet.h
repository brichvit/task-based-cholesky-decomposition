#ifndef GEMM_CODELET_H_
#define GEMM_CODELET_H_

#if defined (USE_STARPU)
#include <starpu.h>
#endif

#if defined (USE_OPENMP)
#include "block_data.h"
#endif

#if defined (USE_STARPU)
/**
 * @brief A codelet for StarPU to perform the GEMM computation (see src/gemm/gemm.h) with the following parameter values:
 * - TRANSA = 'N'
 * - TRANSB = 'T'
 * - ALPHA = -1
 * - BETA = 1
 */
extern struct starpu_codelet gemm_codelet;
#endif

/**
 * @brief Performs the GEMM computation (see src/gemm/gemm.h) with the following parameter values:
 * - TRANSA = 'N'
 * - TRANSB = 'T'
 * - ALPHA = -1
 * - BETA = 1
 */
#if defined (USE_OPENMP)
void generic_gemm (block_data_t A_block_data, block_data_t B_block_data, block_data_t C_block_data);
#endif

#endif /* GEMM_CODELET_H_ */
