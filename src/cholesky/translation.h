#ifndef TRANSLATION_H_
#define TRANSLATION_H_

#include "translation_methods.h"

#if defined (USE_STARPU)
#include <starpu.h>
#endif

#if defined (USE_OPENMP)
#include "block_data.h"
#endif

/**
 * @brief Translates a lower triangular square matrix in a column major full storage format into a column major block storage format.
 * The translation is done in place, so the original full storage matrix is overwritten.
 * The translation is sequential (the function uses only the thread that it was called from).
 * 
 * @param mat The translated matrix 
 * @param size The width and height of the translated matrix
 * @param block_size The size of the main blocks of the translated matrix
 * @param leading_dimension The leading size of the translated matrix
 */
void translate_columnwise_to_blockwise_sequential (double* mat, int size, int block_size, int leading_dimension);

/**
 * @brief Translates a lower triangular square matrix in a column major block storage format into a column major full storage format.
 * The translation is done in place, so the original block storage matrix is overwritten.
 * The translation is sequential (the function uses only the thread that it was called from).
 * 
 * @param mat The translated matrix 
 * @param size The width and height of the translated matrix
 * @param block_size The size of the main blocks of the translated matrix
 * @param leading_dimension The leading size of the translated matrix
 */
void translate_blockwise_to_columnwise_sequential (double* mat, int size, int block_size, int leading_dimension);

#if defined (USE_STARPU)
/**
 * @brief Translates a lower triangular square matrix in a column major full storage format into a column major block storage format.
 * This function uses the StarPU runtime system to perform the translation in parallel.
 * 
 * @param columnwise_handles Column major full storage matrix of handles representing the blocks in the source matrix
 * @param blockwise_handles Column major full storage matrix of handles representing the blocks in the destination matrix
 * @param block_mat_size The width and height of both of the handle matrices
 */
void starpu_translate_columnwise_to_blockwise_parallel (starpu_data_handle_t* columnwise_handles, starpu_data_handle_t* blockwise_handles,
														int block_mat_size);

/**
 * @brief Translates a lower triangular square matrix in a column major block storage format into a column major full storage format.
 * This function uses the StarPU runtime system to perform the translation in parallel.
 * 
 * @param blockwise_handles Column major full storage matrix of handles representing the blocks in the source matrix
 * @param columnwise_handles Column major full storage matrix of handles representing the blocks in the destination matrix
 * @param block_mat_size The width and height of both of the handle matrices
 */
void starpu_translate_blockwise_to_columnwise_parallel (starpu_data_handle_t* blockwise_handles, starpu_data_handle_t* columnwise_handles,
														int block_mat_size);
#endif

#if defined (USE_OPENMP)
/**
 * @brief Translates a lower triangular square matrix in a column major full storage format into a column major block storage format.
 * This function uses the OpenMP runtime system to perform the translation in parallel.
 * 
 * @param columnwise_block_data_mat Column major full storage matrix of data about blocks in the source matrix
 * @param blockwise_block_data_mat Column major full storage matrix of data about blocks in the destination matrix
 * @param block_mat_size The width and height of both of the block data matrices
 */
void openmp_translate_columnwise_to_blockwise_parallel (block_data_t* columnwise_block_data_mat,
														block_data_t* blockwise_block_data_mat,
														int block_mat_size);

/**
 * @brief Translates a lower triangular square matrix in a column major block storage format into a column major full storage format.
 * This function uses the OpenMP runtime system to perform the translation in parallel.
 * 
 * @param blockwise_block_data_mat Column major full storage matrix of data about blocks in the source matrix
 * @param columnwise_block_data_mat Column major full storage matrix of data about blocks in the destination matrix
 * @param block_mat_size The width and height of both of the block data matrices
 */
void openmp_translate_blockwise_to_columnwise_parallel (block_data_t* blockwise_block_data_mat,
														block_data_t* columnwise_block_data_mat,
														int block_mat_size);
#endif

#endif /* TRANSLATION_H_ */
