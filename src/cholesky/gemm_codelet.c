#include "gemm_codelet.h"

#include "starpu_matrix_util.h"

#if defined (USE_MKL)
#include <mkl.h>
#elif defined (USE_AOCL) || defined (USE_OPENBLAS)
#include <cblas.h>
#elif defined (USE_ARMPL)
#include <armpl.h>
#else
#include "../gemm/gemm.h"
#endif

#if defined (USE_STARPU)
void gemm_func (void* buffers[], __attribute__ ((unused)) void* arg) {
	int M = STARPU_COL_MATRIX_GET_NY (buffers[0]);
	int K = STARPU_COL_MATRIX_GET_NY (buffers[1]);
	int N = STARPU_COL_MATRIX_GET_NX (buffers[2]);

	int LDA = STARPU_COL_MATRIX_GET_LD (buffers[0]);
	int LDB = STARPU_COL_MATRIX_GET_LD (buffers[1]);
	int LDC = STARPU_COL_MATRIX_GET_LD (buffers[2]);

	double* A = (double*)STARPU_MATRIX_GET_PTR (buffers[0]);
	double* B = (double*)STARPU_MATRIX_GET_PTR (buffers[1]);
	double* C = (double*)STARPU_MATRIX_GET_PTR (buffers[2]);

	#if defined (USE_MKL) || defined (USE_AOCL) || defined (USE_OPENBLAS) || defined (USE_ARMPL)
	cblas_dgemm (CblasColMajor, CblasNoTrans, CblasTrans, M, N, K, -1, A, LDA, B, LDB, 1, C, LDC);
	#else
	dgemm ('N', 'T', M, N, K, -1, A, LDA, B, LDB, 1, C, LDC);
	#endif
}

struct starpu_codelet gemm_codelet = {
    .cpu_funcs = { gemm_func },
    .cpu_funcs_name = { "gemm" },
    .nbuffers = 3,
    .modes = { STARPU_R, STARPU_R, STARPU_RW },
	.name = "GEMM"
};
#endif

#if defined (USE_OPENMP)
void generic_gemm (block_data_t A_block_data, block_data_t B_block_data, block_data_t C_block_data) {
	int M = A_block_data.height;
	int K = B_block_data.width;
	int N = C_block_data.width;

	int LDA = A_block_data.leading_dimension;
	int LDB = B_block_data.leading_dimension;
	int LDC = C_block_data.leading_dimension;

	double* A = A_block_data.ptr;
	double* B = B_block_data.ptr;
	double* C = C_block_data.ptr;

	#if defined (USE_MKL) || defined (USE_AOCL) || defined (USE_OPENBLAS) || defined (USE_ARMPL)
	cblas_dgemm (CblasColMajor, CblasNoTrans, CblasTrans, M, N, K, -1, A, LDA, B, LDB, 1, C, LDC);
	#else
	dgemm ('N', 'T', M, N, K, -1, A, LDA, B, LDB, 1, C, LDC);
	#endif
}
#endif
