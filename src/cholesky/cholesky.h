#ifndef CHOLESKY_H_
#define CHOLESKY_H_

#include "translation_methods.h"

#if defined (USE_OPENMP)
/**
 * @brief Computes the Cholesky factorization of an N x N symmetric positive definite matrix A.
 * The computation is done in place, so the original matrix is overwritten with L such that A = L * L^T.
 * 
 * This function utilizes the OpenMP runtime system, the DGEMM, DSYRK and DTRSM Level 3 BLAS functions,
 * and the DPOTF2 LAPACK routine to perform the computation.
 * 
 * @param orig The symmetric positive definite matrix to be factorized
 * @param size The width and height of the factorized matrix
 * @param block_size The main block size of the factorized matrix
 * @param translation_method The translation method used for the computation
 */
void openmp_block_cholesky (double* orig, int size, int block_size, translation_method_e translation_method);
#endif

#if defined (USE_STARPU)
/**
 * @brief Computes the Cholesky factorization of an N x N symmetric positive definite matrix A.
 * The computation is done in place, so the original matrix is overwritten with L such that A = L * L^T.
 * 
 * This function utilizes the StarPU runtime system, the DGEMM, DSYRK and DTRSM Level 3 BLAS functions,
 * and the DPOTF2 LAPACK routine to perform the computation.
 * 
 * @param orig The symmetric positive definite matrix to be factorized
 * @param size The width and height of the factorized matrix
 * @param block_size The main block size of the factorized matrix
 * @param translation_method The translation method used for the computation
 * @param shutdown_starpu A value indicating whether StarPU should be shut down at the end of the computation
 * @return int The error code (zero if the function has finished correctly)
 */
int starpu_block_cholesky (double* orig, int size, int block_size, translation_method_e translation_method, int shutdown_starpu);
#endif

#endif /* CHOLESKY_H_ */
