#include "../common/math_util.h"

#include "translation.h"
#include "starpu_matrix_util.h"
#include "cholesky.h"

#include "gemm_codelet.h"
#include "syrk_codelet.h"
#include "trsm_codelet.h"
#include "potf2_codelet.h"

#if defined (USE_STARPU)
#include <starpu.h>
#endif

#include <stdlib.h>

#define CHOLESKY_EXPORT __attribute__ ((visibility ("default")))

// Codelets

#if defined (USE_STARPU)
extern struct starpu_codelet gemm_codelet;
extern struct starpu_codelet syrk_codelet;
extern struct starpu_codelet trsm_codelet;
extern struct starpu_codelet potf2_codelet;
#endif

// Helper functions

#if defined (USE_STARPU)
int starpu_ensure_initialized () {
	if (starpu_is_initialized ()) {
		return 0;
	}

	if (starpu_init (NULL) != 0) {
		return 1;
	}
	starpu_wait_initialized ();
	return 0;
}
#endif

// Main function

#if defined (USE_OPENMP)
CHOLESKY_EXPORT
void openmp_block_cholesky (double* orig, int size, int block_size, translation_method_e translation_method) {
	if (size == 0) {
		return;
	}

	// Clamp block_size to be between 1 and block_size
	block_size = clamp (block_size, 1, size);

	double* work;
	if (translation_method == PARALLEL_TRANSLATION) {
		// If the translation is done in parallel using OpenMP, it cannot be done in place,
		// so we need a second matrix to store the blockwise represetation in
		work = (double*)malloc (size * size * sizeof (double));
	} else {
		// If the translation is done sequentially, we can do it in place
		work = orig;
	}

	int block_mat_size = (size - 1) / block_size + 1;
	int no_blocks = block_mat_size * block_mat_size;

	if (translation_method == SEQUENTIAL_TRANSLATION) {
		translate_columnwise_to_blockwise_sequential (work, size, block_size, size);
	}

	// orig_block_data_mat is only used when translation_method = NO_TRANSLATION
	block_data_t* orig_block_data_mat = NULL;
	block_data_t* work_block_data_mat = (block_data_t*)malloc (no_blocks * sizeof (block_data_t));;

	if (translation_method == PARALLEL_TRANSLATION) {
		orig_block_data_mat = (block_data_t*)malloc (no_blocks * sizeof (block_data_t));
	}

	for (int col_start = 0; col_start < size; col_start += block_size) {
		int col_size = min (size - col_start, block_size);
		int block_col = col_start / block_size;

		for (int row_start = 0; row_start < size; row_start += block_size) {
			int row_size = min (size - row_start, block_size);
			int block_row = row_start / block_size;

			if (translation_method == NO_TRANSLATION) {
				double* block_start_work = work + col_start * size + row_start;

				work_block_data_mat[block_col * block_mat_size + block_row] = (block_data_t){
					.height = row_size,
					.width = col_size,
					.leading_dimension = size,
					.ptr = block_start_work
				};
			} else {
				double* block_start_work = work + col_start * size + row_start * col_size;

				work_block_data_mat[block_col * block_mat_size + block_row] = (block_data_t){
					.height = row_size,
					.width = col_size,
					.leading_dimension = row_size,
					.ptr = block_start_work
				};
			}
			if (translation_method == PARALLEL_TRANSLATION) {
				double* block_start_orig = orig + col_start * size + row_start;

				orig_block_data_mat[block_col * block_mat_size + block_row] = (block_data_t){
					.height = row_size,
					.width = col_size,
					.leading_dimension = size,
					.ptr = block_start_orig
				};
			}
		}
	}
 
	if (translation_method == PARALLEL_TRANSLATION) {
		openmp_translate_columnwise_to_blockwise_parallel (orig_block_data_mat, work_block_data_mat, block_mat_size);
	}

	for (int i = 0; i < block_mat_size; i++) {
		block_data_t A_ii_block_data = work_block_data_mat[i * block_mat_size + i];

		#pragma omp task depend (inout: work_block_data_mat[i * block_mat_size + i])
		{
			generic_potf2 (A_ii_block_data);
		}

		for (int j = i + 1; j < block_mat_size; j++) {
			block_data_t A_ji_block_data = work_block_data_mat[i * block_mat_size + j];

			#pragma omp task depend (in: work_block_data_mat[i * block_mat_size + i]) \
				depend (inout: work_block_data_mat[i * block_mat_size + j])
			{
				generic_trsm (A_ii_block_data, A_ji_block_data);
			}
		}

		for (int k = i + 1; k < block_mat_size; k++) {
			block_data_t A_kk_block_data = work_block_data_mat[k * block_mat_size + k];
			block_data_t A_ki_block_data = work_block_data_mat[i * block_mat_size + k];

			#pragma omp task depend (in: work_block_data_mat[i * block_mat_size + k]) \
				depend (inout: work_block_data_mat[k * block_mat_size + k])
			{
				generic_syrk (A_ki_block_data, A_kk_block_data);
			}

			for (int j = k + 1; j < block_mat_size; j++) {
				block_data_t A_ji_block_data = work_block_data_mat[i * block_mat_size + j];
				block_data_t A_jk_block_data = work_block_data_mat[k * block_mat_size + j];

				#pragma omp task depend(in: work_block_data_mat[i * block_mat_size + j]) \
					depend(in: work_block_data_mat[i * block_mat_size + k]) \
					depend(inout: work_block_data_mat[k * block_mat_size + j])
				{
					generic_gemm (A_ji_block_data, A_ki_block_data, A_jk_block_data);
				}
			}
		}
	}

	if (translation_method == PARALLEL_TRANSLATION) {
		openmp_translate_blockwise_to_columnwise_parallel (work_block_data_mat, orig_block_data_mat, block_mat_size);
	}

	// Wait for all tasks to finish
	#pragma omp barrier

	if (translation_method == SEQUENTIAL_TRANSLATION) {
		translate_blockwise_to_columnwise_sequential (work, size, block_size, size);
	}

	// Free all allocated helper matrices
	if (translation_method == PARALLEL_TRANSLATION) {
		free (work);
		free (orig_block_data_mat);
	}
	free (work_block_data_mat);
}
#endif

#if defined (USE_STARPU)
CHOLESKY_EXPORT
int starpu_block_cholesky (double* orig, int size, int block_size, translation_method_e translation_method, int shutdown_starpu) {

	// Ensure that StarPU is initialized
	if (starpu_ensure_initialized () != 0) {
		return 1;
	}

	if (size == 0) {
		return 0;
	}

	// Clamp block_size to be between 1 and block_size
	block_size = clamp (block_size, 1, size);
	
	double* work;
	if (translation_method == PARALLEL_TRANSLATION) {
		// If the translation is done in parallel (using StarPU), it cannot be done in place,
		// so we need a second matrix to store the blockwise represetation in
		work = (double*)malloc (size * size * sizeof (double));
	} else {
		// If the translation is done sequentially, we can do it in place
		work = orig;
	}

	int block_mat_size = (size - 1) / block_size + 1;
	int no_blocks = block_mat_size * block_mat_size;

	// If the translation is sequential, we need to do it before we register the data in StarPU
	if (translation_method == SEQUENTIAL_TRANSLATION) {
		translate_columnwise_to_blockwise_sequential (work, size, block_size, size);
	}

	// orig_handles_mat is only used when translation_method = NO_TRANSLATION
	starpu_data_handle_t* orig_handles_mat = NULL;
	starpu_data_handle_t* work_handles_mat = (starpu_data_handle_t*)malloc (no_blocks * sizeof (starpu_data_handle_t));;

	if (translation_method == PARALLEL_TRANSLATION) {
		orig_handles_mat = (starpu_data_handle_t*)malloc (no_blocks * sizeof (starpu_data_handle_t));
	}

	for (int col_start = 0; col_start < size; col_start += block_size) {
		int col_size = min (size - col_start, block_size);
		int block_col = col_start / block_size;

		for (int row_start = 0; row_start < size; row_start += block_size) {
			int row_size = min (size - row_start, block_size);
			int block_row = row_start / block_size;

			if (translation_method == NO_TRANSLATION) {
				double* block_start_work = work + col_start * size + row_start;

				STARPU_COL_MATRIX_DATA_REGISTER (&work_handles_mat[block_col * block_mat_size + block_row],
											 STARPU_MAIN_RAM,
											 (uintptr_t)block_start_work, 
											 size,
											 col_size,
											 row_size,
											 sizeof (double));
			} else {
				double* block_start_work = work + col_start * size + row_start * col_size;

				STARPU_COL_MATRIX_DATA_REGISTER (&work_handles_mat[block_col * block_mat_size + block_row],
											 STARPU_MAIN_RAM,
											 (uintptr_t)block_start_work,
											 row_size,
											 col_size,
											 row_size,
											 sizeof (double));
			}
			if (translation_method == PARALLEL_TRANSLATION) {
				double* block_start_orig = orig + col_start * size + row_start;

				STARPU_COL_MATRIX_DATA_REGISTER (&orig_handles_mat[block_col * block_mat_size + block_row],
											 STARPU_MAIN_RAM,
											 (uintptr_t)block_start_orig, 
											 size,
											 col_size,
											 row_size,
											 sizeof (double));
			}
		}
	}
 
	// If the translation is parallel, we need to do it after we register the data in StarPU
	if (translation_method == PARALLEL_TRANSLATION) {
		starpu_translate_columnwise_to_blockwise_parallel (orig_handles_mat, work_handles_mat, block_mat_size);
	}

	for (int i = 0; i < block_mat_size; i++) {
		starpu_data_handle_t A_ii_handle = work_handles_mat[i * block_mat_size + i];

		starpu_task_insert (&potf2_codelet, STARPU_RW, A_ii_handle, 0);

		for (int j = i + 1; j < block_mat_size; j++) {
			starpu_data_handle_t A_ji_handle = work_handles_mat[i * block_mat_size + j];

			starpu_task_insert (&trsm_codelet, STARPU_R, A_ii_handle, STARPU_RW, A_ji_handle, 0);
		}

		for (int k = i + 1; k < block_mat_size; k++) {
			starpu_data_handle_t A_kk_handle = work_handles_mat[k * block_mat_size + k];
			starpu_data_handle_t A_ki_handle = work_handles_mat[i * block_mat_size + k];

			starpu_task_insert (&syrk_codelet, STARPU_R, A_ki_handle, STARPU_RW, A_kk_handle, 0);

			for (int j = k + 1; j < block_mat_size; j++) {
				starpu_data_handle_t A_ji_handle = work_handles_mat[i * block_mat_size + j];
				starpu_data_handle_t A_jk_handle = work_handles_mat[k * block_mat_size + j];

				starpu_task_insert (&gemm_codelet, STARPU_R, A_ji_handle, STARPU_R, A_ki_handle, STARPU_RW, A_jk_handle, 0);
			}
		}
	}

	// If the translation is parallel, we need to do it before we deregister the data in StarPU
	if (translation_method == PARALLEL_TRANSLATION) {
		starpu_translate_blockwise_to_columnwise_parallel (work_handles_mat, orig_handles_mat, block_mat_size);
	}

	// Wait for all tasks to finish
	starpu_task_wait_for_all ();

	for (int block_id = 0; block_id < no_blocks; block_id++) {
		starpu_data_unregister (work_handles_mat[block_id]);

		if (translation_method == PARALLEL_TRANSLATION) {
			starpu_data_unregister (orig_handles_mat[block_id]);
		}
	}

	// If the translation is sequential, we need to do it after we deregister the data in StarPU
	if (translation_method == SEQUENTIAL_TRANSLATION) {
		translate_blockwise_to_columnwise_sequential (work, size, block_size, size);
	}

	// Free all allocated helper matrices
	if (translation_method == PARALLEL_TRANSLATION) {
		free (work);
		free (orig_handles_mat);
	}
	free (work_handles_mat);

	// Shutdown StarPU (if requested by the user)
	if (shutdown_starpu) {
		starpu_shutdown ();
	}

	return 0;
}
#endif
