#ifndef POTF2_CODELET_H_
#define POTF2_CODELET_H_

#if defined (USE_STARPU)
#include <starpu.h>
#endif

#if defined (USE_OPENMP)
#include "block_data.h"
#endif

#if defined (USE_STARPU)
/**
 * @brief A StarPU codelet to perform the POTF2 computation (see src/potf2/potf2.h) with UPLO = 'L'.
 */
extern struct starpu_codelet potf2_codelet;
#endif

#if defined (USE_OPENMP)
/**
 * @brief Performs the POTF2 computation (see src/potf2/potf2.h) with UPLO = 'L'.
 */
void generic_potf2 (block_data_t A_block_data);
#endif

#endif /* POTF2_CODELET_H_ */
