#ifndef TRANSLATION_METHOD_H_
#define TRANSLATION_METHOD_H_

/**
 * @brief An enum type representing the possible methods for matrix storage format translation:
 * - NO_TRANSLATION - no translation is done
 * - SEQUENTIAL_TRANSLATION - the translation is done sequentially
 * - PARALLEL_TRANSLATION - the translation is done in parallel using the OpenMP or StarPU runtime systems
 */
typedef enum {
	NO_TRANSLATION = 1,
	SEQUENTIAL_TRANSLATION = 2,
	PARALLEL_TRANSLATION = 3
} translation_method_e;

#endif /* TRANSLATION_METHOD_H_ */
