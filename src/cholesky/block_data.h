#ifndef BLOCK_DATA_H_
#define BLOCK_DATA_H_

/**
 * @brief Structure representing data about a single matrix blockin a column major full storage format.
 * 
 */
typedef struct {
	int width, height, leading_dimension;
	double* ptr;
} block_data_t;

#endif /* BLOCK_DATA_H_ */
