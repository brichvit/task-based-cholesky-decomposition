#ifndef TESTING_H_
#define TESTING_H_

/**
 * @brief Fills the entries above the main diagonal of a square matrix with zeros.
 * The matrix has to be in a column major full storage format with the leading dimension equal to its size.
 * 
 * @param mat The matrix to set the entries of
 * @param size The size of the matrix
 */
void make_matrix_lower_triangular (double* mat, int size);

/**
 * @brief Calculates the test ratio |A-LL^T| / n / |A| / (machine epsilon), where L and A are square matrices of size n
 * in a column major full storage format, L is lower triangular and the leading dimensions of both L and A are equal to n.
 * 
 * @param L The matrix L
 * @param A The matrix A
 * @param size The size of matrices @p L and @p A (denoted by n in the formula above)
 * @return double The test ratio
 */
double get_cholesky_test_ratio (double* L, double* A, int size);

#endif /* TESTING_H_ */
