#include "matrix_generation.h"

#include <stdlib.h>
#include <math.h>

#if defined (USE_MKL)
#include <mkl.h>
#elif defined (USE_AOCL)
#define BLIS_DISABLE_BLAS_DEFS
#include <cblas.h>
#elif defined (USE_OPENBLAS)
#include <cblas.h>
#include <lapacke.h>
#include <omp.h>
#elif defined (USE_ARMPL)
#include <armpl.h>
#else
#include "../../src/gemm/gemm.h"
#endif

void make_matrix_lower_triangular (double* mat, int size) {
	for (int row = 0; row < size; row++) {
		for (int col = row + 1; col < size; col++) {
			mat[col * size + row] = 0;
		}
	}
}

double calculate_symmetric_matrix_1_norm (double* mat, int size) {
	double norm = 0;

	for (int col = 0; col < size; col++) {
		double column_sum = 0;

		for (int row = 0; row < col; row++) {
			column_sum += fabs (mat[row * size + col]);
		}
		column_sum += fabs (mat[col * size + col]);
		for (int row = col + 1; row < size; row++) {
			column_sum += fabs (mat[col * size + row]);
		}

		norm = fmax (norm, column_sum);
	}

	return norm;
}

double get_cholesky_test_ratio (double* L, double* orig_mat, int size) {
	double* A = (double*)malloc (size * size * sizeof (double));

	// Copy orig_mat into A
	#if defined (USE_MKL) || defined (USE_OPENBLAS) || defined (USE_ARMPL)
	LAPACKE_dlacpy (LAPACK_COL_MAJOR, 'L', size, size, orig_mat, size, A, size);
	#elif defined (USE_AOCL)
	dlacpy_ ("L", &size, &size, orig_mat, &size, A, &size);
	#else
	copy_lower_triangular_matrix (orig_mat, A, size, size);
	#endif


	// Compute the norm of A
	double a_norm;
	#if defined (USE_MKL) || defined (USE_OPENBLAS) || defined (USE_ARMPL)
	a_norm = LAPACKE_dlansy (LAPACK_COL_MAJOR, '1', 'L', size, A, size);
	#else
	a_norm = calculate_symmetric_matrix_1_norm (A, size);
	#endif

	// Replace A with the residue matrix (A - LL^T)
	#if defined (USE_MKL) || defined (USE_AOCL) || defined (USE_OPENBLAS) || defined (USE_ARMPL)
	cblas_dgemm (CblasColMajor, CblasNoTrans, CblasTrans, size, size, size, -1, L, size, L, size, 1, A, size);
	#else
	dgemm ('N', 'T', size, size, size, -1, L, size, L, size, 1, A, size);
	#endif

	// Calculate the norm of the residue matrix A - LL^T
	double residue_norm = 0;

	#if defined (USE_MKL) || defined (USE_OPENBLAS) || defined (USE_ARMPL)
	residue_norm = LAPACKE_dlansy (LAPACK_COL_MAJOR, '1', 'L', size, A, size);
	#else
	residue_norm = calculate_symmetric_matrix_1_norm (A, size);
	#endif

	free (A);

	return residue_norm / size / a_norm / __DBL_EPSILON__;
}
