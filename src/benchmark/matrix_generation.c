#include "matrix_generation.h"

#if defined (USE_MKL)
#include <mkl.h>
#elif defined (USE_AOCL) || defined (USE_OPENBLAS)
#include <cblas.h>
#elif defined (USE_ARMPL)
#include <armpl.h>
#else
#include "../../src/gemm/gemm.h"
#endif

#include <stdlib.h>

inline void set_random_seed (int seed);
void set_random_seed (int seed) {
	srand (seed);
}

inline double get_random_uniform_number ();
double get_random_uniform_number () {
	return (double)rand () / (double)RAND_MAX;
}

void generate_random_general_matrix (double* mat, int width, int height, int leading_dimension) {
	for (int col = 0; col < width; col++) {
		for (int row = 0; row < height; row++) {
			mat[col * leading_dimension + row] = get_random_uniform_number ();
		}
	}
}

void generate_random_sypod_matrix (double* mat, int mat_size, int leading_dimension) {
	double* A = (double*)malloc (mat_size * mat_size * sizeof (double));

	// Generate a random square matrix A
	generate_random_general_matrix (A, mat_size, mat_size, mat_size);

	// Compute mat <- A * A^T (so that mat is symmetric)
	#if defined (USE_MKL) || defined (USE_AOCL) || defined (USE_OPENBLAS) || defined (USE_ARMPL)
	cblas_dgemm (CblasColMajor, CblasNoTrans, CblasTrans, mat_size, mat_size, mat_size, 1, A, mat_size, A, mat_size, 0, mat, leading_dimension);
	#else
	dgemm ('N', 'T', mat_size, mat_size, mat_size, 1, A, mat_size, A, mat_size, 0, mat, leading_dimension);
	#endif

	free (A);

	// Add 1 to diagonal elements of mat (so that mat is symmetric positive definite)
	for (int i = 0; i < mat_size; i++) {
		mat[i * leading_dimension + i] += 1;
	}
}

void copy_lower_triangular_matrix (double* src_mat, double* dest_mat, int size, int leading_dimension) {
	for (int col = 0; col < size; col++) {
		for (int row = col; row < size; row++) {
			dest_mat[col * leading_dimension + row] = src_mat[col * leading_dimension + row];
		}
	}
}
