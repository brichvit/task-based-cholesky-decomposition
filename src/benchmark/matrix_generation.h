#ifndef MATRIX_GENERATION_H_
#define MATRIX_GENERATION_H_

/**
 * @brief Sets the random seed to be used for subsequent matrix generation.
 * 
 * @param seed The new random seed 
 */
void set_random_seed (int seed);

/**
 * @brief Fills a general matrix in a column major full storage format with uniform random values from the interval [0,1].
 * 
 * @param mat The pointer to the matrix
 * @param width The width of the matrix
 * @param height The height of the matrix
 * @param leading_dimension The leading dimension of the matrix 
 */
void generate_random_general_matrix (double* mat, int width, int height, int leading_dimension);

/**
 * @brief Fills a square matrix in a column major full storage format with uniform random values such that the generated matrix
 * is symmetric positive definite.
 * 
 * @param mat The pointer to the matrix
 * @param size The width and height of the matrix
 * @param leading_dimension The leading dimension of the matrix 
 */
void generate_random_sypod_matrix (double* mat, int mat_size, int leading_dimension);

/**
 * @brief Copies the lower triangular part of a square matrix to another square matrix.
 * Both matrices are assumed to be in a column major full storage format.
 * 
 * @param src_mat The source matrix to copy entries from
 * @param dest_mat The destination matrix to copy entries to
 * @param size The width and height of both @p src_mat and @p dest_mat
 * @param leading_dimension The leading dimension of both @p src_mat and @p dest_mat
 */
void copy_lower_triangular_matrix (double* src_mat, double* dest_mat, int size, int leading_dimension);

#endif /* MATRIX_GENERATION_H_ */
