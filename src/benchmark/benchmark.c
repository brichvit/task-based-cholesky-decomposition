#include "../../src/cholesky/cholesky.h"
#include "arg_parsing.h"
#include "matrix_generation.h"
#include "testing.h"

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include <argp.h>

#if defined (USE_MKL)
#include <mkl.h>
#elif defined (USE_AOCL)
#include <FLAME.h>
#elif defined (USE_OPENBLAS)
#include <cblas.h>
#include <lapacke.h>
#elif defined (USE_ARMPL)
#include <armpl.h>
#endif

#if defined (USE_STARPU)
#include <starpu.h>
#endif

#if defined (USE_OPENMP)
#include <omp.h>
#endif

#include <assert.h>

#define DEFAULT_MAT_SIZE 3000
#define DEFAULT_ITERATIONS 1
#define DEFAULT_SEED time (NULL)
#define DEFAULT_NO_THREADS -1
#define DEFAULT_BLOCK_SIZE 128

#if defined (USE_MKL)
#define CUSTOM_STARPU_IMPLEMENTATION_TEXT "Custom DPOTRF using StarPU and MKL routines"
#define CUSTOM_OPENMP_IMPLEMENTATION_TEXT "Custom DPOTRF using OpenMP and MKL routines"
#define LIBRARY_IMPLEMENTATION_TEXT "MKL DPOTRF"
#elif defined (USE_AOCL)
#define CUSTOM_STARPU_IMPLEMENTATION_TEXT "Custom DPOTRF using StarPU and AOCL routines"
#define CUSTOM_OPENMP_IMPLEMENTATION_TEXT "Custom DPOTRF using OpenMP and AOCL routines"
#define LIBRARY_IMPLEMENTATION_TEXT "AOCL DPOTRF"
#elif defined (USE_OPENBLAS)
#define CUSTOM_STARPU_IMPLEMENTATION_TEXT "Custom DPOTRF using StarPU and OpenBLAS routines"
#define CUSTOM_OPENMP_IMPLEMENTATION_TEXT "Custom DPOTRF using OpenMP and OpenBLAS routines"
#define LIBRARY_IMPLEMENTATION_TEXT "OpenBLAS DPOTRF"
#elif defined (USE_ARMPL)
#define CUSTOM_STARPU_IMPLEMENTATION_TEXT "Custom DPOTRF using StarPU and ARMPL routines"
#define CUSTOM_OPENMP_IMPLEMENTATION_TEXT "Custom DPOTRF using OpenMP and ARMPL routines"
#define LIBRARY_IMPLEMENTATION_TEXT "ARMPL DPOTRF"
#else
#define CUSTOM_STARPU_IMPLEMENTATION_TEXT "Custom DPOTRF using StarPU and custom routines"
#define CUSTOM_OPENMP_IMPLEMENTATION_TEXT "Custom DPOTRF using OpenMP and custom routines"
#endif

#define TEST_RATIO_THRESHOLD 30

// Helper struct for argp.h representing command line options
struct argp_option options[] = {
	{.name = "mat-size", .key = 'm', .arg = "<Matrix size>", .flags = 0, .doc = "The size of the matrix."},
  	{.name = "iterations", .key = 'i', .arg = "<Iterations>", .flags = 0, .doc = "The number of times the routine is run for each matrix size."},
	{.name = "seed", .key = 's', .arg = "<Random seed>", .flags = 0, .doc = "The random seed used to generate the matrix."},
	{.name = "no-threads", .key = 't', .arg = "<Number of threads>", .flags = 0, .doc = "The number of threads used (both by the MKL and custom implementations) to perform the Cholesky decomposition."},
	{.name = "block-size", .key = 'b', .arg = "<Block size>", .flags = 0, .doc = "The block size - used for the custom implementation only."},
	{.name = "translation-method", .key = 'T', .arg = "<Translation method>", .flags = 0, .doc = "The translation method used in the custom implementation (possible values: no, sequential, parallel)."},
	{.name = "custom-only", .key = 'c', .arg = NULL, .flags = 0, .doc = "Only run the custom implementation and skip the library one."},
	{.name = "check-correctness", .key = 'C', .arg = NULL, .flags = 0, .doc = "Calculate the test ratio and print the test rasults."},
	{.name = "fxt-trace", .key = 'f', .arg = NULL, .flags = 0, .doc = "Uses special calls to StarPU functions to ensure correct trace generation using the FxT library. Can only be used when "},
	{0}
};

// Helper struct for argp.h storing parameter values
struct arguments {
    start_end_step_range_t mat_size, block_size;
    int iterations, seed, no_threads, custom_only, test_correctness, fxt_trace_compat;
	translation_method_e translation_method;
};

// Callback for argp.h used to parse command line arguments
error_t parse_opt(int key, char *arg, struct argp_state *state) {
	struct arguments *arguments = state->input;

    switch (key) {
        case 'm': arguments->mat_size = parse_positive_start_end_step_range (arg, "<Matrix size>"); break;
		case 'i': arguments->iterations = parse_positive_int_argument (arg, "<Iterations>"); break;
		case 's': arguments->seed = parse_nonnegative_int_argument (arg, "<Random seed>"); break;
		case 't': arguments->no_threads = parse_positive_int_argument (arg, "<Number of threads>"); break;
		case 'b': arguments->block_size = parse_positive_start_end_step_range (arg, "<Block size>"); break;
		case 'T': arguments->translation_method = parse_translation_method (arg, "<Translation method>"); break;
		case 'C': arguments->test_correctness = 1; break;
		case 'c': arguments->custom_only = 1; break;
		case 'f': arguments->fxt_trace_compat = 1; break;
		case ARGP_KEY_ARG: argp_usage (state); break;
        default: return ARGP_ERR_UNKNOWN;
	}
    return 0;
}

// Helper struct for argp.h
struct argp argp = {
	.options = options, 
	.parser = parse_opt
};

// Calculate wall time from start and end clock_t timestamps
double calculate_wall_time (struct timespec start, struct timespec finish) {
	return finish.tv_sec - start.tv_sec + (finish.tv_nsec - start.tv_nsec) * 1e-9;
}

void print_cholesky_result (char* implementation_name, double time, int mat_size, int block_size, int no_threads, int iteration) {
	// Calculate the performance in Gflop/s
	double floating_point_ops = ((double)mat_size * (double)mat_size * (double)mat_size) / 3.0 +
								((double)mat_size * (double)mat_size) / 2.0 +
								(double)mat_size / 6.0;
	double gflop_per_second = floating_point_ops / 1e9 / time;

	printf ("%-48s\t%-11.3lf\t%-11.3lf\t%-11d\t%-11d\t%-11d\t%-11d\n",
		implementation_name,
		time,
		gflop_per_second,
		mat_size,
		block_size,
		no_threads,
		iteration
	);
}

void print_cholesky_result_with_test (char* implementation_name, double test_ratio, double time, int mat_size, int block_size, int no_threads, int iteration) {
	// Calculate the performance in Gflop/s
	double floating_point_ops = ((double)mat_size * (double)mat_size * (double)mat_size) / 3.0 +
								((double)mat_size * (double)mat_size) / 2.0 +
								(double)mat_size / 6.0;
	double gflop_per_second = floating_point_ops / 1e9 / time;

	printf ("%-48s\t%-11s\t%-11.6lf\t%-11.3lf\t%-11.3lf\t%-11d\t%-11d\t%-11d\t%-11d\n",
		implementation_name,
		(test_ratio <= TEST_RATIO_THRESHOLD) ? "pass" : "fail",
		test_ratio,
		time,
		gflop_per_second,
		mat_size,
		block_size,
		no_threads,
		iteration
	);
}

#if defined (USE_ARMPL)
void armpl_set_num_threads_helper (int* no_threads) {
	armpl_set_num_threads (*no_threads);
}
#endif

int main (int argc, char** argv) {
	// Set default parameter values
	struct arguments args;
	args.mat_size = get_single_value_start_end_step_range (DEFAULT_MAT_SIZE);
	args.iterations = DEFAULT_ITERATIONS;
	args.seed = DEFAULT_SEED;
	args.no_threads = DEFAULT_NO_THREADS;
	args.block_size = get_single_value_start_end_step_range (DEFAULT_BLOCK_SIZE);
	args.translation_method = PARALLEL_TRANSLATION;
	args.custom_only = 0;
	args.test_correctness = 0;
	args.fxt_trace_compat = 0;

	// Parse command line arguments
    argp_parse (&argp, argc, argv, 0, 0, &args);

	// Check that --fxt-trace is not use when StarPU is not in use
	#if !defined (USE_STARPU)
	if (args.fxt_trace_compat) {
		fprintf (stderr, "Error: the --fxt-trace option may only be used when StarPU is in use\n");
		exit (INVALID_ARGUMENT_COMBINATION);
	}
	#endif

	// Check that no incompatible argument combination has been used
	if (args.fxt_trace_compat && args.iterations != 1) {
		fprintf (stderr, "Error: the --fxt-trace option may not be used when the --iteration option has a value larger than 1\n");
		exit (INVALID_ARGUMENT_COMBINATION);
	}
	if (args.fxt_trace_compat && !args.custom_only) {
		fprintf (stderr, "Error: the --fxt-trace option may only be used in combination with the --custom-only option\n");
		exit (INVALID_ARGUMENT_COMBINATION);
	}

	if (args.block_size.start != args.block_size.end && !args.custom_only) {
		fprintf (stderr, "Error: start-step-end range for the --block-size option may only be used in combination with the --custom-only option\n");
		exit (INVALID_ARGUMENT_COMBINATION);
	}

	// Set the random seed for matrix generation
	set_random_seed (args.seed);

	// Tell StarPU to not start profiling right after initialization, otherwise the generated trace
	// will have a big time gap before the first submitted task 
	#if defined (USE_STARPU)
	if (args.fxt_trace_compat) {
		starpu_fxt_autostart_profiling (0);
	}
	#endif

	// Set number of threads used by StarPU or obtain the real number of CPUs available from either OpenMP or StarPU
	#if defined (USE_STARPU)
	if (args.no_threads == DEFAULT_NO_THREADS) {
		if (starpu_init (NULL) != 0) {
			fprintf (stderr, "Error: StarPU could not be initialized\n");
			exit (STARPU_INITIALIZATION_ERROR);
		}
		args.no_threads = starpu_cpu_worker_get_count ();
		starpu_shutdown ();
	}

	struct starpu_conf starpu_init_conf;
	starpu_conf_init (&starpu_init_conf);
	starpu_init_conf.ncpus = args.no_threads;
	#elif defined (USE_MKL)
	if (args.no_threads == DEFAULT_NO_THREADS) {
		args.no_threads = mkl_get_max_threads ();
	} else {
		omp_set_num_threads (args.no_threads);
	}
	#else
	if (args.no_threads == DEFAULT_NO_THREADS) {
		args.no_threads = omp_get_max_threads ();
	} else {
		omp_set_num_threads (args.no_threads);
	}
	#endif

	// Print used parameter values
	printf ("Cholesky decomposition benchmark utility\n\n");
	if (args.mat_size.start == args.mat_size.end) {
		printf ("Used matrix size: %d\n", args.mat_size.start);
	} else {
		printf ("Used matrix sizes: %d - %d (step: %d)\n", args.mat_size.start, args.mat_size.end, args.mat_size.step);
	}
	printf ("Used random seed: %d\n", args.seed);
	printf ("Used number of threads: %u\n", args.no_threads);
	if (args.block_size.start == args.block_size.end) {
		printf ("Used block size (for custom implementation): %d\n", args.block_size.start);
	} else {
		printf ("Used block sizes (for custom implementation): %d - %d (step: %d)\n", args.block_size.start, args.block_size.end, args.block_size.step);
	}

	printf ("\n");
	printf ("-------------------------- RESULTS: --------------------------\n");

	if (args.test_correctness) {
			printf ("%-48s\t%-11s\t%-11s\t%-11s\t%-11s\t%-11s\t%-11s\t%-11s\t%-11s\n",
			"Implementation",
			"Test result",
			"Test ratio",
			"Time",
			"Gflop/s",
			"Matrix size",
			"Block size",
			"No. Threads",
			"Iteration"
		);
	} else {
		printf ("%-48s\t%-11s\t%-11s\t%-11s\t%-11s\t%-11s\t%-11s\n",
			"Implementation",
			"Time",
			"Gflop/s",
			"Matrix size",
			"Block size",
			"No. Threads",
			"Iteration"
		);
	}

	// Iterate through all of the matrix sizes in the input range
	for (int mat_size = args.mat_size.start; mat_size <= args.mat_size.end; mat_size += args.mat_size.step) {

		// Allocate and generate a random symmetric positive definite matrix to perform Cholesky factorization on
		double* orig_mat = (double*)malloc (mat_size * mat_size * sizeof (double));

		// Allocate a second matrix which will be used for computation
		double* work_mat = (double*)malloc (mat_size * mat_size * sizeof (double));

		// Iterate through all of the block sizes in the input range
		for (int block_size = args.block_size.start; block_size <= args.block_size.end; block_size += args.block_size.step) {

			// Perform the requested amount of iterations for this matrix size
			for (int iteration = 0; iteration < args.iterations; iteration++) {

				// Parallelize BLAS routines in order to generate the matrix faster
				#if defined (USE_MKL)
				mkl_set_num_threads (args.no_threads);
				#elif defined (USE_AOCL)
				bli_thread_set_num_threads (args.no_threads);
				#elif defined (USE_OPENBLAS)
				openblas_set_num_threads (args.no_threads);
				#elif defined (USE_ARMPL)
				armpl_set_num_threads (args.no_threads);
				#endif

				// Generate the matrix to perform Cholesky factorization on
				generate_random_sypod_matrix (orig_mat, mat_size, mat_size);

				#if defined (USE_STARPU)
				// Copy it to work_mat
				#if defined (USE_MKL) || defined (USE_OPENBLAS) || defined (USE_ARMPL)
				LAPACKE_dlacpy (LAPACK_COL_MAJOR, 'L', mat_size, mat_size, orig_mat, mat_size, work_mat, mat_size);
				#elif defined (USE_AOCL)
				dlacpy_ ("L", &mat_size, &mat_size, orig_mat, &mat_size, work_mat, &mat_size);
				#else
				copy_lower_triangular_matrix (orig_mat, work_mat, mat_size, mat_size);
				#endif

				// Set number of threads used by MKL/BLIS to 1 so as not to parallelize the BLAS routines
				#if defined (USE_MKL)
				mkl_set_num_threads (1);
				#elif defined (USE_AOCL)
				bli_thread_set_num_threads (1);
				FLASH_Queue_set_num_threads (1);
				#elif defined (USE_OPENBLAS)
				openblas_set_num_threads (1);
				#elif defined (USE_ARMPL)
				armpl_set_num_threads (1);
				// As ARM performance libraries are parallelized using OpenMP, the number of threads count in OpenMP is kept separately
				// for each StarPU worker, so we have to tell StarPU to set the OpenMP thread count to 1 on each worker
				// Source: https://github.com/starpu-runtime/starpu/issues/8
				if (starpu_init (&starpu_init_conf) != 0) {
					free (orig_mat);
					free (work_mat);

					fprintf (stderr, "Error: StarPU could not be initialized\n");
					exit (STARPU_INITIALIZATION_ERROR);
				}
				int one = 1;
				starpu_execute_on_each_worker ((void(*)(void *))armpl_set_num_threads_helper, &one, STARPU_CPU);
				#endif

				// Tell StarPU to start recording data for trace & DAG generation
				if (args.fxt_trace_compat) {
					if (starpu_init (&starpu_init_conf) != 0) {
						free (orig_mat);
						free (work_mat);

						fprintf (stderr, "Error: StarPU could not be initialized\n");
						exit (STARPU_INITIALIZATION_ERROR);
					}
					starpu_fxt_start_profiling ();
				}

				// Run the custom StarPU implementation
				// Source for wall time measurement: https://stackoverflow.com/questions/2962785/c-using-clock-to-measure-time-in-multi-threaded-programs
				struct timespec custom_starpu_start, custom_starpu_finish;
				clock_gettime (CLOCK_MONOTONIC, &custom_starpu_start);
				if (starpu_init (&starpu_init_conf) != 0) {
					free (orig_mat);
					free (work_mat);

					fprintf (stderr, "Error: StarPU could not be initialized\n");
					exit (STARPU_INITIALIZATION_ERROR);
				}
				if (starpu_block_cholesky (work_mat, mat_size, block_size, args.translation_method, !args.fxt_trace_compat) != 0) {
					free (orig_mat);
					free (work_mat);

					fprintf (stderr, "Error: StarPU could not be initialized\n");
					exit (STARPU_INITIALIZATION_ERROR);
				}
				clock_gettime (CLOCK_MONOTONIC, &custom_starpu_finish);

				// Tell StarPU to stop recording data for trace & DAG generation
				if (args.fxt_trace_compat) {
					starpu_fxt_stop_profiling ();
					starpu_shutdown ();
				}

				// Calculate the wall time of custom implementation
				double custom_starpu_time = calculate_wall_time (custom_starpu_start, custom_starpu_finish);

				if (args.test_correctness) {
					make_matrix_lower_triangular (work_mat, mat_size);
					double custom_starpu_test_ratio = get_cholesky_test_ratio (work_mat, orig_mat, mat_size);

					print_cholesky_result_with_test (
						CUSTOM_STARPU_IMPLEMENTATION_TEXT,
						custom_starpu_test_ratio,
						custom_starpu_time,
						mat_size,
						block_size,
						args.no_threads,
						iteration + 1
					);
				} else {
					print_cholesky_result (
						CUSTOM_STARPU_IMPLEMENTATION_TEXT,
						custom_starpu_time,
						mat_size,
						block_size,
						args.no_threads,
						iteration + 1
					);
				}
				#endif
				
				#if defined (USE_OPENMP)
				// Copy the original matrix to work_mat
				#if defined (USE_MKL) || defined (USE_OPENBLAS) || defined (USE_ARMPL)
				LAPACKE_dlacpy (LAPACK_COL_MAJOR, 'L', mat_size, mat_size, orig_mat, mat_size, work_mat, mat_size);
				#elif defined (USE_AOCL)
				dlacpy_ ("L", &mat_size, &mat_size, orig_mat, &mat_size, work_mat, &mat_size);
				#else
				copy_lower_triangular_matrix (orig_mat, work_mat, mat_size, mat_size);
				#endif

				// Set number of threads used by MKL/BLIS to 1 so as not to parallelize the BLAS routines
				#if defined (USE_MKL)
				mkl_set_num_threads (1);
				#elif defined (USE_AOCL)
				bli_thread_set_num_threads (1);
				FLASH_Queue_set_num_threads (1);
				#elif defined (USE_OPENBLAS)
				openblas_set_num_threads (1);
				#elif defined (USE_ARMPL)
				armpl_set_num_threads (1);
				#endif

				// Run the custom OpenMP implementation
				// Source for wall time measurement: https://stackoverflow.com/questions/2962785/c-using-clock-to-measure-time-in-multi-threaded-programs
				struct timespec custom_openmp_start, custom_openmp_finish;
				clock_gettime (CLOCK_MONOTONIC, &custom_openmp_start);
				openmp_block_cholesky (work_mat, mat_size, block_size, args.translation_method);
				clock_gettime (CLOCK_MONOTONIC, &custom_openmp_finish);

				// Calculate the wall time of custom implementation
				double custom_openmp_time = calculate_wall_time (custom_openmp_start, custom_openmp_finish);

				if (args.test_correctness) {
					make_matrix_lower_triangular (work_mat, mat_size);
					double custom_openmp_test_ratio = get_cholesky_test_ratio (work_mat, orig_mat, mat_size);

					print_cholesky_result_with_test (
						CUSTOM_OPENMP_IMPLEMENTATION_TEXT,
						custom_openmp_test_ratio,
						custom_openmp_time,
						mat_size,
						block_size,
						args.no_threads,
						iteration + 1
					);
				} else {
					print_cholesky_result (
						CUSTOM_OPENMP_IMPLEMENTATION_TEXT,
						custom_openmp_time,
						mat_size,
						block_size,
						args.no_threads,
						iteration + 1
					);
				}
				#endif

				#if defined (LIBRARY_IMPLEMENTATION_TEXT)
				if (!args.custom_only) {

					// Overwrite the calulated result in work_mat
					#if defined (USE_MKL) || defined (USE_OPENBLAS) || defined (USE_ARMPL)
					LAPACKE_dlacpy (LAPACK_COL_MAJOR, 'L', mat_size, mat_size, orig_mat, mat_size, work_mat, mat_size);
					#elif defined (USE_AOCL)
					dlacpy_ ("L", &mat_size, &mat_size, orig_mat, &mat_size, work_mat, &mat_size);
					#else
					copy_lower_triangular_matrix (orig_mat, work_mat, mat_size, mat_size);
					#endif

					// Set the desired number of threads for the library implementation
					#if defined (USE_MKL)
					mkl_set_num_threads (args.no_threads);
					#elif defined (USE_AOCL)
					bli_thread_set_num_threads (args.no_threads);
					FLASH_Queue_set_num_threads (args.no_threads);
					#elif defined (USE_OPENBLAS)
					openblas_set_num_threads (args.no_threads);
					#elif defined (USE_ARMPL)
					armpl_set_num_threads (args.no_threads);
					#endif

					// Run the library implementation
					struct timespec library_start, library_finish;
					#if defined (USE_MKL) || defined (USE_OPENBLAS) || defined (USE_ARMPL)
					clock_gettime (CLOCK_MONOTONIC, &library_start);
					assert (LAPACKE_dpotrf (LAPACK_COL_MAJOR, 'L', mat_size, work_mat, mat_size) == 0);
					clock_gettime (CLOCK_MONOTONIC, &library_finish);
					#elif defined (USE_AOCL)
					clock_gettime (CLOCK_MONOTONIC, &library_start);
					int info;
					dpotrf_ ("L", &mat_size, work_mat, &mat_size, &info);
					assert (info == 0);
					clock_gettime (CLOCK_MONOTONIC, &library_finish);
					#endif

					double library_time = calculate_wall_time (library_start, library_finish);

					if (args.test_correctness) {
						make_matrix_lower_triangular (work_mat, mat_size);
						double library_test_ratio = get_cholesky_test_ratio (work_mat, orig_mat, mat_size);

						print_cholesky_result_with_test (
							LIBRARY_IMPLEMENTATION_TEXT,
							library_test_ratio,
							library_time,
							mat_size,
							block_size,
							args.no_threads,
							iteration + 1
						);
					} else {
						print_cholesky_result (
							LIBRARY_IMPLEMENTATION_TEXT,
							library_time,
							mat_size,
							block_size,
							args.no_threads,
							iteration + 1
						);
					}
				}
				#endif
			}
		}

		free (orig_mat);
		free (work_mat);
	}

	return 0;
}
