#include "trsm.h"
#include "../common/math_util.h"
#include "../common/value_checking.h"

#include <memory.h>

// Convenience macros
#define A_ELEM(i,j) A[i + j * lda]
#define B_ELEM(i,j) B[i + j * ldb]

int dtrsm (char side, char uplo, char trans_A, char diag,
		   int M, int N,
		   double alpha, double* A, int lda,
		   double* B, int ldb) {

	// Test the argument values
	if (!VALID_SIDE_VALUE(side)) {
		return 1;
	} else if (!VALID_UPLO_VALUE(uplo)) {
		return 2;
	} else if (!VALID_TRANS_VALUE(trans_A)) {
		return 3;
	} else if (!VALID_DIAG_VALUE(diag)) {
		return 4;
	} else if (M < 0) {
		return 5;
	} else if (N < 0) {
		return 6;
	} else if ((IS_LEFT(side) && lda < max (1, M)) || (IS_RIGHT(side) && lda < max (1, N))) {
		return 9;
	} else if (ldb < max (1, M)) {
		return 11;
	}

	if (IS_LEFT(side)) {
		// Calculating op(A) * X = alpha * B

		if (IS_UPPER(uplo)) {
			// A is upper triangular

			if (IS_NOT_TRANS(trans_A)) {
				// op(A) = A

				for (int right_side = 0; right_side < N; right_side++) {
					for (int calculated_var = M - 1; calculated_var >= 0; calculated_var--) {
						B_ELEM(calculated_var, right_side) *= alpha;
						for (int already_solved_var = M - 1; already_solved_var > calculated_var; already_solved_var--) {
							B_ELEM(calculated_var, right_side) -= B_ELEM(already_solved_var, right_side) * A_ELEM(calculated_var, already_solved_var);
						}

						// Only divide by the diagonal entries if the matrix does not have a unit diagonal
						if (IS_NOT_UNIT(diag)) {
							B_ELEM(calculated_var, right_side) /= A_ELEM(calculated_var, calculated_var);
						}
					}
				}
			} else {
				// op(A) = A^T

				for (int right_side = 0; right_side < N; right_side++) {
					for (int calculated_var = 0; calculated_var < M; calculated_var++) {
						B_ELEM(calculated_var, right_side) *= alpha;
						for (int already_solved_var = 0; already_solved_var < calculated_var; already_solved_var++) {
							B_ELEM(calculated_var, right_side) -= B_ELEM(already_solved_var, right_side) * A_ELEM(already_solved_var, calculated_var);
						}

						// Only divide by the diagonal entries if the matrix does not have a unit diagonal
						if (IS_NOT_UNIT(diag)) {
							B_ELEM(calculated_var, right_side) /= A_ELEM(calculated_var, calculated_var);
						}
					}
				}
			}
		} else {
			// A is lower triangular

			if (IS_NOT_TRANS(trans_A)) {
				// op(A) = A

				for (int right_side = 0; right_side < N; right_side++) {
					for (int calculated_var = 0; calculated_var < M; calculated_var++) {
						B_ELEM(calculated_var, right_side) *= alpha;
						for (int already_solved_var = 0; already_solved_var < calculated_var; already_solved_var++) {
							B_ELEM(calculated_var, right_side) -= B_ELEM(already_solved_var, right_side) * A_ELEM(calculated_var, already_solved_var);
						}

						// Only divide by the diagonal entries if the matrix does not have a unit diagonal
						if (IS_NOT_UNIT(diag)) {
							B_ELEM(calculated_var, right_side) /= A_ELEM(calculated_var, calculated_var);
						}
					}
				}
			} else {
				// op(A) = A^T

				for (int right_side = 0; right_side < N; right_side++) {
					for (int calculated_var = M - 1; calculated_var >= 0; calculated_var--) {
						B_ELEM(calculated_var, right_side) *= alpha;
						for (int already_solved_var = M - 1; already_solved_var > calculated_var; already_solved_var--) {
							B_ELEM(calculated_var, right_side) -= B_ELEM(already_solved_var, right_side) * A_ELEM(already_solved_var, calculated_var);
						}

						// Only divide by the diagonal entries if the matrix does not have a unit diagonal
						if (IS_NOT_UNIT(diag)) {
							B_ELEM(calculated_var, right_side) /= A_ELEM(calculated_var, calculated_var);
						}
					}
				}
			}
		}
	} else {
		// Calculating X * op(A) = alpha * B

		if (IS_UPPER(uplo)) {
			// A is upper triangular

			if (IS_NOT_TRANS(trans_A)) {
				// op(A) = A

				for (int right_side = 0; right_side < M; right_side++) {
					for (int calculated_var = 0; calculated_var < N; calculated_var++) {
						B_ELEM(right_side, calculated_var) *= alpha;
						for (int already_solved_var = 0; already_solved_var < calculated_var; already_solved_var++) {
							B_ELEM(right_side, calculated_var) -= B_ELEM(right_side, already_solved_var) * A_ELEM(already_solved_var, calculated_var);
						}

						// Only divide by the diagonal entries if the matrix does not have a unit diagonal
						if (IS_NOT_UNIT(diag)) {
							B_ELEM(right_side, calculated_var) /= A_ELEM(calculated_var, calculated_var);
						}
					}
				}
			} else {
				// op(A) = A^T

				for (int right_side = 0; right_side < M; right_side++) {
					for (int calculated_var = N - 1; calculated_var >= 0; calculated_var--) {
						B_ELEM(right_side, calculated_var) *= alpha;
						for (int already_solved_var = N - 1; already_solved_var > calculated_var; already_solved_var--) {
							B_ELEM(right_side, calculated_var) -= B_ELEM(right_side, already_solved_var) * A_ELEM(calculated_var, already_solved_var);
						}

						// Only divide by the diagonal entries if the matrix does not have a unit diagonal
						if (IS_NOT_UNIT(diag)) {
							B_ELEM(right_side, calculated_var) /= A_ELEM(calculated_var, calculated_var);
						}
					}
				}
			}
		} else {
			// A is lower triangular

			if (IS_NOT_TRANS(trans_A)) {
				// op(A) = A

				for (int right_side = 0; right_side < M; right_side++) {
					for (int calculated_var = N - 1; calculated_var >= 0; calculated_var--) {
						B_ELEM(right_side, calculated_var) *= alpha;
						for (int already_solved_var = N - 1; already_solved_var > calculated_var; already_solved_var--) {
							B_ELEM(right_side, calculated_var) -= B_ELEM(right_side, already_solved_var) * A_ELEM(already_solved_var, calculated_var);
						}

						// Only divide by the diagonal entries if the matrix does not have a unit diagonal
						if (IS_NOT_UNIT(diag)) {
							B_ELEM(right_side, calculated_var) /= A_ELEM(calculated_var, calculated_var);
						}
					}
				}
			} else {
				// op(A) = A^T
				// This is the only branch used in Cholesky decomposition, therefore it is the only optimised one

				for (int calculated_var = 0; calculated_var < N; calculated_var++) {
					for (int right_side = 0; right_side < M; right_side++) {
						B_ELEM(right_side, calculated_var) *= alpha;
					}

					for (int already_solved_var = 0; already_solved_var < calculated_var; already_solved_var++) {
						for (int right_side = 0; right_side < M; right_side++) {
							B_ELEM(right_side, calculated_var) -= B_ELEM(right_side, already_solved_var) * A_ELEM(calculated_var, already_solved_var);
						}
					}

					// Only divide by the diagonal entries if the matrix does not have a unit diagonal
					if (IS_NOT_UNIT(diag)) {
						for (int right_side = 0; right_side < M; right_side++) {
							B_ELEM(right_side, calculated_var) /= A_ELEM(calculated_var, calculated_var);
						}
					}
				}
			}
		}
	}

	return 0;
}
