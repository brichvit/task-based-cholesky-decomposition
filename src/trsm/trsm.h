#ifndef TRSM_H_
#define TRSM_H_

/**
 * @brief Performs the matrix-matrix operation
 * 
 * - B <- alpha * op(A)^-1 * B, where A in an M x M matrix is SIDE = 'L'
 * - B <- alpha * B * op(A)^-1, where A in an N x N matrix is SIDE = 'R'
 * 
 * with a nonsignular triangular matrix A and an M x N matrix B.
 * 
 * @param SIDE The calculation to be performed ('L' or 'R', see above)
 * @param UPLO The part of A that is accessed ('L' for lower, 'U' for upper)
 * @param TRANSA Determines whether A should act as transposed ('N' for op(A)=A, 'T' or 'C' for op(A)=A^T)
 * @param DIAG Determines whether the diagonal entries of A are assumed to be equal to one ('U' for unit diagonal, 'N' for non-unit diagonal)
 * @param M The number of rows of B
 * @param N The number of columns of B
 * @param ALPHA The parameter alpha used above
 * @param A The nonsingular triangular matrix A in a column major full storage format
 * @param LDA The leading dimension of A
 * @param B The matrix B in a column major full storage format
 * @param LDB The leading dimension of B
 * @return int The error code (zero if the the function has finished correctly)
 */
int dtrsm (char SIDE, char UPLO, char TRANSA, char DIAG,
		   int M, int N,
		   double ALPHA, double* A, int LDA,
		   double* B, int LDB);

#endif /* TRSM_H_ */
