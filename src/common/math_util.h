#ifndef MATH_UTIL_H_
#define MATH_UTIL_H_

/**
 * @brief Returns the maximum of two numbers @p a and @p b.
 * 
 * @return int The maximum of @p a and @p b 
 */
int min (int a, int b);

/**
 * @brief Returns the minimum of two numbers @p a and @p b.
 * 
 * @return int The minimum of @p a and @p b 
 */
int max (int a, int b);

/**
 * @brief Returns:
 * - @p val if @p min_val <= @p val <= @p max_val
 * - @p min_val if @p val < @p min_val
 * - @p max_val if @p max_val < @p max_val
 * 
 * @return int The returned value as described above
 */
int clamp (int val, int min_val, int max_val);

#endif /* MATH_UTIL_H_ */
