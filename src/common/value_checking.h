#ifndef VALUE_CHECKING_H_
#define VALUE_CHECKING_H_

#include <ctype.h>

#define IS_TRANS(val) (toupper (val) == 'T' || toupper (val) == 'C')
#define IS_NOT_TRANS(val) (toupper (val) == 'N')
#define VALID_TRANS_VALUE(val) (IS_TRANS(val) || IS_NOT_TRANS(val))

#define IS_UPPER(val) (toupper (val) == 'U')
#define IS_LOWER(val) (toupper (val) == 'L')
#define VALID_UPLO_VALUE(val) (IS_UPPER(val) || IS_LOWER(val))

#define IS_LEFT(val) (toupper (val) == 'L')
#define IS_RIGHT(val) (toupper (val) == 'R')
#define VALID_SIDE_VALUE(val) (IS_LEFT(val) || IS_RIGHT(val))

#define IS_UNIT(val) (toupper (val) == 'U')
#define IS_NOT_UNIT(val) (toupper (val) == 'N')
#define VALID_DIAG_VALUE(val) (IS_UNIT(val) || IS_NOT_UNIT(val))

#define IS_NAN(val) (val != val)

#endif /* VALUE_CHECKING_H_ */
