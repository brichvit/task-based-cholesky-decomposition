TOPDIR	= .
include make.inc

C_SRC	= $(wildcard src/*/*.c)
BLAS_SRCS	= $(wildcard $(GEMM_SRC)/*.c) $(wildcard $(SYRK_SRC)/*.c) $(wildcard $(TRSM_SRC)/*.c)
POTF2_SRCS	= $(wildcard $(POTF2_SRC)/*.c)

COMMON_SRCS	= $(wildcard $(COMMON_SRC)/*.c)

CHOLESKY_SRCS	= $(wildcard $(CHOLESKY_SRC)/*.c)

BENCHMARK_SRCS	= $(wildcard $(BENCHMARK_SRC)/*.c)

LAPACK_SRCS = $(wildcard lapack/*.f lapack/*.f90 lapack/*.F90)

OBJS = $(C_SRC:.c=.o)

BLAS_OBJS 	= $(BLAS_SRCS:.c=.o)
COMMON_OBJS 	= $(COMMON_SRCS:.c=.o)
POTF2_OBJS 	= $(POTF2_SRCS:.c=.o)

CHOLESKY_OBJS	= $(CHOLESKY_SRCS:.c=.o)

BENCHMARK_OBJS	= $(BENCHMARK_SRCS:.c=.o)

DEPS		= $(C_SRC:.c=.d)

.PHONY: all
all: libcholesky.so $(STATIC_TARGET) cholesky.h cholesky_benchmark test

-include $(DEPS)

.PHONY: test
test: blas_test potf2_test cholesky_test

src/%.d: src/%.c
	$(CC) -MM $< | sed "s/^\(.*\).o:/$$(dirname $< | sed "s/\//\\\\\//g")\/\1.o:/g" > $@
	$(CC) -MM $< | sed "s/^\(.*\).o:/$$(dirname $< | sed "s/\//\\\\\//g")\/\1.d:/g" >> $@

$(STATIC_TARGET): $(CHOLESKY_OBJS) $(COMMON_OBJS) $(ADDITIONAL_CHOLESKY_OBJS)
	$(AR) rcs $@ $^

libcholesky.so: $(CHOLESKY_OBJS) $(COMMON_OBJS) $(ADDITIONAL_CHOLESKY_OBJS)
	$(CC) $(CFLAGS) -shared $^ -o $@ $(LDFLAGS)

cholesky.h: $(CHOLESKY_SRC)/cholesky.h
	$(CC) $(CFLAGS) -o- -E $< | grep -v "^#" > $@

cholesky_benchmark: $(CHOLESKY_OBJS) $(COMMON_OBJS) $(BENCHMARK_OBJS) $(ADDITIONAL_CHOLESKY_OBJS)
	$(CC) $(CFLAGS) $^ -o $@ $(LDFLAGS)

blas_test: $(BLAS_TEST)/blas_test.o $(BLAS_TEST)/blas_wrappers.o $(BLAS_OBJS) $(COMMON_OBJS)
	$(FC) $(FFLAGS) $^ -o $@

$(BLAS_TEST)/blas_test.o: $(BLAS_TEST)/blas_test.f
	$(FC) $(FFLAGS) $< -c -o $@

$(BLAS_TEST)/blas_wrappers.o: $(BLAS_TEST)/blas_wrappers.c
	$(CC) $(CFLAGS) $< -c -o $@

potf2_test: $(POTF2_TEST)/potf2_test.o $(POTF2_TEST)/potf2_wrapper.o $(POTF2_TEST)/xerbla_helper.o $(POTF2_OBJS) $(COMMON_OBJS) \
			$(LAPACK)/dchkpo.o $(LAPACK)/la_constants.o $(LAPACK)/la_xisnan.o $(LAPACK)/dlatb4.o \
			$(LAPACK)/dpptri.o $(LAPACK)/dlamch.o $(LAPACK)/dsptrs.o $(LAPACK)/dgetrs.o $(LAPACK)/dsytrf_rk.o \
			$(LAPACK)/alareq.o $(LAPACK)/dlarnv.o $(LAPACK)/dpot01.o $(LAPACK)/daxpy.o $(LAPACK)/dsygs2.o \
			$(LAPACK)/dporfs.o $(LAPACK)/dget06.o $(LAPACK)/dgetrf.o $(LAPACK)/ddot.o $(LAPACK)/dlasyf_rk.o \
			$(LAPACK)/dgesv.o $(LAPACK)/dgbtrf.o $(LAPACK)/dppsvx.o $(LAPACK)/dger.o $(LAPACK)/derrvx.o \
			$(LAPACK)/iladlc.o $(LAPACK)/dptcon.o $(LAPACK)/dtrmm.o $(LAPACK)/ilaver.o $(LAPACK)/dgemm.o \
			$(LAPACK)/alaesm.o $(LAPACK)/xerbla.o $(LAPACK)/dlarfg.o $(LAPACK)/dsbmv.o $(LAPACK)/dlaqsy.o \
			$(LAPACK)/dsymv.o $(LAPACK)/derrpo.o $(LAPACK)/drscl.o $(LAPACK)/dtrti2.o $(LAPACK)/dpotri.o \
			$(LAPACK)/dtptri.o $(LAPACK)/dtrsm.o $(LAPACK)/dtrsv.o $(LAPACK)/dget04.o $(LAPACK)/dsycon.o \
			$(LAPACK)/dpbrfs.o $(LAPACK)/dlangb.o $(LAPACK)/dlartg.o $(LAPACK)/dsytf2.o $(LAPACK)/dspcon.o \
			$(LAPACK)/dlaqsp.o $(LAPACK)/dsysvx.o $(LAPACK)/dpoequ.o $(LAPACK)/dspmv.o $(LAPACK)/lsame.o \
			$(LAPACK)/dsytrf.o $(LAPACK)/dsysv_aa.o $(LAPACK)/dlaisnan.o $(LAPACK)/drot.o $(LAPACK)/dpttrs.o \
			$(LAPACK)/dspsvx.o $(LAPACK)/dlaset.o $(LAPACK)/dsytrs.o $(LAPACK)/dsyrk.o $(LAPACK)/dtrsm.o \
			$(LAPACK)/dsyr2.o $(LAPACK)/dpot03.o $(LAPACK)/dgbcon.o $(LAPACK)/dlangt.o $(LAPACK)/alasvm.o \
			$(LAPACK)/dlarft.o $(LAPACK)/dlatm1.o $(LAPACK)/ieeeck.o $(LAPACK)/dsytrs_aa.o \
			$(LAPACK)/dlasyf.o $(LAPACK)/dsyrfs.o $(LAPACK)/dsprfs.o $(LAPACK)/alaerh.o $(LAPACK)/dpot05.o \
			$(LAPACK)/chkxer.o $(LAPACK)/dtpmv.o $(LAPACK)/dlantb.o $(LAPACK)/dsytrs_rook.o $(LAPACK)/dsysv_rk.o \
			$(LAPACK)/dlansp.o $(LAPACK)/dscal.o $(LAPACK)/dsytrf_rook.o $(LAPACK)/dsyrk.o $(LAPACK)/dlassq.o \
			$(LAPACK)/dgerq2.o $(LAPACK)/dlagge.o $(LAPACK)/dtrtri.o $(LAPACK)/dlacn2.o $(LAPACK)/dlaswp.o \
			$(LAPACK)/dtrmv.o $(LAPACK)/dsytf2_rook.o $(LAPACK)/dlatrs.o $(LAPACK)/dgbrfs.o $(LAPACK)/iladlr.o \
			$(LAPACK)/dsptrf.o $(LAPACK)/dsysv_rook.o $(LAPACK)/dppsv.o $(LAPACK)/disnan.o $(LAPACK)/dlauum.o \
			$(LAPACK)/dppcon.o $(LAPACK)/dposv.o $(LAPACK)/xlaenv.o $(LAPACK)/dpbequ.o $(LAPACK)/dpbtf2.o \
			$(LAPACK)/dsyr2k.o $(LAPACK)/ilaenv.o $(LAPACK)/dlaqsb.o $(LAPACK)/dptts2.o $(LAPACK)/dpot02.o \
			$(LAPACK)/dsymm.o $(LAPACK)/dgtsvx.o $(LAPACK)/dgtts2.o $(LAPACK)/dgerfs.o $(LAPACK)/dsygst.o \
			$(LAPACK)/dgbequ.o $(LAPACK)/dgbmv.o $(LAPACK)/dlarhs.o $(LAPACK)/dgerqf.o $(LAPACK)/dasum.o \
			$(LAPACK)/dsyr.o $(LAPACK)/dgttrf.o $(LAPACK)/dsytrf_aa_2stage.o $(LAPACK)/iparmq.o $(LAPACK)/dgesvx.o \
			$(LAPACK)/dpbtrs.o $(LAPACK)/dnrm2.o $(LAPACK)/dlarot.o $(LAPACK)/dsyconv.o $(LAPACK)/lsamen.o \
			$(LAPACK)/dlaqge.o $(LAPACK)/dpbcon.o $(LAPACK)/dspsv.o $(LAPACK)/dgetrf2.o $(LAPACK)/dptsvx.o \
			$(LAPACK)/dpprfs.o $(LAPACK)/alasum.o $(LAPACK)/dpbsv.o $(LAPACK)/aladhd.o $(LAPACK)/dlatps.o \
			$(LAPACK)/ddrvpo.o $(LAPACK)/dlatbs.o $(LAPACK)/dgtrfs.o $(LAPACK)/dgtcon.o $(LAPACK)/dswap.o \
			$(LAPACK)/dlabad.o $(LAPACK)/dpptrf.o $(LAPACK)/dpbsvx.o $(LAPACK)/alahd.o $(LAPACK)/dlacpy.o \
			$(LAPACK)/dpocon.o $(LAPACK)/dlatms.o $(LAPACK)/dsecnd.o $(LAPACK)/dsytrs_3.o $(LAPACK)/dlagtm.o \
			$(LAPACK)/dtbmv.o $(LAPACK)/dlasyf_rook.o $(LAPACK)/dgtsv.o $(LAPACK)/dsytf2_rk.o $(LAPACK)/dgttrs.o \
			$(LAPACK)/dcopy.o $(LAPACK)/dlaran.o $(LAPACK)/dposvx.o $(LAPACK)/dspr.o $(LAPACK)/dlaruv.o \
			$(LAPACK)/dlauu2.o $(LAPACK)/dgbsv.o $(LAPACK)/dsysv_aa_2stage.o $(LAPACK)/dpptrs.o $(LAPACK)/dlapy2.o \
			$(LAPACK)/dlarf.o $(LAPACK)/dlaqgb.o $(LAPACK)/dgbtrs.o $(LAPACK)/dlarnd.o $(LAPACK)/dptsv.o \
			$(LAPACK)/dlansy.o $(LAPACK)/dptrfs.o $(LAPACK)/dlasyf_aa.o $(LAPACK)/dlantr.o $(LAPACK)/dtpsv.o \
			$(LAPACK)/dlanst.o $(LAPACK)/dpttrf.o $(LAPACK)/dpotrs.o $(LAPACK)/dpbtrf.o $(LAPACK)/dtbsv.o \
			$(LAPACK)/dppequ.o $(LAPACK)/dlagsy.o $(LAPACK)/dgbtf2.o $(LAPACK)/dgbsvx.o $(LAPACK)/dgeequ.o \
			$(LAPACK)/idamax.o $(LAPACK)/dlarfb.o $(LAPACK)/dsysv.o $(LAPACK)/dgecon.o $(LAPACK)/dsytrf_aa.o \
			$(LAPACK)/dsytrs_aa_2stage.o $(LAPACK)/dlansb.o $(LAPACK)/dgemv.o $(LAPACK)/dsytrs2.o $(LAPACK)/dlange.o
	$(FC) $(FFLAGS) $^ -o $@ 

$(POTF2_TEST)/potf2_test.o: $(POTF2_TEST)/potf2_test.f
	$(FC) $(FFLAGS) $< -c -o $@

$(POTF2_TEST)/xerbla_helper.o: $(POTF2_TEST)/xerbla_helper.f
	$(FC) $(FFLAGS) $< -c -o $@

$(POTF2_TEST)/potf2_wrapper.o: $(POTF2_TEST)/potf2_wrapper.c
	$(CC) $(CFLAGS) $< -c -o $@

cholesky_test: $(CHOLESKY_TEST)/cholesky_test.o $(CHOLESKY_OBJS) $(COMMON_OBJS) $(ADDITIONAL_CHOLESKY_OBJS) \
				$(LAPACK)/aladhd.o $(LAPACK)/alaerh.o $(LAPACK)/alaesm.o $(LAPACK)/alahd.o $(LAPACK)/alareq.o \
				$(LAPACK)/alasum.o $(LAPACK)/alasvm.o $(LAPACK)/chkxer.o $(LAPACK)/dasum.o $(LAPACK)/daxpy.o \
				$(LAPACK)/dchkpo.o $(LAPACK)/dcopy.o $(LAPACK)/ddot.o $(LAPACK)/ddrvpo.o $(LAPACK)/derrpo.o \
				$(LAPACK)/derrvx.o $(LAPACK)/dgbcon.o $(LAPACK)/dgbequ.o $(LAPACK)/dgbmv.o $(LAPACK)/dgbrfs.o \
				$(LAPACK)/dgbsv.o $(LAPACK)/dgbsvx.o $(LAPACK)/dgbtf2.o $(LAPACK)/dgbtrf.o $(LAPACK)/dgbtrs.o \
				$(LAPACK)/dgecon.o $(LAPACK)/dgeequ.o $(LAPACK)/dgemm.o $(LAPACK)/dgemv.o $(LAPACK)/dger.o \
				$(LAPACK)/dgerfs.o $(LAPACK)/dgerq2.o $(LAPACK)/dgerqf.o $(LAPACK)/dgesv.o $(LAPACK)/dgesvx.o\
				$(LAPACK)/dget04.o $(LAPACK)/dget06.o $(LAPACK)/dgetrf2.o $(LAPACK)/dgetrf.o $(LAPACK)/dgetrs.o \
				$(LAPACK)/dgtcon.o $(LAPACK)/dgtrfs.o $(LAPACK)/dgtsv.o $(LAPACK)/dgtsvx.o $(LAPACK)/dgttrf.o \
				$(LAPACK)/dgttrs.o $(LAPACK)/dgtts2.o $(LAPACK)/disnan.o $(LAPACK)/dlabad.o $(LAPACK)/dlacn2.o \
				$(LAPACK)/dlacpy.o $(LAPACK)/dlagge.o $(LAPACK)/dlagsy.o $(LAPACK)/dlagtm.o $(LAPACK)/dlaisnan.o \
				$(LAPACK)/dlamch.o $(LAPACK)/dlangb.o $(LAPACK)/dlange.o $(LAPACK)/dlangt.o $(LAPACK)/dlansb.o \
				$(LAPACK)/dlansp.o $(LAPACK)/dlanst.o $(LAPACK)/dlansy.o $(LAPACK)/dlantb.o $(LAPACK)/dlantr.o \
				$(LAPACK)/dlapy2.o $(LAPACK)/dlaqgb.o $(LAPACK)/dlaqge.o $(LAPACK)/dlaqsb.o $(LAPACK)/dlaqsp.o \
				$(LAPACK)/dlaqsy.o $(LAPACK)/dlaran.o $(LAPACK)/dlarfb.o $(LAPACK)/dlarf.o $(LAPACK)/dlarfg.o \
				$(LAPACK)/dlarft.o $(LAPACK)/dlarhs.o $(LAPACK)/dlarnd.o $(LAPACK)/dlarnv.o $(LAPACK)/dlarot.o \
				$(LAPACK)/dlartg.o $(LAPACK)/dlaruv.o $(LAPACK)/dlaset.o $(LAPACK)/dlassq.o $(LAPACK)/dlaswp.o \
				$(LAPACK)/dlasyf_aa.o $(LAPACK)/dlasyf.o $(LAPACK)/dlasyf_rk.o $(LAPACK)/dlasyf_rook.o $(LAPACK)/dlatb4.o \
				$(LAPACK)/dlatbs.o $(LAPACK)/dlatm1.o $(LAPACK)/dlatms.o $(LAPACK)/dlatps.o $(LAPACK)/dlatrs.o \
				$(LAPACK)/dlauu2.o $(LAPACK)/dlauum.o $(LAPACK)/dnrm2.o $(LAPACK)/dpbcon.o $(LAPACK)/dpbequ.o \
				$(LAPACK)/dpbrfs.o $(LAPACK)/dpbsv.o $(LAPACK)/dpbsvx.o $(LAPACK)/dpbtf2.o $(LAPACK)/dpbtrf.o \
				$(LAPACK)/dpbtrs.o $(LAPACK)/dpocon.o $(LAPACK)/dpoequ.o $(LAPACK)/dporfs.o $(LAPACK)/dposv.o \
				$(LAPACK)/dposvx.o $(LAPACK)/dpot01.o $(LAPACK)/dpot02.o $(LAPACK)/dpot03.o $(LAPACK)/dpot05.o \
				$(LAPACK)/dpotri.o $(LAPACK)/dpotrs.o $(LAPACK)/dpotrf.o $(LAPACK)/dpotrf2.o $(LAPACK)/dpotf2.o \
				$(LAPACK)/dppcon.o $(LAPACK)/dppequ.o $(LAPACK)/dpprfs.o $(LAPACK)/dppsv.o $(LAPACK)/dppsvx.o \
				$(LAPACK)/dpptrf.o $(LAPACK)/dpptri.o $(LAPACK)/dpptrs.o $(LAPACK)/dptcon.o $(LAPACK)/dptrfs.o \
				$(LAPACK)/dptsv.o $(LAPACK)/dptsvx.o $(LAPACK)/dpttrf.o $(LAPACK)/dpttrs.o $(LAPACK)/dptts2.o \
				$(LAPACK)/drot.o $(LAPACK)/drscl.o $(LAPACK)/dsbmv.o $(LAPACK)/dscal.o $(LAPACK)/dsecnd.o \
				$(LAPACK)/dspcon.o $(LAPACK)/dspmv.o $(LAPACK)/dspr.o $(LAPACK)/dsprfs.o $(LAPACK)/dspsv.o \
				$(LAPACK)/dspsvx.o $(LAPACK)/dsptrf.o $(LAPACK)/dsptrs.o $(LAPACK)/dswap.o $(LAPACK)/dsycon.o \
				$(LAPACK)/dsyconv.o $(LAPACK)/dsygs2.o $(LAPACK)/dsygst.o $(LAPACK)/dsymm.o $(LAPACK)/dsymv.o \
				$(LAPACK)/dsyr2.o $(LAPACK)/dsyr2k.o $(LAPACK)/dsyr.o $(LAPACK)/dsyrfs.o $(LAPACK)/dsyrk.o \
				$(LAPACK)/dsysv_aa_2stage.o $(LAPACK)/dsysv_aa.o $(LAPACK)/dsysv.o $(LAPACK)/dsysv_rk.o \
				$(LAPACK)/dsysv_rook.o $(LAPACK)/dsysvx.o $(LAPACK)/dsytf2.o $(LAPACK)/dsytf2_rk.o $(LAPACK)/dsytf2_rook.o \
				$(LAPACK)/dsytrf_aa_2stage.o $(LAPACK)/dsytrf_aa.o $(LAPACK)/dsytrf.o $(LAPACK)/dsytrf_rk.o $(LAPACK)/dsytrf_rook.o \
				$(LAPACK)/dsytrs2.o $(LAPACK)/dsytrs_3.o $(LAPACK)/dsytrs_aa_2stage.o $(LAPACK)/dsytrs_aa.o $(LAPACK)/dsytrs.o \
				$(LAPACK)/dsytrs_rook.o $(LAPACK)/dtbmv.o $(LAPACK)/dtbsv.o $(LAPACK)/dtpmv.o $(LAPACK)/dtpsv.o \
				$(LAPACK)/dtptri.o $(LAPACK)/dtrmm.o $(LAPACK)/dtrmv.o $(LAPACK)/dtrsv.o $(LAPACK)/dtrsm.o \
				$(LAPACK)/dtrti2.o $(LAPACK)/dtrtri.o $(LAPACK)/idamax.o $(LAPACK)/ieeeck.o $(LAPACK)/iladlc.o \
				$(LAPACK)/iladlr.o $(LAPACK)/ilaenv.o $(LAPACK)/ilaver.o $(LAPACK)/iparmq.o $(LAPACK)/la_constants.o \
				$(LAPACK)/la_xisnan.o $(LAPACK)/lsame.o $(LAPACK)/lsamen.o $(LAPACK)/xerbla.o $(LAPACK)/xlaenv.o
	$(FC) $(FFLAGS) $^ -o $@ $(LDFLAGS)

$(CHOLESKY_TEST)/cholesky_test.o: $(CHOLESKY_TEST)/cholesky_test.c
	$(CC) $(CFLAGS) $< -c -o $@
				
src/%.o: src/%.c src/%.d
	$(CC) $(CFLAGS) $< -c -o $@

$(LAPACK)/dlartg.o: $(LAPACK)/dlartg.f90 $(LAPACK)/la_constants.mod
	$(FC) $(FFLAGS) $< -c -o $@

$(LAPACK)/dlassq.o: $(LAPACK)/dlassq.f90 $(LAPACK)/la_xisnan.mod
	$(FC) $(FFLAGS) $< -c -o $@

$(LAPACK)/dnrm2.o: $(LAPACK)/dnrm2.f90
	$(FC) $(FFLAGS) $< -c -o $@

$(LAPACK)/la_constants.mod $(LAPACK)/la_constants.o: $(LAPACK)/la_constants.f90
	$(FC) $(FFLAGS) $< -c -o $(LAPACK)/la_constants.o -J$(LAPACK)

$(LAPACK)/la_xisnan.mod $(LAPACK)/la_xisnan.o: $(LAPACK)/la_xisnan.F90 $(LAPACK)/la_constants.mod
	$(FC) $(FFLAGS) $< -c -o $(LAPACK)/la_xisnan.o -J$(LAPACK)

$(LAPACK)/%.o: $(LAPACK)/%.f
	$(FC) $(FFLAGS) $< -c -o $@

.PHONY: check check_blas check_potf2 check_cholesky
check: check_blas check_potf2 check_cholesky

check_blas: blas_test
	@echo ""
	@TEST_OUTPUT=$$(./blas_test < $(BLAS_TEST)/blas_test.in); \
	TEST_FAIL_LINES=$$(echo "$$TEST_OUTPUT" | grep '\(DGEMM\|DSYRK\|DTRSM\)  FAILED'); \
	if [ "$$TEST_FAIL_LINES" ]; then \
		echo "\033[1;31mFAIL\033[0m: blas_test"; \
		echo "Test output:"; \
		echo ; \
		echo "$$TEST_OUTPUT"; \
	else \
		echo "\033[1;32mPASS\033[0m: blas_test"; \
	fi
	
check_potf2: potf2_test
	@echo ""
	@TEST_OUTPUT=$$(./potf2_test < $(POTF2_TEST)/potf2_test.in); \
	TEST_FAIL_LINES=$$(echo "$$TEST_OUTPUT" | grep 'failed\|error messages'); \
	if [ "$$TEST_FAIL_LINES" ]; then \
		echo "\033[1;31mFAIL\033[0m: potf2_test"; \
		echo "Test output:"; \
		echo ; \
		echo "$$TEST_OUTPUT"; \
	else \
		echo "\033[1;32mPASS\033[0m: potf2_test"; \
	fi

check_cholesky: cholesky_test
	@echo ""
	@TEST_OUTPUT=$$(./cholesky_test); \
	TEST_FAIL_LINES=$$(echo "$$TEST_OUTPUT" | grep 'failed'); \
	if [ "$$TEST_FAIL_LINES" ]; then \
		echo "\033[1;31mFAIL\033[0m: cholesky_test"; \
		echo "Test output:"; \
		echo ; \
		echo "$$TEST_OUTPUT"; \
	else \
		echo "\033[1;32mPASS\033[0m: cholesky_test"; \
	fi

.PHONY: install install_dynamic install_static install_headers
install: install_benchmark install_dynamic install_static install_headers

$(PREFIX):
	mkdir -p "$(PREFIX)"

$(PREFIX)/bin: $(PREFIX)
	mkdir -p "$(PREFIX)/bin"

$(PREFIX)/include: $(PREFIX)
	mkdir -p "$(PREFIX)/include"

$(PREFIX)/lib: $(PREFIX)
	mkdir -p "$(PREFIX)/lib"

install_benchmark: cholesky_benchmark $(PREFIX)/bin
	cp $< "$(PREFIX)/bin/cholesky_benchmark"

install_dynamic: libcholesky.so $(PREFIX)/lib
	cp $< "$(PREFIX)/lib/libcholesky.so"

install_static: $(STATIC_TARGET) $(PREFIX)/lib
	if [ "$(STATIC_TARGET)" ]; then \
		cp $(STATIC_TARGET) "$(PREFIX)/lib/libcholesky.a"; \
	fi

install_headers: cholesky.h $(PREFIX)/include
	cp $< "$(PREFIX)/include/cholesky.h"


.PHONY: uninstall
uninstall:
	-rm "$(PREFIX)/bin/cholesky_benchmark" "$(PREFIX)/include/cholesky.h" "$(PREFIX)/lib/libcholesky.so" "$(PREFIX)/lib/libcholesky.a"

.PHONY: clean clean_dynamic clean_headers clean_benchmark clean_blas_test clean_potf2_test clean_cholesky_test clean_lapack clean_test_artifacts
clean: clean_dynamic clean_headers clean_benchmark clean_blas_test clean_potf2_test clean_cholesky_test clean_lapack clean_test_artifacts
	-rm $(OBJS) $(DEPS) $(LAPACK)/*.o $(LAPACK)/*.mod 2> /dev/null

clean_test_artifacts: 
	-rm blas_test potf2_test cholesky_test 2> /dev/null

clean_dynamic:
	-rm libcholesky.so 2> /dev/null

clean_headers:
	-rm cholesky.h 2> /dev/null

clean_benchmark:
	-rm cholesky_benchmark 2> /dev/null

clean_blas_test:
	-rm $(BLAS_TEST)/*.o 2> /dev/null

clean_potf2_test:
	-rm $(POTF2_TEST)/*.o 2> /dev/null

clean_cholesky_test:
	-rm $(CHOLESKY_TEST)/*.o 2> /dev/null

clean_lapack:
	-rm $(LAPACK)/*.o $(LAPACK)/*.mod 2> /dev/null

.PHONY: distclean
distclean: clean
	-rm make_config.inc